package tools.descartes.dml2pcm

import edu.kit.ipd.descartes.mm.applicationlevel.functions.FunctionsPackage
import edu.kit.ipd.descartes.mm.applicationlevel.repository.Repository
import edu.kit.ipd.descartes.mm.applicationlevel.repository.RepositoryPackage
import edu.kit.ipd.descartes.mm.applicationlevel.system.System
import edu.kit.ipd.descartes.mm.deployment.Deployment
import edu.kit.ipd.descartes.mm.deployment.DeploymentPackage
import edu.kit.ipd.descartes.mm.resourcelandscape.DistributedDataCenter
import edu.kit.ipd.descartes.mm.resourcelandscape.ResourcelandscapePackage
import edu.kit.ipd.descartes.mm.usageprofile.UsageProfile
import edu.kit.ipd.descartes.mm.usageprofile.UsageProfilePackage
import java.io.File
import java.io.IOException
import java.util.ArrayList
import java.util.Collections
import java.util.HashMap
import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl

class ModelIO {
	private static final Log log = LogFactory.getLog(ModelIO);

	public static ResourceSet resourceSet = null;

/* 	def void loadModel(ArrayList<File> files, String[] filetype, File pathToTransformation) {
		val pathTransformation = pathToTransformation.absolutePath;

		checkResourceSet();
		resourceSet.resources.removeAll();		
		var dmlResources = new HashMap<String,EObject>();
		for (var int i = 0; i < filetype.size; i++) {
			var resource = try {
					resourceSet.getResource(getURI(files.get(i)), true).contents.get(0);
				} catch (IOException e) {
					null
				}
			if(resource!=null){
				log.info("loaded DML "+filetype.get(i));
			}
			else{
				log.info("failed to load DML "+filetype.get(i));
			}
			dmlResources.put(filetype.get(i),resource);
		}
		
		

		val pcmName = "Transformation"
		val pcmResourceList = newArrayList("repository", "allocation", "usagemodel", "resourceenvironment", "system");
		val pcmResources = new HashMap<String, Resource>();
		pcmResourceList.forEach[pcmResources.put(it, addResource(pcmName, it, resourceSet, pathTransformation))];
		
		var transformator = new Transformator()
		
		
		//val pcmResourceLandscape = Starter.resourceLandscapeTransformator.
			//transformDistributedDataCenter(dmlResources.get("resourcelandscape") as DistributedDataCenter);
		//val pcmRepository = Starter.repositoryTransformator.transformRepository(dmlResources.get("repository") as Repository);
		 
		val pcmAllocation = Starter.deploymentTransformator.transformDeployment(dmlResources.get("deployment") as Deployment);
		if ((dmlResources.get("system") as System).id != pcmAllocation.system_Allocation.id) {
			log.info("system loaded from File is not the system of Allocation");
		}
		val pcmUsageModel = Starter.usageprofileTransformator.transformUsageProfile(dmlResources.get("usageprofile") as UsageProfile);
		
		log.info("saving transformation resources to:"+pathTransformation);/
		pcmResources.get("resourceenvironment").contents.add(pcmResourceLandscape);
		pcmResources.get("repository").contents.add(pcmRepository);
		pcmResources.get("system").contents.add(pcmAllocation.system_Allocation);
		pcmResources.get("allocation").contents.add(pcmAllocation);
		pcmResources.get("usagemodel").contents.add(pcmUsageModel);
		pcmResources.forEach[k, v|log.info("Saving "+k);saveResource(v)];
	}
	*/
	
	def void loadModel(File[] files){
		loadModel(files.get(0),files.get(1),files.get(2));
	}
	def void loadModel(File deploymentFile,File usageFile, File pathToTransformation) {
		val pathTransformation = pathToTransformation.absolutePath;

		checkResourceSet();
		resourceSet.resources.removeAll();		
		
		var deployment = resourceSet.getResource(getURI(deploymentFile),true).contents.get(0);
		var usage = resourceSet.getResource(getURI(usageFile),true).contents.get(0);
		var transformator = new Transformator(deployment as Deployment,usage as UsageProfile);
		saveModel(transformator,pathToTransformation);
		//var transformator = new Transformator(deployment,usage);		
	}
	
	def void saveModel(Transformator transformator, File pcmPath){
			val pcmName = "Transformation"
		val pcmResourceList = newArrayList("repository", "allocation", "usagemodel", "resourceenvironment", "system");
		val pcmResources = new HashMap<String, Resource>();
		pcmResourceList.forEach[pcmResources.put(it, addResource(pcmName, it, resourceSet, pcmPath.absolutePath))];
		
		pcmResources.get("resourceenvironment").contents.add(transformator.resourceEnvironment);
		pcmResources.get("repository").contents.add(transformator.repository);
		pcmResources.get("system").contents.add(transformator.system);
		pcmResources.get("allocation").contents.add(transformator.allocation);
		pcmResources.get("usagemodel").contents.add(transformator.usage);
		pcmResources.forEach[k, v|log.info("Saving "+k);saveResource(v)];
		
		
	}
	
	
	def File[] getDeploymentReferences(File deploymentFile){
		checkResourceSet();
		resourceSet.resources.removeAll();
		var deployment = resourceSet.getResource(getURI(deploymentFile),true).contents.get(0) as Deployment;
		var resourcelandscape = deployment.targetResourceLandscape;
		var system = deployment.system;
		var repository = deployment.system.assemblyContexts.get(0).encapsulatedComponent.repository;
		var resourcelandscapeFile = new File(resourcelandscape.eResource.URI.path);
		var systemFile = new File(system.eResource.URI.path);
		var repositoryFile = new File(repository.eResource.URI.path);
		var File[] files = #[resourcelandscapeFile,systemFile,repositoryFile];
		return(files);
		
	}
		
	def checkResourceSet() {
		if (resourceSet == null) {
			instantiateResourceSet();
		}
	}


	/**
	 * make URI
	 */
	def URI getURI(File file) {
		if(file == null) return null;
		return (URI.createFileURI(file.absolutePath));
	}

	def URI getURI(String path, String name) {
		return (URI.createFileURI(getFile(path, name).absolutePath));
	}

	/**
	 * returns a file with the given ending in the given folder,
	 * if more than one was found returns first
	 */
	def File getFile(String path, String ending) {
		val list = getFiles(path, ending);
		return (list.get(0));
	}

	def File[] getFiles(String path, String ending) {
		val folder = new File(path);
		val list = folder.listFiles.filter[it.name.endsWith(ending)];
		return (list)
	}

	def Resource addResource(String name, String filename, ResourceSet resourceSet, String path) {
		val fileUrl = URI.createFileURI(new File(path + "/" + name + "." + filename).getAbsolutePath());
		val resource = resourceSet.createResource(fileUrl);
		return resource;
	}

	def void saveResource(Resource resource) {
		resource.save(Collections.EMPTY_MAP);

	}

	def void instantiateResourceSet() {
		resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
			Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
		val dmlDeploymentPackage = DeploymentPackage.eINSTANCE;
		val dmlRepositoryPackage = RepositoryPackage.eINSTANCE;
		val dmlUsagePackage = UsageProfilePackage.eINSTANCE;
		val dmlResourcePackage = ResourcelandscapePackage.eINSTANCE;
		val dmlFunction = FunctionsPackage.eINSTANCE;
	}

}
