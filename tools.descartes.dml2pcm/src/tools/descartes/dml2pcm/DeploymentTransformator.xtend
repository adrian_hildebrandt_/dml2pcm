package tools.descartes.dml2pcm


import edu.kit.ipd.descartes.mm.deployment.Deployment
import edu.kit.ipd.descartes.mm.deployment.DeploymentContext
import de.uka.ipd.sdq.pcm.allocation.AllocationFactory
import edu.kit.ipd.descartes.mm.applicationlevel.repository.AssemblyContext
import de.uka.ipd.sdq.pcm.core.CoreFactory
import de.uka.ipd.sdq.pcm.core.composition.CompositionFactory
import de.uka.ipd.sdq.pcm.repository.BasicComponent
import java.util.ArrayList
import de.uka.ipd.sdq.pcm.allocation.AllocationContext
import org.eclipse.emf.common.util.EList
import de.uka.ipd.sdq.pcm.repository.CompositeComponent
import de.uka.ipd.sdq.pcm.repository.RepositoryComponent
import de.uka.ipd.sdq.pcm.repository.OperationSignature
import de.uka.ipd.sdq.pcm.seff.ResourceDemandingSEFF
import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import de.uka.ipd.sdq.pcm.qosannotations.qos_performance.provider.ComponentSpecifiedExecutionTimeItemProvider
import de.uka.ipd.sdq.pcm.qosannotations.qos_performance.util.QosPerformanceAdapterFactory
import de.uka.ipd.sdq.pcm.qosannotations.QoSAnnotations
import de.uka.ipd.sdq.pcm.qosannotations.qos_performance.QosPerformanceFactory
import de.uka.ipd.sdq.pcm.repository.ProvidedRole
import de.uka.ipd.sdq.pcm.repository.OperationProvidedRole
import de.uka.ipd.sdq.pcm.qosannotations.SpecifiedQoSAnnotation
import de.uka.ipd.sdq.pcm.qosannotations.QosannotationsFactory
import de.uka.ipd.sdq.pcm.seff.InternalAction
import de.uka.ipd.sdq.pcm.core.PCMRandomVariable

class BlackBox2Component{
	public BasicComponent component;
	public ResourceDemandingSEFF seff;
	new (BasicComponent component, ResourceDemandingSEFF seff){
		this.component = component;
		this.seff = seff;
	}
}

 


class DeploymentTransformator {
	private static final Log log = LogFactory.getLog(DeploymentTransformator);
	var de.uka.ipd.sdq.pcm.system.System transformedSystem = null; 
	Transformator transformator;
	new(Transformator transformator) {
		this.transformator = transformator;
	}
	
	/**
	 * Transform Deployment
	 */
	def create pcmAllocation:AllocationFactory.eINSTANCE.createAllocation transformDeployment(Deployment dmlDeployment){
		log.info("Transforming Deplyoment "+dmlDeployment.name+" to Allocation");
		pcmAllocation.id = dmlDeployment.id;
		pcmAllocation.entityName = dmlDeployment.name;
		pcmAllocation.targetResourceEnvironment_Allocation = transformator.resourceLandscapeTransformator.getResourceEnvironmentById(dmlDeployment.targetResourceLandscape.id);		
		pcmAllocation.system_Allocation = transformator.repositoryTransformator.transformSystem(dmlDeployment.system);
		transformedSystem = pcmAllocation.system_Allocation;
		for(dmldeploymentcontext : dmlDeployment.deploymentContexts){
			pcmAllocation.allocationContexts_Allocation.add(transformDeploymentContext(dmldeploymentcontext));
		}		
		addDelayResourceToResourceContainer(pcmAllocation.allocationContexts_Allocation);
		
	}	
	/**
	 * Transform DeploymentContext
	 */
	def create pcmAllocationContext:AllocationFactory.eINSTANCE.createAllocationContext transformDeploymentContext(DeploymentContext dmlDeplyomentcontext){
		log.info("Transforming DeplyomentContext "+dmlDeplyomentcontext.name+" to AllocationContext");
		pcmAllocationContext.id = dmlDeplyomentcontext.id;
		pcmAllocationContext.entityName = dmlDeplyomentcontext.name;
		pcmAllocationContext.allocation_AllocationContext = transformDeployment(dmlDeplyomentcontext.deployment);
		pcmAllocationContext.resourceContainer_AllocationContext = transformator.resourceLandscapeTransformator.getResourceContainer(dmlDeplyomentcontext.resourceContainer);
		pcmAllocationContext.assemblyContext_AllocationContext = transformator.repositoryTransformator.transformAssemblyContext(dmlDeplyomentcontext.assemblyContext);
	}
	
	/**
	 * add Components with transformed BlackBoxBehavior (called from RepositoryTransformator)
	 */
	var componentsWithBlackBoxBehavior = new ArrayList<BlackBox2Component>();
	
	
	
	public def void addBlackBoxComponent(BasicComponent component, ResourceDemandingSEFF seff){
		componentsWithBlackBoxBehavior.add(new tools.descartes.dml2pcm.BlackBox2Component(component,seff));
	}
	
	
	
	def void addDelayResourceToResourceContainer(EList<AllocationContext> allocations){
		var addDelay = true;
		if(componentsWithBlackBoxBehavior.size==0) return;
		
		//TODO: Get User Input if resouce containers should be changed
		//set delay accordingly
				
		if(addDelay==false){
			removeBlackBoxDelays();
			return;
		}		
		
		for(bc:componentsWithBlackBoxBehavior){
			
			
			
		}
		
		while(componentsWithBlackBoxBehavior.size()>0){
			val bc = componentsWithBlackBoxBehavior.remove(0);
			var doubles = componentsWithBlackBoxBehavior.filter[it.component == bc.component];
			componentsWithBlackBoxBehavior.removeAll(doubles);
			lookUpAllocationOfComponent(bc.component, allocations);
		}		
	}
	
	/**
	 * remove the allready implemented InternalActions for Delay
	 */
	def void removeBlackBoxDelays(){
		while(componentsWithBlackBoxBehavior.size()>0){
			var bc = componentsWithBlackBoxBehavior.remove(0);
			transformator.behaviorTransformator.removeResourceDemandOfSeff(bc.seff);
		}
	}
	
	/**
	 * go through AllocationContexts and see if the Component with transformed BlackBoxBehavior was deployed there
	 * add Delay if yes
	 */
	def void lookUpAllocationOfComponent(BasicComponent toSearch, EList<AllocationContext> allocations){
		for(allocation: allocations){
			var component = allocation.assemblyContext_AllocationContext.encapsulatedComponent__AssemblyContext;
			if(lookUpComponentInComposition(toSearch,component)>0){
				log.info("Adding Delay ResourceType to "+allocation.resourceContainer_AllocationContext.entityName);
				transformator.resourceLandscapeTransformator.addDelayResourceToContainer(allocation.resourceContainer_AllocationContext);
			}
		}		
	}
	
	/**
	 * Helperfunction that goes through the deployed compositecomponents and looks for the given BasicComponent
	 * returns count of occurences
	 */
	def int lookUpComponentInComposition(BasicComponent toSearch, RepositoryComponent cc){
		var instances = 0;
		if(cc instanceof CompositeComponent){
			for(assembly: cc.assemblyContexts__ComposedStructure){
				var innercomponent = assembly.encapsulatedComponent__AssemblyContext
				var systemspecifiedexecutiontime = QosPerformanceFactory.eINSTANCE.createComponentSpecifiedExecutionTime();
				if(innercomponent instanceof CompositeComponent){										
					instances += lookUpComponentInComposition(toSearch,innercomponent);
				}
				else if(innercomponent instanceof BasicComponent){					
					if(innercomponent.id == toSearch.id){
						systemspecifiedexecutiontime.assemblyContext_ComponentSpecifiedExecutionTime = assembly;
						
						instances++;
					}
				}
			}
		}
		else if(cc instanceof BasicComponent){
			if(cc.id == toSearch.id){
				instances++;	
			}
		}
		return instances;
	}
	
	def void searchForAssemblyContextOfComponent(BasicComponent toSearch, RepositoryComponent cc,ResourceDemandingSEFF seff){
		if(cc instanceof CompositeComponent){
			for(assembly: cc.assemblyContexts__ComposedStructure){
				var innercomponent = assembly.encapsulatedComponent__AssemblyContext
				var systemspecifiedexecutiontime = QosPerformanceFactory.eINSTANCE.createComponentSpecifiedExecutionTime();
				if(innercomponent instanceof CompositeComponent){										
					lookUpComponentInComposition(toSearch,innercomponent);
				}
				else if(innercomponent instanceof BasicComponent){					
					if(innercomponent.id == toSearch.id){
						addQoSTime(assembly,seff);
						
					}
				}
			}
		}			
	}
	def void addQoSTime(de.uka.ipd.sdq.pcm.core.composition.AssemblyContext assembly, ResourceDemandingSEFF seff){
		var componentspecifiedexecutiontime = QosPerformanceFactory.eINSTANCE.createComponentSpecifiedExecutionTime();
		componentspecifiedexecutiontime.assemblyContext_ComponentSpecifiedExecutionTime = assembly;
		componentspecifiedexecutiontime.signature_SpecifiedQoSAnnation = seff.describedService__SEFF;
		var component = assembly.encapsulatedComponent__AssemblyContext;
		var boolean roleFound = false;
		for(role:component.providedRoles_InterfaceProvidingEntity){			
			if(!roleFound){
				if(role instanceof OperationProvidedRole){
					var signature = seff.describedService__SEFF;
					if(signature instanceof OperationSignature){
							role.providedInterface__OperationProvidedRole == signature.interface__OperationSignature{
							roleFound = true;
							componentspecifiedexecutiontime.role_SpecifiedQoSAnnotation = role;
						}
					}				
				}
			}
			var PCMRandomVariable specification = null;
			for(step:seff.steps_Behaviour){
				if(step instanceof InternalAction){
					specification = step.resourceDemand_Action.get(0).specification_ParametericResourceDemand;
				}
			}
			componentspecifiedexecutiontime.specification_SpecifiedExecutionTime = specification;
		}
		if(transformedSystem.qosAnnotations_System.size()==0){
			var qosAnnotations = QosannotationsFactory.eINSTANCE.createQoSAnnotations;
			transformedSystem.qosAnnotations_System.add(qosAnnotations);
		}		
		transformedSystem.qosAnnotations_System.get(0).specifiedQoSAnnotations_QoSAnnotations.add(componentspecifiedexecutiontime);		
	}
	
	
}