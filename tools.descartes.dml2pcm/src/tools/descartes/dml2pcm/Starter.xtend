package tools.descartes.dml2pcm

import java.io.File
import java.util.ArrayList
import javax.swing.SwingUtilities
import javax.swing.UIManager
import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import tools.descartes.dml2pcm.gui.FileChooser
import org.apache.commons.cli.Options
import org.apache.commons.cli.CommandLine
import org.apache.commons.cli.CommandLineParser
import org.apache.commons.cli.BasicParser
import java.util.HashMap
import org.apache.commons.cli.Option

class Starter {

	def static void main(String[] args) {
		
		var Options options = new Options();		
		// add t option
		options.addOption("gui",false,"use graphical user interface");
		options.addOption("source",true,"location of DML model");
		options.addOption("target", true, "location to save the PCM model");
		var CommandLineParser parser = new BasicParser();
		var CommandLine cmd = parser.parse(options, args);
		for(opt :cmd.options){
			println(opt)
		}
		
		
		if(cmd.hasOption("gui")){
			new Starter().run();
			
			
		}
		else{
			var String pathToDml = cmd.getOptionValue("source");
			println(pathToDml)
			var pathToPcm = cmd.getOptionValue("target");
			var pcm = new File(pathToPcm);
			var filesEval = validateDMLPath(pathToDml);
			var deployment = filesEval.get("deployment");
			var usage = filesEval.get("usageprofile");
			var filesForTransformation = #[deployment,usage,pcm]; 
			startTransformation(filesForTransformation);
		}
		
	}

	//String pathToDML;
	private static final Log log = LogFactory.getLog(Starter);
	private static String[] modules = #["repository", "deployment", "usageprofile", "resourcelandscape", "system"];

	def void run() {
		
		SwingUtilities.invokeLater(
			new Runnable() {
				override void run() {
					UIManager.put("swing.boldMetal", Boolean.FALSE);
					FileChooser.createAndShowGUI();
				}
			});
	}

	public static def pathFromGui(File[] files) {
		startTransformation(files);
	}

	static def void startTransformation(File[] files) {
		log.info("Starting transformation");
		var modelio = new ModelIO();
		modelio.loadModel(files);
		log.info("Transformation finished");
		//println("done running");
	}
	
	
	static def HashMap<String,File> validateDMLPath(String path){
		var files = new HashMap<String,File>();
		var File f = new File(path);
		if(f.directory){
			var deploymentList = new ArrayList<File>();
			for(file:f.listFiles){
				if(!file.directory && file.name.toLowerCase.endsWith(".deployment")){
					deploymentList.add(file);
				}
			}
			if(deploymentList.size()>=1){
				var deploymentused = deploymentList.get(0);
				var filesDepl = new ModelIO().getDeploymentReferences(deploymentused);
				
				files.put("deployment",deploymentused);				
				files.put("resourcelandscape",filesDepl.get(0));
				files.put("system",filesDepl.get(1));					
				files.put("repository",filesDepl.get(2))
			}
			else{
				
			}	
			
			var usageList = new ArrayList<File>();
			for(file:f.listFiles){
				if(!file.directory && file.name.toLowerCase.endsWith(".usageprofile")){
					usageList.add(file);
				}
			}
			
			if(usageList.size()>=1){
				var usage = usageList.get(0);
				files.put("usageprofile",usage);				
			}
			
					
		}
		return files;
	}
	

}
