package tools.descartes.dml2pcm

import java.util.ArrayList
import edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.FineGrainedBehavior
import de.uka.ipd.sdq.pcm.seff.ResourceDemandingSEFF
import de.uka.ipd.sdq.pcm.seff.SeffFactory
import edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.ComponentInternalBehavior
import de.uka.ipd.sdq.pcm.seff.ResourceDemandingBehaviour
import de.uka.ipd.sdq.pcm.seff.AbstractAction
import org.eclipse.emf.common.util.EList
import edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.ForkAction
import edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.InternalAction
import de.uka.ipd.sdq.pcm.seff.seff_performance.SeffPerformanceFactory
import edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.ResourceDemand
import org.eclipse.emf.common.util.BasicEList
import edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.AcquireAction
import edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.ReleaseAction
import edu.kit.ipd.descartes.mm.applicationlevel.parameterdependencies.ModelVariableCharacterizationType
import edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.LoopAction
import de.uka.ipd.sdq.pcm.core.CoreFactory
import edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.ExternalCallAction
import de.uka.ipd.sdq.pcm.repository.OperationSignature
import edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.CoarseGrainedBehavior
import edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.ExternalCallFrequency
import de.uka.ipd.sdq.pcm.resourcetype.ProcessingResourceType
import java.util.List
import edu.kit.ipd.descartes.mm.applicationlevel.parameterdependencies.CallParameter
import de.uka.ipd.sdq.pcm.parameter.ParameterFactory
import de.uka.ipd.sdq.stoex.StoexFactory
import edu.kit.ipd.descartes.mm.applicationlevel.parameterdependencies.ParameterCharacterizationType
import de.uka.ipd.sdq.pcm.parameter.VariableCharacterisationType
import edu.kit.ipd.descartes.mm.applicationlevel.parameterdependencies.InfluencingParameter
import edu.kit.ipd.descartes.mm.applicationlevel.parameterdependencies.ServiceInputParameter
import edu.kit.ipd.descartes.mm.applicationlevel.parameterdependencies.ExternalCallParameter
import edu.kit.ipd.descartes.mm.applicationlevel.parameterdependencies.ExternalCallReturnParameter
import org.apache.commons.logging.LogFactory
import org.apache.commons.logging.Log
import edu.kit.ipd.descartes.mm.applicationlevel.parameterdependencies.ShadowParameter
import edu.kit.ipd.descartes.mm.applicationlevel.repository.BasicComponent
import edu.kit.ipd.descartes.mm.applicationlevel.parameterdependencies.DependencyRelationship
import edu.kit.ipd.descartes.mm.applicationlevel.parameterdependencies.RelationshipCharacterizationType
import edu.kit.ipd.descartes.mm.applicationlevel.parameterdependencies.InfluencedVariableReference
import edu.kit.ipd.descartes.mm.applicationlevel.functions.RandomVariable
import edu.kit.ipd.descartes.mm.applicationlevel.parameterdependencies.DependencyPropagationRelationship
import edu.kit.ipd.descartes.mm.applicationlevel.parameterdependencies.ModelVariable
import org.eclipse.emf.ecore.EObject
import de.uka.ipd.sdq.pcm.seff.ProbabilisticBranchTransition
import de.uka.ipd.sdq.pcm.seff.GuardedBranchTransition
import edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.BranchAction


class BehaviorTransformator {
	private static final Log log = LogFactory.getLog(BehaviorTransformator);
	int forkActionCounter = 1;

	
	new(){
		
	}
	
	Transformator transformator;
	new(Transformator transformator) {
		this.transformator = transformator;
	}
	
	/**
	 * Transform ForkAction
	 */
	def create pcmForkAction:SeffFactory.eINSTANCE.createForkAction transformForkAction(ForkAction dmlForkAction) {

		//if synchronized add SynchronisationPoint to pcmForkAction and add all Behaviors to that
		pcmForkAction.entityName = "Transformed Fork Action " + forkActionCounter++;
		if (dmlForkAction.withSynchronizationBarrier == true) {
			log.info(getLogString("ForkAction", null, null, "ForkAction") + " with Synchroniation");
			var pcmSyncPoint = SeffFactory.eINSTANCE.createSynchronisationPoint;
			pcmForkAction.synchronisingBehaviours_ForkAction = pcmSyncPoint;
			for (dmlForkbehavior : dmlForkAction.forkedBehaviors) {
				pcmSyncPoint.synchronousForkedBehaviours_SynchronisationPoint.add(
					transformForkedBehavior(dmlForkbehavior));
			}
			return;
		}
		log.info(getLogString("ForkAction", null, null, "ForkAction") + " without Synchroniation");

		//else if no sync
		for (dmlForkbehavior : dmlForkAction.forkedBehaviors) {
			pcmForkAction.asynchronousForkedBehaviours_ForkAction.add(transformForkedBehavior(dmlForkbehavior));
		}
	}

	def create pcmForkedBehaviour:SeffFactory.eINSTANCE.createForkedBehaviour transformForkedBehavior(
		ComponentInternalBehavior dmlForkedBehavior) {
		log.info(getLogString("forked ComponentInternalBehavior", null, null, "ForkedBehavior"));
		transformActionList(dmlForkedBehavior.actions, pcmForkedBehaviour.steps_Behaviour);
	}

	int aquireActionsCounter = 1;

	def create pcmAcquireAction:SeffFactory.eINSTANCE.createAcquireAction transformAcquireAction(
		AcquireAction dmlAcquireAction) {
		log.info(
			getLogString("AquireAction", null, null, "AquireAction") + " of Semaphore " +
				dmlAcquireAction.semaphore?.name);
		pcmAcquireAction.id = "Transformed AquireAction" + aquireActionsCounter++;
		var passResource = transformator.repositoryTransformator.getPassiveResourceFromSemaphore(dmlAcquireAction.semaphore);
		pcmAcquireAction.passiveresource_AcquireAction = passResource;
	}

	int releaseActionsCounter = 1;

	def create pcmReleaseAciton:SeffFactory.eINSTANCE.createReleaseAction transformReleaseAction(
		ReleaseAction dmlReleaseAction) {
		log.info(
			getLogString("ReleaseAction", null, null, "ReleaseAction") + " of Semaphore " +
				dmlReleaseAction.semaphore?.name);
		pcmReleaseAciton.entityName = "Transformed ReleaseAction" + releaseActionsCounter++;
		var passResource = transformator.repositoryTransformator.getPassiveResourceFromSemaphore(dmlReleaseAction.semaphore);
		pcmReleaseAciton.passiveResource_ReleaseAction = passResource;
	}

	int counterInternalActions = 1;

	def create pcmInternalAction:SeffFactory.eINSTANCE.createInternalAction transformInternalAction(
		InternalAction dmlInternalAction) {
		log.info(getLogString("InternalAction", null, null, "InternalAction"));
		pcmInternalAction.entityName = "Transformed InternalAction " + counterInternalActions++;
		for (dmlResourceDemand : dmlInternalAction.resourceDemand) {
			pcmInternalAction.resourceDemand_Action.add(transformResourceDemand(dmlResourceDemand));
		}
	}

	def create pcmParametricResourceDemand:SeffPerformanceFactory.eINSTANCE.createParametricResourceDemand transformResourceDemand(
		ResourceDemand dmlResourceDemand) {
		log.info(
			getLogString("ResourceDemand", null, null, "ParametricResourceDemand") + " of ResourceType" +
				dmlResourceDemand.resourceType?.name);
		pcmParametricResourceDemand.requiredResource_ParametricResourceDemand = transformator.resourceLandscapeTransformator.
			getResourceType(dmlResourceDemand.resourceType) as ProcessingResourceType;
		if (dmlResourceDemand.characterization == ModelVariableCharacterizationType.EXPLICIT) {
			pcmParametricResourceDemand.specification_ParametericResourceDemand = new StoexTransformator().
				transformRandomVariable(dmlResourceDemand.explicitDescription);
		} else {
			pcmParametricResourceDemand.specification_ParametericResourceDemand = new StoexTransformator().
				transformRandomVariable(null as RandomVariable);
			log.info("ResourceDemand specified EMPIRICAL and can not be transformed, Demand set to zero");
		}
	}

	int loopActionCounter = 1;

	def create pcmLoopAction:SeffFactory.eINSTANCE.createLoopAction transformLoopAction(LoopAction dmlInternalAction) {
		log.info(getLogString("LoopAction", null, null, "LoopAction"));
		pcmLoopAction.entityName = "Transformed Loop Action" + loopActionCounter++;
		if (dmlInternalAction.loopIterationCount.characterization == ModelVariableCharacterizationType.EXPLICIT) {
			pcmLoopAction.iterationCount_LoopAction = new StoexTransformator().
				transformRandomVariable(dmlInternalAction.loopIterationCount.explicitDescription);
		} else {
			pcmLoopAction.iterationCount_LoopAction = new StoexTransformator().
				transformRandomVariable(null as RandomVariable);
			log.info(
				"LoopIterations of LoopAction are specified as EMPIRICAL and can not be transformed, Iterations set to zero");
		}
		log.info("Transforming loop body")
		pcmLoopAction.bodyBehaviour_Loop = transformComponentInternalBehavior(dmlInternalAction.loopBodyBehavior, null);
	}

	int externalCallActionCounter = 1;

	def create pcmExternalCallAction:SeffFactory.eINSTANCE.createExternalCallAction transformExternalCallAction(
		ExternalCallAction dmlExternalCallAction) {
		log.info(
			getLogString("ExternalCallAction", null, null, "ExternalCallAction") + " to Service " +
				dmlExternalCallAction.externalCall.signature.name);
		pcmExternalCallAction.entityName = "Transformed ExternalCall Action " + externalCallActionCounter++;
		pcmExternalCallAction.calledService_ExternalService = transformator.repositoryTransformator.
			getTransformedOperationSignature(dmlExternalCallAction.externalCall.signature) as OperationSignature;
		pcmExternalCallAction.role_ExternalService = transformator.repositoryTransformator.
			getTransformedRequiredRole(dmlExternalCallAction.externalCall.interfaceRequiringRole);

	//TODO: paramter usage etc. pcmExternalCallAction.inputVariableUsages__CallAction
	}

	def create pcmBranchAction:SeffFactory.eINSTANCE.createBranchAction transformBranchAction(
		BranchAction dmlBranchAction) {
		log.info(getLogString("BranchAction", null, null, "BranchAction"));
		var probabilities = null as ArrayList<Double>;
		if (dmlBranchAction.branchProbabilities.characterization == ModelVariableCharacterizationType.EXPLICIT) {
			probabilities = new StoexTransformator().
				transformRandomVariableToListOfProbabilities(dmlBranchAction.branchProbabilities.explicitDescription);
		}

		var counter = 0;
		for (dmlBranch : dmlBranchAction.branches) {
			var pcmProbBranchTransition = SeffFactory.eINSTANCE.createProbabilisticBranchTransition
			pcmProbBranchTransition.branchBehaviour_BranchTransition = transformComponentInternalBehavior(dmlBranch,
				null);
			if (probabilities != null && probabilities.size < counter) {
				pcmProbBranchTransition.branchProbability = probabilities.get(counter++);
			} else {
				pcmProbBranchTransition.branchProbability = 0;
				log.info("no branch probability found for branch action, probability set to 0");
			}

		}
	}

	def de.uka.ipd.sdq.pcm.seff.AbstractAction transformAction(
		edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.AbstractAction dmlAbstractAction) {

		//PCM InternalCallAction: (via ResourceDemandingInternalBehaviour) Class representing component-internal 
		//behaviour not accessible from the component's interface. Comparable to internal method in object-oriented 
		//programming. This behaviour can be called from within a resource demanding behaviour using an InternalCallAction.
		switch (dmlAbstractAction) {
			AcquireAction:
				return (transformAcquireAction(dmlAbstractAction))
			ReleaseAction:
				return (transformReleaseAction(dmlAbstractAction))
			InternalAction:
				return (transformInternalAction(dmlAbstractAction))
			ForkAction:
				return (transformForkAction(dmlAbstractAction))
			LoopAction:
				return (transformLoopAction(dmlAbstractAction))
			ExternalCallAction:
				return (transformExternalCallAction(dmlAbstractAction))
			BranchAction:
				return (transformBranchAction(dmlAbstractAction))
		}

		log.info("type of Abstract action: " + dmlAbstractAction.toString() + " not recognized");
		return null;
	}

	def void transformActionList(
		EList<edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.AbstractAction> dmlActions,
		EList<AbstractAction> pcmActions) {
		val startAction = de.uka.ipd.sdq.pcm.seff.SeffFactory.eINSTANCE.createStartAction();
		//startAction.id = "StartAction " + compInternTransform;
		startAction.entityName = "StartAction " + compInternTransform;
		val stopAction = de.uka.ipd.sdq.pcm.seff.SeffFactory.eINSTANCE.createStopAction();
		//stopAction.id = "StopAction " + compInternTransform;
		stopAction.entityName = "StopAction " + compInternTransform;
		compInternTransform++;
		pcmActions.add(startAction);
		pcmActions.add(stopAction);
		var AbstractAction pcmActionPred = startAction;
		for (action : dmlActions) {
			var pcmAction = transformAction(action);
			pcmAction.predecessor_AbstractAction = pcmActionPred;
			pcmActionPred.successor_AbstractAction = pcmAction;
			pcmActionPred = pcmAction as AbstractAction;
			pcmActions.add(pcmAction);
		}
		pcmActionPred.successor_AbstractAction = stopAction;
		stopAction.predecessor_AbstractAction = pcmActionPred;

	}

	int compInternTransform = 1;

	def de.uka.ipd.sdq.pcm.seff.ResourceDemandingBehaviour transformComponentInternalBehavior(
		ComponentInternalBehavior dmlInternalBehaviour, ResourceDemandingBehaviour transform) {
		var pcmResourceDemBehav = transform;
		if (transform == null) {
			pcmResourceDemBehav = de.uka.ipd.sdq.pcm.seff.SeffFactory.eINSTANCE.createResourceDemandingBehaviour;
		}
		transformActionList(dmlInternalBehaviour.actions, pcmResourceDemBehav.steps_Behaviour);
		return pcmResourceDemBehav;
	}

	/**
	 * Transform FineGrainedBehavior
	 */
	int pcmSEFFid = 0;

	def create pcmResourceDemandingSEFF:SeffFactory.eINSTANCE.createResourceDemandingSEFF transformFineGrainedBehavior(
		FineGrainedBehavior dmlFGB) {
		log.info(getLogString("FineGrainedBehavior with Signature "+dmlFGB.describedSignature?.id, null, null, "ResourceDemandingSEFF"));
		
		//pcmResourceDemandingSEFF.en = "transformFGB" + (pcmSEFFid++);
		if (dmlFGB.behavior != null) {
			transformActionList(dmlFGB.behavior.actions, pcmResourceDemandingSEFF.steps_Behaviour);
		}
		pcmResourceDemandingSEFF.describedService__SEFF = transformator.repositoryTransformator.
			getTransformedOperationSignature(dmlFGB.describedSignature);

		//Transform explicit Relationships that exist within the BasicComponent
		val component = dmlFGB.eContainer as BasicComponent;
		for (influencingparameter : dmlFGB.influencingParameter) {
			if (influencingparameter instanceof CallParameter) {
				var String specification = null;
				for (relationship : component.relationships) {
					if (relationship instanceof DependencyPropagationRelationship) {
						if (relationship.characterization == RelationshipCharacterizationType.EMPIRICAL) {
							//log.info("empirical relationship can not be transformed");
						} else {
							if (relationship.dependent == influencingparameter) {
								if (relationship.characterization == RelationshipCharacterizationType.IDENTITY) {
									specification = getVariableName(relationship.independent.get(0));
								} else if (relationship.characterization == RelationshipCharacterizationType.EXPLICIT) {
									specification = new StoexTransformator().
										expressionToSpecification(relationship.explicitDescription.expression);
								}
							}
						}
					}
				}
				if (specification != null) {
					switch (influencingparameter) {
						//ServiceInputParameter: should not change as it is input
						ExternalCallParameter:
							assignVariableUseToExternalCall(influencingparameter, dmlFGB, specification)
						ExternalCallReturnParameter:
							assignVariableUseToExternalCallReturn(influencingparameter, dmlFGB, specification)
						default:
							println("do nothing")
					}
				}

			} else {
				log.info("Transformation of ShadowParameter not supported");
			}
		}
		//var listofrelationships = component.relationships?.filter[it instanceof DependencyRelationship && it.characterization != RelationshipCharacterizationType.EMPIRICAL]
		for (influencedvariablereference : dmlFGB.influencedVariableReference) {
			for (relationship : component.relationships) {
				if (relationship instanceof DependencyRelationship) {
					if (relationship.characterization == RelationshipCharacterizationType.EMPIRICAL) {
						log.info("empirical relationship can not be transformed");
					} else {
						if (relationship.dependent == influencedvariablereference) {
							if (relationship.characterization == RelationshipCharacterizationType.IDENTITY) {
								var specification = getVariableName(relationship.independent.get(0));
								assignSpecificationToInfluencedVariable(specification, influencedvariablereference);
							} else if (relationship.characterization == RelationshipCharacterizationType.EXPLICIT) {
								var specification = new StoexTransformator().
									expressionToSpecification(relationship.explicitDescription.expression);
								assignSpecificationToInfluencedVariable(specification, influencedvariablereference);
							}
						}
					}
				}
			}
		}
	}

	def assignSpecificationToInfluencedVariable(String specification, InfluencedVariableReference ivr) {
		if (ivr.resourceDemand != null) {
			var pcmParametricResourceDemand = transformResourceDemand(ivr.resourceDemand);
			pcmParametricResourceDemand.specification_ParametericResourceDemand.specification = specification;
		} else if (ivr.controlFlowVariable != null) {
			log.info("Transformation of ControlFlowVariable Dependency not implemented");
			var dmlControlFlowActions = new ArrayList<AbstractAction>();
			var fgb = ivr.eContainer as FineGrainedBehavior;
			extractControlFlowActions(dmlControlFlowActions, fgb.behavior.actions);
			for(controlflow:dmlControlFlowActions){
				switch(controlflow){
					LoopAction: changeLoopVariable(controlflow, specification, ivr)
					BranchAction: changeBranchProbabilities(controlflow,specification, ivr)
				}
			}
			
			
			
		}
	}
	
	def changeBranchProbabilities(BranchAction branchAction, String specification, InfluencedVariableReference ivr) {
		if(branchAction.branchProbabilities != ivr.controlFlowVariable){
			return;
		}
		var pcmBranchAction = transformAction(branchAction) as de.uka.ipd.sdq.pcm.seff.BranchAction;
		var guardedBranches = new ArrayList<GuardedBranchTransition>();
		for(branch:pcmBranchAction.branches_Branch){
			if(branch instanceof ProbabilisticBranchTransition){
				var guardedBranch = SeffFactory.eINSTANCE.createGuardedBranchTransition;
				guardedBranch.branchBehaviour_BranchTransition = branch.branchBehaviour_BranchTransition;
				guardedBranches.add(guardedBranch);
			}			
		}
		pcmBranchAction.branches_Branch.clear();
		for(guardedBranch:guardedBranches){
			pcmBranchAction.branches_Branch.add(guardedBranch);
		}
		
		log.info("transformed branchAction into GuardedBranchAction with "+guardedBranches.size() +" branches");
		log.info("PCM expects the probability of one branch the expression to evaluate to true or false");
		log.info("DML Specification is: "+specification);
				
		for(guardedBranch:guardedBranches){
			//ask user to set by hand;
			guardedBranch.branchCondition_GuardedBranchTransition.specification = "ask the user";
		}
		
	}
	
	def changeLoopVariable(LoopAction loopAction, String specification, InfluencedVariableReference ivr) {
		if(loopAction.loopIterationCount != ivr.controlFlowVariable){
			return;
		}
		var pcmLoopAction = transformAction(loopAction) as de.uka.ipd.sdq.pcm.seff.LoopAction;
		pcmLoopAction.iterationCount_LoopAction.specification = specification;
		
	}

	def String getVariableName(InfluencingParameter ip) {
		if (ip instanceof CallParameter) {
			return (ip.parameter.name);
		} else
			return "(0)";
	}

	def void assignVariableUseToServiceInput(ServiceInputParameter sip, FineGrainedBehavior fgb) {
	}

	def void assignVariableUseToExternalCallReturn(ExternalCallReturnParameter ecrp, FineGrainedBehavior fgb,
		String specification) {
		var pcmVariableUse = transformInfluencingParameter(ecrp, specification);
		var dmlExternalactions = new ArrayList<ExternalCallAction>();
		extractExternalCallActions(dmlExternalactions, fgb.behavior.actions);
		for (action : dmlExternalactions) {
			if (action.externalCall == ecrp.externalCall) {
				transformExternalCallAction(action).returnVariableUsage__CallReturnAction.add(pcmVariableUse);

			}
		}

	}

	def void assignVariableUseToExternalCall(ExternalCallParameter ecp, FineGrainedBehavior fgb, String specification) {
		var pcmVariableUse = transformInfluencingParameter(ecp, specification);
		var dmlExternalactions = new ArrayList<ExternalCallAction>();
		extractExternalCallActions(dmlExternalactions, fgb.behavior.actions);
		for (action : dmlExternalactions) {
			if (action.externalCall == ecp.externallCall) {
				transformExternalCallAction(action).inputVariableUsages__CallAction.add(pcmVariableUse);
			}
		}

	}
	
	/**
	 * Get all loop and branch actions in a behavior
	 */
	def void extractControlFlowActions(ArrayList<AbstractAction> controlFlowActions,
		EList<edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.AbstractAction> steps) {
		for (action : steps) {
			if (action instanceof LoopAction || action instanceof BranchAction) {
				controlFlowActions.add(action as AbstractAction);				
			}
			if (action instanceof LoopAction) {
				extractControlFlowActions(controlFlowActions, action.loopBodyBehavior.actions)
			} else if (action instanceof edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.BranchAction) {
				for (branch : action.branches) {
					extractControlFlowActions(controlFlowActions, branch.actions);
				}
			} else if (action instanceof ForkAction) {
				for (fork : action.forkedBehaviors) {
					extractControlFlowActions(controlFlowActions, fork.actions);
				}
			}
		}

	}
	
	
	/**
	 * Get all external call actions in a behavior
	 */
	def void extractExternalCallActions(ArrayList<ExternalCallAction> externalactions,
		EList<edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.AbstractAction> steps) {
		for (action : steps) {
			if (action instanceof ExternalCallAction) {
				externalactions.add(action);
			} else if (action instanceof LoopAction) {
				extractExternalCallActions(externalactions, action.loopBodyBehavior.actions)
			} else if (action instanceof edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.BranchAction) {
				for (branch : action.branches) {
					extractExternalCallActions(externalactions, branch.actions);
				}
			} else if (action instanceof ForkAction) {
				for (fork : action.forkedBehaviors) {
					extractExternalCallActions(externalactions, fork.actions);
				}
			}
		}

	}

	def create varuse:ParameterFactory.eINSTANCE.createVariableUsage transformInfluencingParameter(
		InfluencingParameter ip, String specification) {
		var String name = null;
		switch (ip) {
			CallParameter: name = ip.parameter.name
			ShadowParameter: name = ip.name
			default: name = null
		}
		log.info(getLogString("InfluencingParameter", name, null, "VariableUsage"));

		var pcmVarCharacterisation = ParameterFactory.eINSTANCE.createVariableCharacterisation;
		var pcmVarRef = StoexFactory.eINSTANCE.createVariableReference; //ParameterFactory.eINSTANCE
		varuse.variableCharacterisation_VariableUsage.add(pcmVarCharacterisation);
		varuse.namedReference__VariableUsage = pcmVarRef;
		if (ip instanceof CallParameter) {
			if (ip.characterization == ParameterCharacterizationType.VALUE) {
				pcmVarCharacterisation.type = VariableCharacterisationType.VALUE;
			} else if (ip.characterization == ParameterCharacterizationType.NUMBER_OF_ELEMENTS) {
				pcmVarCharacterisation.type = VariableCharacterisationType.NUMBER_OF_ELEMENTS;
			}
			pcmVarRef.referenceName = ip.parameter.name;
			pcmVarCharacterisation.specification_VariableCharacterisation = CoreFactory.eINSTANCE.
				createPCMRandomVariable();
			pcmVarCharacterisation.specification_VariableCharacterisation.specification = specification;
		} else if (ip instanceof ShadowParameter) {
			log.info("Transformation of ShadowParameter " + ip.name + " not implemented");
		} else {
			log.info("Unknown Type of Influencing Parameter " + ip.toString);
		}
	}

	/**
	 * Transform CoarseGrainedBehavior
	 * resourceDemand->interalAction
	 * externalCallFrequency->BranchAction
	 */
	def create pcmResourceDemandingSEFF:SeffFactory.eINSTANCE.createResourceDemandingSEFF transformCoarseGrainedBehavior(
		CoarseGrainedBehavior dmlCGB) {
		log.info(
			getLogString("CoarseGrainedBehavior with Signature " + dmlCGB.describedSignature?.name, null, null,
				"ResourceDemandingSEFF"));
		val startAction = de.uka.ipd.sdq.pcm.seff.SeffFactory.eINSTANCE.createStartAction();
		startAction.entityName = "StartAction " + compInternTransform;
		val stopAction = de.uka.ipd.sdq.pcm.seff.SeffFactory.eINSTANCE.createStopAction();
		stopAction.entityName = "StopAction " + compInternTransform;
		compInternTransform++;
		pcmResourceDemandingSEFF.steps_Behaviour.add(startAction);
		pcmResourceDemandingSEFF.steps_Behaviour.add(stopAction);
		var pcmActionPred = startAction as AbstractAction;
		for (externalCallFrequency : dmlCGB.externalCallFrequency) {
			var pcmExternalCallAction = transformExternalCallFrequencyToLoopAction(externalCallFrequency);
			pcmActionPred.successor_AbstractAction = pcmExternalCallAction;
			pcmExternalCallAction.predecessor_AbstractAction = pcmActionPred;
			pcmActionPred = pcmExternalCallAction;
			pcmResourceDemandingSEFF.steps_Behaviour.add(pcmExternalCallAction);
		}
		if (dmlCGB.resourceDemand != null && dmlCGB.resourceDemand.size > 0) {

			//PCM: "An Internal-Action may have at most one resource demand for eachProcessingResourceType" (kozpar08, p125)
			//don't know about dml CoarseGrainedBehavior so resourcedemands are transformed into a list of actions
			//the function returns the first, predecessor/successor are set for elements within the list
			var resourceDemands = new ArrayList<ResourceDemand>();
			resourceDemands.addAll(dmlCGB.resourceDemand);
			var pcmInternalCallAction = transformResourceDemandsToActions(resourceDemands,resourceDemands.size);
			pcmInternalCallAction.predecessor_AbstractAction = pcmActionPred;
			pcmActionPred.successor_AbstractAction = pcmInternalCallAction;
			pcmResourceDemandingSEFF.steps_Behaviour.add(pcmInternalCallAction);
			while (pcmInternalCallAction.successor_AbstractAction != null) {					
				pcmResourceDemandingSEFF.steps_Behaviour.add(pcmInternalCallAction.successor_AbstractAction);
				pcmInternalCallAction = pcmInternalCallAction.successor_AbstractAction as de.uka.ipd.sdq.pcm.seff.InternalAction;
				
			}
			
			
			//pcmInternalCallAction.predecessor_AbstractAction = pcmActionPred;
			//pcmActionPred.successor_AbstractAction = pcmInternalCallAction;
			pcmActionPred = pcmInternalCallAction;
		}
		stopAction.predecessor_AbstractAction = pcmActionPred;
		pcmActionPred.successor_AbstractAction = stopAction as AbstractAction;
		
		var aa = startAction as AbstractAction;
		
		pcmResourceDemandingSEFF.describedService__SEFF = transformator.repositoryTransformator.
			getTransformedOperationSignature(dmlCGB.describedSignature);

	}

	/**
	 * Transform the ResourceDemands of CoarseGrainedBehavior into a list of actions
	 */
	def create pcmInternalAction:SeffFactory.eINSTANCE.createInternalAction transformResourceDemandsToActions(
		ArrayList<ResourceDemand> dmlResourceDemands,int oldsize) {
		log.info("Transforming ResourceDemand of CoarseGrainedBehavior");
		pcmInternalAction.entityName = "Transformed InternalAction " + counterInternalActions++;
		var listResourceTypesUsed = new ArrayList<ResourceDemand>();
		var resourceDemandsUnused = new ArrayList<ResourceDemand>();
		resourceDemandsUnused.addAll(dmlResourceDemands);
		for (dmlResourceDemand : dmlResourceDemands) {
			if (listResourceTypesUsed.filter[dmlResourceDemand.resourceType == it.resourceType].size == 0) {
				pcmInternalAction.resourceDemand_Action.add(transformResourceDemand(dmlResourceDemand))
				listResourceTypesUsed.add(dmlResourceDemand);
				resourceDemandsUnused.remove(dmlResourceDemand);
			}
		}
		if (resourceDemandsUnused.size > 0) {
			var nextAction = transformResourceDemandsToActions(resourceDemandsUnused,resourceDemandsUnused.size);
			pcmInternalAction.id=pcmInternalAction.id+":"+oldsize;
			pcmInternalAction.successor_AbstractAction = nextAction;
			nextAction.predecessor_AbstractAction = pcmInternalAction;
		}
	}
	
	
	/**
	 * transform externalcallfrequency of coarse grained behvior to loop action  
	 */
	def create pcmLoopAction:SeffFactory.eINSTANCE.createLoopAction transformExternalCallFrequencyToLoopAction(
		ExternalCallFrequency dmlExternalCallFrequency) {
		log.info(getLogString("ExternalCallFrequency", null, null, "BranchAction"));
		pcmLoopAction.iterationCount_LoopAction = new StoexTransformator().transformRandomVariable(dmlExternalCallFrequency.frequency.explicitDescription);
			
		pcmLoopAction.bodyBehaviour_Loop = SeffFactory.eINSTANCE.
			createResourceDemandingBehaviour;
		val externalStart = SeffFactory.eINSTANCE.createStartAction;
		val externalCall = transformExternalCallFrequencyToCall(dmlExternalCallFrequency);
		val externalStop = SeffFactory.eINSTANCE.createStopAction;
		externalStart.successor_AbstractAction = externalCall;
		externalCall.predecessor_AbstractAction = externalStart;
		externalCall.successor_AbstractAction = externalStop;
		externalStop.predecessor_AbstractAction = externalCall;
		
		pcmLoopAction.bodyBehaviour_Loop.steps_Behaviour.add(externalStart);
		pcmLoopAction.bodyBehaviour_Loop.steps_Behaviour.add(externalStop);
		pcmLoopAction.bodyBehaviour_Loop.steps_Behaviour.add(externalCall);		
	}
	
	int externalCallFrequencyCounter = 1;

	def create pcmExternalCallAction:SeffFactory.eINSTANCE.createExternalCallAction transformExternalCallFrequencyToCall(
		ExternalCallFrequency dmlExternalCallFrequency) {
		pcmExternalCallAction.id = "Transformed ExternalCall Action " + externalCallFrequencyCounter++;
		pcmExternalCallAction.calledService_ExternalService = transformator.repositoryTransformator.
			getTransformedOperationSignature(dmlExternalCallFrequency.externalCall.signature) as OperationSignature;
		pcmExternalCallAction.role_ExternalService = transformator.repositoryTransformator.
			getTransformedRequiredRole(dmlExternalCallFrequency.externalCall.interfaceRequiringRole);

	//dmlExternalCallFrequency.frequency.explicitDescription
	//TODO: transform ExternalCallFrequency Frequency into PCMRandomVariable 
	}

	/**
	 * Transform BlackBoxBehavior to SEFF
	 * adds InternalAction with ResourceType Delay
	 * all ResourceContainer on which a Component with a transformed BBB, get a
	 * Delay Resource 
	 */
	def create pcmResourceDemandingSEFF:SeffFactory.eINSTANCE.createResourceDemandingSEFF transformBlackBoxBehaviour(
		edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.BlackBoxBehavior dmlBlackBox) {
		log.info(
			getLogString("CoarseGrainedBehavior with Signature " + dmlBlackBox.describedSignature?.name, null, null,
				"ResourceDemandingSEFF"));
		pcmResourceDemandingSEFF.id = "transformBBB" + (pcmSEFFid++);
		val startAction = de.uka.ipd.sdq.pcm.seff.SeffFactory.eINSTANCE.createStartAction();
		startAction.entityName = "StartAction BBB" + compInternTransform;
		val stopAction = de.uka.ipd.sdq.pcm.seff.SeffFactory.eINSTANCE.createStopAction();
		stopAction.entityName = "StopAction BBB" + compInternTransform;

		val delayAction = de.uka.ipd.sdq.pcm.seff.SeffFactory.eINSTANCE.createInternalAction();
		delayAction.entityName = "BlackBoxBehavior Delay" + counterInternalActions++;

		val pcmParametricResourceDemand = SeffPerformanceFactory.eINSTANCE.createParametricResourceDemand
		pcmParametricResourceDemand.requiredResource_ParametricResourceDemand = transformator.resourceLandscapeTransformator.
			getDefaultPCMResourceTypeByName("DELAY") as ProcessingResourceType;

		if (dmlBlackBox.responseTime.explicitDescription != null) {
			pcmParametricResourceDemand.specification_ParametericResourceDemand = new StoexTransformator().
				transformRandomVariable(dmlBlackBox.responseTime.explicitDescription);
		} else {
			pcmParametricResourceDemand.specification_ParametericResourceDemand = new StoexTransformator().
				transformRandomVariable(null as RandomVariable);
		}

		delayAction.resourceDemand_Action.add(pcmParametricResourceDemand);

		pcmResourceDemandingSEFF.steps_Behaviour.add(startAction);
		pcmResourceDemandingSEFF.steps_Behaviour.add(stopAction);
		pcmResourceDemandingSEFF.steps_Behaviour.add(delayAction);

		var pcmActionPred = startAction as AbstractAction;

		pcmActionPred.successor_AbstractAction = delayAction;
		delayAction.predecessor_AbstractAction = pcmActionPred;
		pcmActionPred = delayAction;

		stopAction.predecessor_AbstractAction = pcmActionPred;
		pcmActionPred.successor_AbstractAction = stopAction as AbstractAction;
		pcmResourceDemandingSEFF.describedService__SEFF = transformator.repositoryTransformator.
			getTransformedOperationSignature(dmlBlackBox.describedSignature);

	}

	/**
	 * Remove all Actions from Seff except Start and Stop
	 * (used if BlackBoxBehavior DelayAction should not be used)
	 */
	def removeResourceDemandOfSeff(ResourceDemandingSEFF seff) {
		var start = seff.steps_Behaviour.filter[it.predecessor_AbstractAction == null].get(0);
		var stop = seff.steps_Behaviour.filter[it.successor_AbstractAction == null].get(0);
		var interim = start.successor_AbstractAction;
		while (interim != stop) {
			seff.steps_Behaviour.remove(interim);
			interim = interim.successor_AbstractAction;
		}
		stop.predecessor_AbstractAction = start;
		start.successor_AbstractAction = stop;
	}

	def String getLogString(String typeOriginal, String nameOriginal, String idOriginal, String typeTransform) {
		var logOutput = "Transforming " + typeOriginal;
		if (nameOriginal != null) {
			logOutput += " " + nameOriginal;
		} else if (idOriginal != null) {
			logOutput += " " + idOriginal;
		}
		logOutput += " to " + typeTransform;
		return logOutput;
	}

}
