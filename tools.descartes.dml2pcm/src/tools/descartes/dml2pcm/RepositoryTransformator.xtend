package tools.descartes.dml2pcm

import de.uka.ipd.sdq.pcm.core.CoreFactory
import de.uka.ipd.sdq.pcm.core.composition.CompositionFactory
import de.uka.ipd.sdq.pcm.repository.DataType
import de.uka.ipd.sdq.pcm.repository.Interface
import de.uka.ipd.sdq.pcm.repository.Repository
import de.uka.ipd.sdq.pcm.repository.RepositoryComponent
import de.uka.ipd.sdq.pcm.repository.RepositoryFactory
import de.uka.ipd.sdq.pcm.subsystem.SubsystemFactory
import de.uka.ipd.sdq.pcm.system.SystemFactory
import edu.kit.ipd.descartes.mm.applicationlevel.repository.AssemblyConnector
import edu.kit.ipd.descartes.mm.applicationlevel.repository.AssemblyContext
import edu.kit.ipd.descartes.mm.applicationlevel.repository.BasicComponent
import edu.kit.ipd.descartes.mm.applicationlevel.repository.CollectionDataType
import edu.kit.ipd.descartes.mm.applicationlevel.repository.ComposedProvidingRequiringEntity
import edu.kit.ipd.descartes.mm.applicationlevel.repository.CompositeComponent
import edu.kit.ipd.descartes.mm.applicationlevel.repository.CompositeDataType
import edu.kit.ipd.descartes.mm.applicationlevel.repository.InnerDeclaration
import edu.kit.ipd.descartes.mm.applicationlevel.repository.InterfaceProvidingRole
import edu.kit.ipd.descartes.mm.applicationlevel.repository.InterfaceRequiringRole
import edu.kit.ipd.descartes.mm.applicationlevel.repository.PrimitiveDataType
import edu.kit.ipd.descartes.mm.applicationlevel.repository.ProvidingDelegationConnector
import edu.kit.ipd.descartes.mm.applicationlevel.repository.RequiringDelegationConnector
import edu.kit.ipd.descartes.mm.applicationlevel.repository.Semaphore
import edu.kit.ipd.descartes.mm.applicationlevel.repository.Signature
import edu.kit.ipd.descartes.mm.applicationlevel.repository.SubSystem
import edu.kit.ipd.descartes.mm.applicationlevel.system.System
import java.io.File
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl
import de.uka.ipd.sdq.pcm.repository.PassiveResource
import java.util.HashMap
import edu.kit.ipd.descartes.mm.applicationlevel.repository.PrimitiveTypeEnum
import de.uka.ipd.sdq.pcm.repository.OperationInterface
import de.uka.ipd.sdq.pcm.repository.OperationRequiredRole
import java.util.List
import java.util.ArrayList
import de.uka.ipd.sdq.pcm.repository.RepositoryPackage
import de.uka.ipd.sdq.pcm.repository.impl.RepositoryPackageImpl
import de.uka.ipd.sdq.pcm.repository.impl.OperationInterfaceImpl
import de.uka.ipd.sdq.pcm.repository.OperationProvidedRole
import edu.kit.ipd.descartes.mm.applicationlevel.repository.Parameter
import de.uka.ipd.sdq.pcm.repository.ParameterModifier
import edu.kit.ipd.descartes.mm.applicationlevel.repository.InterfaceProvidingEntity
import edu.kit.ipd.descartes.mm.applicationlevel.repository.InterfaceProvidingRequiringEntity
import edu.kit.ipd.descartes.mm.applicationlevel.parameterdependencies.DependencyRelationship
import edu.kit.ipd.descartes.mm.applicationlevel.parameterdependencies.DependencyPropagationRelationship
import org.apache.commons.logging.LogFactory
import org.apache.commons.logging.Log
import org.eclipse.emf.ecore.EObject
import de.uka.ipd.sdq.pcm.core.composition.ComposedStructure

class RepositoryTransformator {
	private static final Log log = LogFactory.getLog(RepositoryTransformator);
	HashMap<String,String> primitiveTypeLiteralMapping = new HashMap<String,String>();
	Repository transformedRepository;
	de.uka.ipd.sdq.pcm.system.System transformedSystem;
	Transformator transformator;
	new(Transformator transformator) {
		this.transformator = transformator;
	}
	/**
	 * Transform Repository
	 */
	def create pcmRepository:RepositoryFactory.eINSTANCE.createRepository transformRepository(edu.kit.ipd.descartes.mm.applicationlevel.repository.Repository dmlRepository){
		log.info("Transforming Repository")
		
		transformedRepository = pcmRepository;
		transformedSystem = null;
		//just to have it all in one place, primitiveenums with pcmprimitives in default repository
		primitiveTypeLiteralMapping.put("STRING","STRING");
		primitiveTypeLiteralMapping.put("BOOL","BOOL");
		primitiveTypeLiteralMapping.put("DOUBLE","DOUBLE");
		primitiveTypeLiteralMapping.put("CHAR","CHAR");
		primitiveTypeLiteralMapping.put("BYTE","BYTE");
		
		for(dmlDataType: dmlRepository.datatypes){
			if(dmlDataType instanceof PrimitiveDataType && 
				primitiveDataTypeInDefaultRepository(dmlDataType as PrimitiveDataType)){
					
			}else{
				pcmRepository.dataTypes__Repository.add(transformDataType(dmlDataType));	
			}
		}
		for(dmlInterface: dmlRepository.interfaces){
			pcmRepository.interfaces__Repository.add(transformRepositoryInterface(dmlInterface));
		}
		for(dmlComponent: dmlRepository.components){
			pcmRepository.components__Repository.add(transformRepositoryComponent(dmlComponent));
		}				
		
		transformBehaviors(dmlRepository);			
		log.info("Transformation Repository finished")
	}
	/**
	 * Transform Relationships (not implemented)
	 * just logs that there need to be readjustments 
	 */
	def void transformRelationships(edu.kit.ipd.descartes.mm.applicationlevel.repository.Repository dmlRepository){
		
		for(dmlComponent: dmlRepository.components){
			var reProvEnt = dmlComponent as InterfaceProvidingRequiringEntity;
			for(relationship:reProvEnt.relationships){
				if(relationship instanceof DependencyRelationship){
					if(dmlComponent instanceof CompositeComponent){
						var output = "Dependency Relationship of Dependent"
						if(relationship.dependent.controlFlowVariable!=null){
							var cfv = relationship.dependent.controlFlowVariable;
							output+= " Control Flow Variable "+cfv.characterization.getName();
						}
						else if(relationship.dependent.resourceDemand!=null){
							var rd = relationship.dependent.resourceDemand;
							output+= " Resource Demand Variable"+rd.characterization.getName();
						}
						else if(relationship.dependent.responseTime!=null){
							var rt = relationship.dependent.responseTime;
							output+= " Response Time Varible "+rt.characterization.getName();
						}
						output+=" with"
						for(independent:relationship.independent){
							output+=" "+independent.toString;
						}
						output+=" are not transformed"
						log.info(output);
					}					
				}
				else if(relationship instanceof DependencyPropagationRelationship){					
					var output = "Dependency Propagation Relationship of Dependent"
					output+=" Influencing Variable "+ relationship.dependent.toString;
					output+=" with"
					for(independent:relationship.independent){
						output+=" "+independent.toString;
					}
					output+=" are not transformed"
					log.info(output);					
				}
			}
		}			
	}
	/**
	 * Transform Behaviors of Components in Repository
	 */
	def void transformBehaviors(edu.kit.ipd.descartes.mm.applicationlevel.repository.Repository dmlRepository){
		for(dmlComponent: dmlRepository.components){
			transformBehavior(dmlComponent);
		}
	}
	/**
	 * Transform Behaviors of BasicComponent
	 * transform all CineGrainedBehaviors
	 * if still signatures of Component exist without Behavior take the corresponding CoarseGrainedBehavior
	 * if still signatures of Component  exist without Behavior take the corresponding BlackGrainedBehavior
	 */
	def void transformBehavior(edu.kit.ipd.descartes.mm.applicationlevel.repository.RepositoryComponent dmlComponent) {
		if(dmlComponent instanceof BasicComponent){
			var pcmComponent = transformBasicComponent(dmlComponent as BasicComponent);
			var describedSignatures = new ArrayList<Signature>();
			
			for(finegrainedbehavior: dmlComponent.fineGrainedBehavior){
				pcmComponent.serviceEffectSpecifications__BasicComponent.add(transformator.behaviorTransformator.transformFineGrainedBehavior(finegrainedbehavior));
				describedSignatures.add(finegrainedbehavior.describedSignature);
			}
			for(coarsegrainedbehavior: dmlComponent.coarseGrainedBehavior){
				if(!describedSignatures.contains(coarsegrainedbehavior.describedSignature)){
					pcmComponent.serviceEffectSpecifications__BasicComponent.add(transformator.behaviorTransformator.transformCoarseGrainedBehavior(coarsegrainedbehavior));
				}					
			}
			for(blackboxbehavior:dmlComponent.blackBoxBehavior){
				if(!describedSignatures.contains(blackboxbehavior.describedSignature)){
					var pcmBehavior = transformator.behaviorTransformator.transformBlackBoxBehaviour(blackboxbehavior)
					pcmComponent.serviceEffectSpecifications__BasicComponent.add(pcmBehavior);
					transformator.deploymentTransformator.addBlackBoxComponent(pcmComponent,pcmBehavior);
				}				
			}
			
		}
		
		
	}
	
	/**
	 * loads default Palladio ResourceTypes from files to be able to link to them in the specifications;
	 * files are from de.uka.sdq.pcm.resources; should be changed to use the plugin, if someone knows how
	 */	
	Repository pcmDefaultRepository;
	
	
	
	def void loadPalladioResourceTypes(){
		val resourceSet = new ResourceSetImpl();
		resourceSet.getPackageRegistry
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
			Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
		
		val fileURI = URI.createFileURI(new File("PCM_Defaults/PrimitiveTypes.repository").getAbsolutePath());	
		val resource = resourceSet.getResource(fileURI, true);
		pcmDefaultRepository = resource.contents.get(0) as Repository;
		//sneak change path
		var uri = URI.createURI("pathmap://PCM_MODELS/PrimitiveTypes.repository");
		resource.setURI(uri);
		
	}
	/**
	 * checks if a DML PrimitiveDataType has in equivalent in default repository 
	 */
	def boolean primitiveDataTypeInDefaultRepository(PrimitiveDataType dmlPrimitiveDataType){
		if(getPrimitiveDataTypeFromRepository(primitiveTypeLiteralMapping.get(dmlPrimitiveDataType.type.literal))!=null){
			return true;
		}
		return false;
	}
	/**
	 * get a primitivedatatype from default repository
	 */
	def de.uka.ipd.sdq.pcm.repository.PrimitiveDataType getPrimitiveDataTypeFromRepository(String name){
		if(name == null) return null;
		if(pcmDefaultRepository==null) loadPalladioResourceTypes();
		for(pcmPrimitiveDataType: pcmDefaultRepository.dataTypes__Repository){
			if(pcmPrimitiveDataType instanceof de.uka.ipd.sdq.pcm.repository.PrimitiveDataType){
				if(pcmPrimitiveDataType.type.literal == name) return pcmPrimitiveDataType;
			}
		}
		return null;
	}
	
	/**
	 * DataTypes
	 * 
	 */	
	/**
	 * Create PrimitiveDataType not in default repository
	 */
	def create pcmPrimitiveDataType:RepositoryFactory.eINSTANCE.createPrimitiveDataType createPrimitiveDataType(PrimitiveDataType dmlPrimitiveDataType){
		if(dmlPrimitiveDataType.type == PrimitiveTypeEnum.INT){
			pcmPrimitiveDataType.type = de.uka.ipd.sdq.pcm.repository.PrimitiveTypeEnum.INT
		}
		else if(dmlPrimitiveDataType.type == PrimitiveTypeEnum.LONG){
			pcmPrimitiveDataType.type = de.uka.ipd.sdq.pcm.repository.PrimitiveTypeEnum.LONG
		}	
		else{
			//should not be able to exist
			log.warn("Transforming PrimitiveDataType "+PrimitiveDataType.name +" failed");
		}
		//if(!transformedRepository?.dataTypes__Repository.contains(pcmPrimitiveDataType)) transformedRepository.dataTypes__Repository.add(pcmPrimitiveDataType)
	}
	
	/**
	 * Transform PrimitiveDataType
	 * Returns Primitive in default repository if existing
	 * PCM has String,Bool, double, char, and byte in default repositories
	 * DML additionally: int, long (those are added to Repository) 
	 */	 
	def de.uka.ipd.sdq.pcm.repository.PrimitiveDataType transformPrimitiveDataType(PrimitiveDataType dmlPrimitiveDataType){
		var returnType = getPrimitiveDataTypeFromRepository(primitiveTypeLiteralMapping.get(dmlPrimitiveDataType.type.literal))
		if(returnType==null){
			return createPrimitiveDataType(dmlPrimitiveDataType);
		}
		return returnType;
	}
	
	/**
	 * Transform CompositeDataType
	 * (Datastructure that stores other Datastructures)
	 */
	def create pcmCompositeDataType:RepositoryFactory.eINSTANCE.createCompositeDataType transformCompositeDataType(CompositeDataType dmlDataType){
		pcmCompositeDataType.id = dmlDataType.id;
		pcmCompositeDataType.entityName = dmlDataType.name;		
		dmlDataType.innerDeclarations.forEach[pcmCompositeDataType.innerDeclaration_CompositeDataType.add(transformInnerDeclaration(it))];
		dmlDataType.parentTypes.forEach[pcmCompositeDataType.parentType_CompositeDataType.add(transformCompositeDataType(it))];
	} 
	/**
	 * Transform InnerDeclaration of CompositeDataType
	 */
	def create pcmInnerDecl:RepositoryFactory.eINSTANCE.createInnerDeclaration transformInnerDeclaration(InnerDeclaration dmlInnerDeclaration){
		pcmInnerDecl.entityName = dmlInnerDeclaration.name;
		//TODO: is compositeDataType of innerdeclaration reference to parent?
		pcmInnerDecl.compositeDataType_InnerDeclaration = transformCompositeDataType(dmlInnerDeclaration.compositeDataType);
		pcmInnerDecl.datatype_InnerDeclaration = transformDataType(dmlInnerDeclaration.dataType);
	}	
	
	/**
	 * Transform CollectionDataType
	 * (Arrays etc.)
	 */
	def create pcmCollectionDataType:RepositoryFactory.eINSTANCE.createCollectionDataType transformCollectionDataType(CollectionDataType dmlDataType){
		log.info(getLogString("CollectionDataType",dmlDataType.name,dmlDataType.id,"CollectionDataType"));
		pcmCollectionDataType.id = dmlDataType.id;
		pcmCollectionDataType.entityName = dmlDataType.name;
		pcmCollectionDataType.innerType_CollectionDataType = transformDataType(dmlDataType.innerType);		
		
	} 
	/**
	 * Transform DataType (which can be Primitive, Composite or Collection)
	 */
	def DataType transformDataType(edu.kit.ipd.descartes.mm.applicationlevel.repository.DataType dmlDataType){
		//log.info("Transforming DataType ");
		switch(dmlDataType){			
			PrimitiveDataType: return transformPrimitiveDataType(dmlDataType)
			CompositeDataType: return transformCompositeDataType(dmlDataType)
			CollectionDataType:return transformCollectionDataType(dmlDataType)
		}		
	}
	
	
	
	/**
	 * Interfaces
	 * 
	 */	
	
	
	def Interface transformRepositoryInterface(edu.kit.ipd.descartes.mm.applicationlevel.repository.Interface dmlInterface){
		//transformation as OperationInterfaces
		return(transformOperationInterface(dmlInterface));
	}
	/**
	 * Transform DML Interface to OperationInterface
	 */
	def create pcmOperationInterface:RepositoryFactory.eINSTANCE.createOperationInterface transformOperationInterface(edu.kit.ipd.descartes.mm.applicationlevel.repository.Interface dmlInterface){
		if(dmlInterface==null) return;
		log.info(getLogString("Interface",dmlInterface.name,dmlInterface.id,"OperationInterface"));
		pcmOperationInterface.id = dmlInterface.id;
		pcmOperationInterface.entityName = dmlInterface.name;
		dmlInterface.signatures.forEach[var pcmSignature = transformOperationSignature(it);
										pcmSignature.interface__OperationSignature=pcmOperationInterface; 
										pcmOperationInterface.signatures__OperationInterface.add(pcmSignature)];	
		
		dmlInterface.parentInterfaces?.forEach[pcmOperationInterface.parentInterfaces__Interface.add(transformOperationInterface(it))];
		//TODO: pcmOperationInterface.requiredCharacterisations
	}
	/**
	 * Transform Signature
	 */
	def create pcmOperationSignature:RepositoryFactory.eINSTANCE.createOperationSignature transformOperationSignature(Signature dmlSignature){
		log.info(getLogString("Signature",dmlSignature.name,dmlSignature.id,"OperationSignature"));
		pcmOperationSignature.id = dmlSignature.id;
		pcmOperationSignature.entityName = dmlSignature.name;
		pcmOperationSignature.returnType__OperationSignature = transformDataType(dmlSignature.returnType);
		dmlSignature.parameters.forEach[pcmOperationSignature.parameters__OperationSignature.add(transformParameter(it))];
	}
	/**
	 * Transform Parameter of a Signature
	 */
	def create pcmParameter:RepositoryFactory.eINSTANCE.createParameter transformParameter(Parameter dmlParameter){
		pcmParameter.parameterName = dmlParameter.name;
		pcmParameter.dataType__Parameter = transformDataType(dmlParameter.dataType);
		
		//Switch refuses to work
		//TODO: check that equals work as intended
		if(dmlParameter.modifier == edu.kit.ipd.descartes.mm.applicationlevel.repository.ParameterModifier.IN){
			pcmParameter.modifier__Parameter = ParameterModifier.IN;
		}
		else if(dmlParameter.modifier == edu.kit.ipd.descartes.mm.applicationlevel.repository.ParameterModifier.NONE){
			pcmParameter.modifier__Parameter = ParameterModifier.NONE
		}
		else if(dmlParameter.modifier == edu.kit.ipd.descartes.mm.applicationlevel.repository.ParameterModifier.OUT){
			pcmParameter.modifier__Parameter = ParameterModifier.OUT
		}
		else if(dmlParameter.modifier == edu.kit.ipd.descartes.mm.applicationlevel.repository.ParameterModifier.INOUT){
			pcmParameter.modifier__Parameter = ParameterModifier.INOUT
		}
	}
		
	
	/**
	 * 
	 * Components
	 * 
	 */
	 
	 
	/**
	 * Transform RepositoryComponent, which can be Basic,Composite or SubSystem
	 */
	def RepositoryComponent transformRepositoryComponent(edu.kit.ipd.descartes.mm.applicationlevel.repository.RepositoryComponent dmlComponent){	
		switch(dmlComponent){
			BasicComponent: 	transformBasicComponent(dmlComponent)
			CompositeComponent: transformCompositeComponent(dmlComponent)
			SubSystem: 			transformSubSystem(dmlComponent)
		}
	}
	
	/**
	 * Transform System
	 */
	def create pcmSystem:SystemFactory.eINSTANCE.createSystem transformSystem(System dmlSystem){
		if(transformedSystem!=null) println("Warning more than one system");
		transformedSystem = pcmSystem;
		//pcm systems need to have at least one uppercase letter in the name?
		pcmSystem.entityName = "A"+dmlSystem.name;
		pcmSystem.id = dmlSystem.id;
		transformComposedInterfaceProvidingRequiringEntity(dmlSystem,pcmSystem);
	}
	/**
	 * Transform Subsystem
	 */
	def create pcmSubSystem:SubsystemFactory.eINSTANCE.createSubSystem transformSubSystem(SubSystem dmlSubSystem) {
		pcmSubSystem.entityName = dmlSubSystem.name;
		pcmSubSystem.id = dmlSubSystem.id;
		transformComposedInterfaceProvidingRequiringEntity(dmlSubSystem,pcmSubSystem);
	}
	/**
	 * Transform CompositeComponent
	 */
	def create pcmComposite:RepositoryFactory.eINSTANCE.createCompositeComponent transformCompositeComponent(CompositeComponent dmlComposite) {
		//new LogFile(dmlComposite,pcmComposite,1,loggingModule);
		pcmComposite.entityName = dmlComposite.name;
		pcmComposite.id = dmlComposite.id;
		//dmlComposite.relationships
		transformComposedInterfaceProvidingRequiringEntity(dmlComposite,pcmComposite);
	}
	
	/**
	 * transform Attributes/References of a ComposedComponent(System,Subsystem,CompositeComponent)
	 * takes DML ComposedComponent and already created PCM model as Input
	 */
	def void transformComposedInterfaceProvidingRequiringEntity(ComposedProvidingRequiringEntity dmlComposed, de.uka.ipd.sdq.pcm.core.entity.ComposedProvidingRequiringEntity pcmComposed){
		dmlComposed.assemblyContexts.forEach[pcmComposed.assemblyContexts__ComposedStructure.add(transformAssemblyContext(it))];
		dmlComposed.requiringDelegationConnectors.forEach[pcmComposed.connectors__ComposedStructure.add(transformRequiringDelegationConnector(it))];
		dmlComposed.providingDelegationConnectors.forEach[pcmComposed.connectors__ComposedStructure.add(transformProvidingDelegationConnector(it))];
		dmlComposed.assemblyConnectors.forEach[pcmComposed.connectors__ComposedStructure.add(transformAssemblyConnector(it))];
		dmlComposed.interfaceProvidingRoles.forEach[pcmComposed.providedRoles_InterfaceProvidingEntity.add(transformInterfaceProvidingRole(it))];
		dmlComposed.interfaceRequiringRoles.forEach[pcmComposed.requiredRoles_InterfaceRequiringEntity.add(transformInterfaceRequiringRole(it))];
		
	}
	/**
	 * Transform BasicComponent
	 */
	def create pcmBasicComponent:RepositoryFactory.eINSTANCE.createBasicComponent transformBasicComponent(BasicComponent dmlComponent) {
		log.info(getLogString("BasicComponent",dmlComponent.name,dmlComponent.id,"BasicComponent"));
		//"The OperationInterface is a specific type of interface related to operation calls. For this, it also refereferences 
		//a set of operation interfaces. Operations can represent methods, functions or any comparable concept."
		
		pcmBasicComponent.id = dmlComponent.id;
		pcmBasicComponent.entityName = dmlComponent.name;
		
		dmlComponent.interfaceProvidingRoles.forEach[pcmBasicComponent.providedRoles_InterfaceProvidingEntity.add(transformInterfaceProvidingRole(it))];
		dmlComponent.interfaceRequiringRoles.forEach[pcmBasicComponent.requiredRoles_InterfaceRequiringEntity.add(transformInterfaceRequiringRole(it))];
		
		dmlComponent.semaphores.forEach[pcmBasicComponent.passiveResource_BasicComponent.add(transformSemaphore(it))]; 
		
	}
	/**
	 * Transform DelegationConnector
	 */
	def create pcmProvidedDelegationConnector:CompositionFactory.eINSTANCE.createProvidedDelegationConnector transformProvidingDelegationConnector(ProvidingDelegationConnector dmlProvDelegationConnector){
		log.info(getLogString("ProvidingDelegationConnector",dmlProvDelegationConnector.name,dmlProvDelegationConnector.id,"ProvidedDelegationConnector"));
		pcmProvidedDelegationConnector.id = dmlProvDelegationConnector.id;
		pcmProvidedDelegationConnector.entityName = dmlProvDelegationConnector.name;
		pcmProvidedDelegationConnector.assemblyContext_ProvidedDelegationConnector = transformAssemblyContext(dmlProvDelegationConnector.assemblyContext);
		pcmProvidedDelegationConnector.outerProvidedRole_ProvidedDelegationConnector = transformInterfaceProvidingRole(dmlProvDelegationConnector.outerInterfaceProvidingRole);
		pcmProvidedDelegationConnector.innerProvidedRole_ProvidedDelegationConnector = transformInterfaceProvidingRole(dmlProvDelegationConnector.innerInterfaceProvidingRole);
		var cs = dmlProvDelegationConnector.parentStructure;
		var ComposedStructure pcmCs = null;
		switch(cs){
			System:				pcmCs = transformSystem(cs)
			CompositeComponent: pcmCs = transformCompositeComponent(cs)
			SubSystem: 			pcmCs = transformSubSystem(cs)
		}
		pcmProvidedDelegationConnector.parentStructure__Connector = pcmCs;
	}
	
	
	/**
	 * Transform RequiringDelegationConnector
	 */
	def create pcmRequiredDelegationConnector:CompositionFactory.eINSTANCE.createRequiredDelegationConnector transformRequiringDelegationConnector(RequiringDelegationConnector dmlReqDelegationConnector){
		log.info(getLogString("RequiringDelegationConnector",dmlReqDelegationConnector.name,dmlReqDelegationConnector.id,"RequiredDelegationConnector"));
		pcmRequiredDelegationConnector.id = dmlReqDelegationConnector.id;
		pcmRequiredDelegationConnector.entityName = dmlReqDelegationConnector.name;
		pcmRequiredDelegationConnector.assemblyContext_RequiredDelegationConnector = transformAssemblyContext(dmlReqDelegationConnector.assemblyContext);
		pcmRequiredDelegationConnector.outerRequiredRole_RequiredDelegationConnector = transformInterfaceRequiringRole(dmlReqDelegationConnector.outerInterfaceRequiringRole);
		pcmRequiredDelegationConnector.innerRequiredRole_RequiredDelegationConnector = transformInterfaceRequiringRole(dmlReqDelegationConnector.innerInterfaceRequiringRole);		
		var cs = dmlReqDelegationConnector.parentStructure;
		var ComposedStructure pcmCs = null;
		switch(cs){
			System:				pcmCs = transformSystem(cs)
			CompositeComponent: pcmCs = transformCompositeComponent(cs)
			SubSystem: 			pcmCs = transformSubSystem(cs)
		}
		pcmRequiredDelegationConnector.parentStructure__Connector = pcmCs;
	}
	/**
	 * Transform AssemblyContext
	 */
	def create pcmAssemblyContext:CompositionFactory.eINSTANCE.createAssemblyContext transformAssemblyContext(AssemblyContext dmlAssemblyContext){
		log.info(getLogString("AssemblyContext",dmlAssemblyContext.name,dmlAssemblyContext.id,"AssemblyContext"));
		pcmAssemblyContext.id = dmlAssemblyContext.id;
		pcmAssemblyContext.entityName = dmlAssemblyContext.name;
		pcmAssemblyContext.encapsulatedComponent__AssemblyContext = transformRepositoryComponent(dmlAssemblyContext.encapsulatedComponent);
	}
	/**
	 * Transform AssemblyConnector
	 */
	def create pcmAssemblyConnector:CompositionFactory.eINSTANCE.createAssemblyConnector transformAssemblyConnector(AssemblyConnector dmlAssemblyConnector){
		log.info(getLogString("AssemblyConnector",dmlAssemblyConnector.name,dmlAssemblyConnector.id,"AssemblyConnector"));
		pcmAssemblyConnector.id = dmlAssemblyConnector.id;
		pcmAssemblyConnector.entityName = dmlAssemblyConnector.name;
		pcmAssemblyConnector.providedRole_AssemblyConnector = transformInterfaceProvidingRole(dmlAssemblyConnector.interfaceProvidingRole);
		pcmAssemblyConnector.requiredRole_AssemblyConnector = transformInterfaceRequiringRole(dmlAssemblyConnector.interfaceRequiringRole);
		pcmAssemblyConnector.providingAssemblyContext_AssemblyConnector = transformAssemblyContext(dmlAssemblyConnector.providingAssemblyContext);
		pcmAssemblyConnector.requiringAssemblyContext_AssemblyConnector = transformAssemblyContext(dmlAssemblyConnector.requiringAssemblyContext);				
	}	
	
	/**
	 * Transform InterfaceProvidingRole to ProvidingRole
	 */
	def create pcmProvidingRole:RepositoryFactory.eINSTANCE.createOperationProvidedRole transformInterfaceProvidingRole(InterfaceProvidingRole dmlInterfaceProvidingRole){
		log.info(getLogString("InterfaceProvidingRole",dmlInterfaceProvidingRole.name,dmlInterfaceProvidingRole.id,"OperationProvidedRole"));
		pcmProvidingRole.id = dmlInterfaceProvidingRole.id;
		pcmProvidingRole.entityName = dmlInterfaceProvidingRole.name;
		pcmProvidingRole.providedInterface__OperationProvidedRole = transformOperationInterface(dmlInterfaceProvidingRole.interface);
	}
	
	/**
	 * Transform InterfaceRequiringRole to RequiredRole
	 */
	def create pcmRequiringRole:RepositoryFactory.eINSTANCE.createOperationRequiredRole transformInterfaceRequiringRole(InterfaceRequiringRole dmlInterfaceProvidingRole){
		log.info(getLogString("InterfaceRequiringRole",dmlInterfaceProvidingRole.name,dmlInterfaceProvidingRole.id,"OperationRequiredRole"));
		pcmRequiringRole.id = dmlInterfaceProvidingRole.id;
		pcmRequiringRole.entityName = dmlInterfaceProvidingRole.name;
		pcmRequiringRole.requiredInterface__OperationRequiredRole = transformOperationInterface(dmlInterfaceProvidingRole.interface);
	}
	
	/**
	 * Transform Semaphore to PassiveResource
	 */
	def create pcmPassiveResource:RepositoryFactory.eINSTANCE.createPassiveResource transformSemaphore(Semaphore dmlSemaphore){
		log.info(getLogString("Semaphore",dmlSemaphore.name,dmlSemaphore.id,"PassiveResource"));
		pcmPassiveResource.id = dmlSemaphore.id;
		pcmPassiveResource.entityName = dmlSemaphore.name;
		pcmPassiveResource.capacity_PassiveResource = CoreFactory.eINSTANCE.createPCMRandomVariable;
		pcmPassiveResource.capacity_PassiveResource.specification = dmlSemaphore.capacity.toString;
	}
	
	
	/**
	 * Getters for transformed Repository Components, e.g. for BehaviorTransformator 
	 */
	 
	 
	/**
	 * returns passiveResource from Semaphore for Transformation of Behavior, transformSemaphore could be used but double checking
	 */
	public def PassiveResource getPassiveResourceFromSemaphore(Semaphore dmlSemaphore){		
		if(dmlSemaphore==null) return null;
		if(transformedRepository!=null){
			for(comp: transformedRepository.components__Repository){
				if(comp instanceof de.uka.ipd.sdq.pcm.repository.BasicComponent){
					for(passiveResource:comp.passiveResource_BasicComponent){
						if(passiveResource.id == dmlSemaphore.id) return passiveResource;
					}
				}
			}
		}
		
		log.warn("Semaphore "+dmlSemaphore.id +" requested, but not found in Repository. This could produce error when saving");
		return transformSemaphore(dmlSemaphore);
	}
	
	/**
	 * get Transformed RequiredRole from DML RequiringRole
	 * assumes Repository has been already transformed
	 */	
	public def OperationRequiredRole getTransformedRequiredRole(InterfaceRequiringRole dmlRole){
		if(dmlRole==null) return null;
		if(transformedRepository!=null){
			for(component : transformedRepository.components__Repository){
				for(requiredrole:component.requiredRoles_InterfaceRequiringEntity){
					if(requiredrole instanceof OperationRequiredRole && dmlRole.id == requiredrole.id){
						return requiredrole as OperationRequiredRole;
					} 
				}
			}
		}
		
		if(transformedSystem!=null){
			for(requiredrole: transformedSystem.requiredRoles_InterfaceRequiringEntity){
				if(requiredrole instanceof OperationRequiredRole && dmlRole.id == requiredrole.id){
					return requiredrole as OperationRequiredRole;
				} 
			}
		}
		log.warn("InterfaceRequiringRole "+dmlRole.id +" requested, but not found in Repository. This could produce error when saving");
		return transformInterfaceRequiringRole(dmlRole);
	}
	
	/**
	 * get Transformed ProvideddRole from DML ProvidingRole
	 * assumes Repository has been already transformed
	 */
	public def OperationProvidedRole getTransformedProvidedRole(InterfaceProvidingRole dmlRole){
		if(dmlRole==null) return null;
		if(transformedRepository!=null){
			for(component : transformedRepository.components__Repository){
				for(providedrole:component.providedRoles_InterfaceProvidingEntity){
					if(providedrole instanceof OperationProvidedRole && dmlRole.id == providedrole.id){
						return providedrole as OperationProvidedRole;
					} 
				}
			}		
		}
		if(transformedSystem!=null){
			for(providedrole: transformedSystem.providedRoles_InterfaceProvidingEntity){
				if(providedrole instanceof OperationProvidedRole && dmlRole.id == providedrole.id){
					return providedrole as OperationProvidedRole;
				} 
			}
		}
		log.warn("InterfaceProvidingRole "+dmlRole.id +" requested, but not found in Repository. This could produce error when saving");
		return transformInterfaceProvidingRole(dmlRole);
	}
	
	/**
	 * get Transformed Signature from DML Signature
	 * assumes Repository has been already transformed
	 */
	public def de.uka.ipd.sdq.pcm.repository.Signature getTransformedOperationSignature(Signature dmlSignature){
		if(dmlSignature==null) return null;
		if(transformedRepository!=null){
			for(interface: transformedRepository.interfaces__Repository){
				if(interface instanceof OperationInterface){
					for(signature: interface.signatures__OperationInterface){
						if(signature.id == dmlSignature.id) return signature;
					}
				}
			}		
		}
		log.warn("Signature "+dmlSignature?.id +" requested, but not found in Repository. This could produce error when saving");
		return transformOperationSignature(dmlSignature);
		
	}	
	/**
	 * get Transformed RepositoryComponent from DML RepositoryComponent
	 * assumes Repository has been already transformed
	 */
	public def RepositoryComponent getTransformedRepositoryComponent(edu.kit.ipd.descartes.mm.applicationlevel.repository.RepositoryComponent dmlRepositoryComponent){
		if(dmlRepositoryComponent==null) return null;
		for(rc : transformedRepository.components__Repository){
			if(dmlRepositoryComponent.id == rc.id){
				return(rc);
			}
		}
		log.warn("RepositoryComponent "+dmlRepositoryComponent.id +" requested, but not found in Repository. This could produce error when saving");
		return transformRepositoryComponent(dmlRepositoryComponent);
	}
	
	def String getLogString(String typeOriginal,String nameOriginal,String idOriginal,String typeTransform){
		var logOutput = "Transforming "+typeOriginal;
		if(nameOriginal!=null){
			logOutput+= " "+nameOriginal;
		} 
		else if(idOriginal!=null){
			logOutput +=" "+idOriginal;
		} 
		logOutput+=" to "+typeTransform;
		return logOutput;
	}
	
//	int brackets;
//	def String getLogString(EObject originalInstance, EObject transformInstance, int bracketeering){
//		
//		brackets = brackets+bracketeering;
//		var dmlClassName = "";//originalInstance?.eClass?.name;
//		var dmlnamestruc = originalInstance?.eClass?.getEStructuralFeature("name");
//		var pcmnamestruc = transformInstance?.eClass?.getEStructuralFeature("entityName");
//
//		var tab = "";
//		for(var i = 0;i<brackets;i++){
//			tab+= "	";
//		}
//		var String logOutput ="Transforming "+ dmlClassName;
//		if(dmlnamestruc!=null){logOutput+=" "+originalInstance.eGet(dmlnamestruc);}
//		logOutput+= " to "+transformInstance?.eClass?.name;
//		return logOutput;
//		
//	}
//	def void bracketdown(){
//		brackets--;
//	}
	
}