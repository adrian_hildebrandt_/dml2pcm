package tools.descartes.dml2pcm

import de.uka.ipd.sdq.pcm.core.CoreFactory
import de.uka.ipd.sdq.pcm.core.PCMRandomVariable
import de.uka.ipd.sdq.pcm.repository.RepositoryFactory
import de.uka.ipd.sdq.pcm.resourceenvironment.LinkingResource
import de.uka.ipd.sdq.pcm.resourceenvironment.ProcessingResourceSpecification
import de.uka.ipd.sdq.pcm.resourceenvironment.ResourceContainer
import de.uka.ipd.sdq.pcm.resourceenvironment.ResourceEnvironment
import de.uka.ipd.sdq.pcm.resourceenvironment.ResourceenvironmentFactory
import de.uka.ipd.sdq.pcm.resourcetype.CommunicationLinkResourceType
import de.uka.ipd.sdq.pcm.resourcetype.ProcessingResourceType
import de.uka.ipd.sdq.pcm.resourcetype.ResourceRepository
import de.uka.ipd.sdq.pcm.resourcetype.ResourceType
import de.uka.ipd.sdq.pcm.resourcetype.ResourcetypeFactory
import de.uka.ipd.sdq.pcm.resourcetype.SchedulingPolicy
import edu.kit.ipd.descartes.mm.resourceconfiguration.ActiveResourceSpecification
import edu.kit.ipd.descartes.mm.resourceconfiguration.ConfigurationSpecification
import edu.kit.ipd.descartes.mm.resourceconfiguration.CustomConfigurationSpecification
import edu.kit.ipd.descartes.mm.resourceconfiguration.LinkingResourceSpecification
import edu.kit.ipd.descartes.mm.resourceconfiguration.PassiveResourceSpecification
import edu.kit.ipd.descartes.mm.resourcelandscape.CompositeHardwareInfrastructure
import edu.kit.ipd.descartes.mm.resourcelandscape.ComputingInfrastructure
import edu.kit.ipd.descartes.mm.resourcelandscape.Container
import edu.kit.ipd.descartes.mm.resourcelandscape.DataCenter
import edu.kit.ipd.descartes.mm.resourcelandscape.DistributedDataCenter
import edu.kit.ipd.descartes.mm.resourcelandscape.RuntimeEnvironment
import java.io.File
import java.math.BigInteger
import java.util.ArrayList
import java.util.HashMap
import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl
import de.uka.ipd.sdq.pcm.resourcetype.ResourcetypePackage
import de.uka.ipd.sdq.pcm.resourceenvironment.CommunicationLinkResourceSpecification

class ResourceLandscapeTransformator {
	private static final Log log = LogFactory.getLog(ResourceLandscapeTransformator);
	static var resourcelandscapes = new HashMap<String, ResourceEnvironment>();
	val mappingVMtoHardware = new HashMap<Container, ResourceContainer>;
	int id = 0;

	/**
	 * Transform DistributedDataCenter to ResourceLandscape
	 * DataCenters are discarded, contained ComputingInfrastructures are transformed to ResourceContainers
	 */
	public def create pcmResourcelandscape:ResourceenvironmentFactory.eINSTANCE.createResourceEnvironment transformDistributedDataCenter(
		DistributedDataCenter ddc) {
		log.info("Starting ResourceLandscape Transformation");
		log.info(getLogString("DistributedDataCenter", ddc.name, ddc.id, "ResourceLandscape"));
		pcmResourcelandscape.entityName = ddc.name as String;
		if (resourcelandscapes.get(ddc.id) != null) {
			//throw new Error("this resource landscape already exists");
		}
		resourcelandscapes.put(ddc.id, pcmResourcelandscape);
		loadPalladioResourceTypes();

		var pcmResourceContainerList = new ArrayList<ResourceContainer>();
		for (DataCenter dc : ddc.consistsOf) {
			transformDataCenter(dc, pcmResourceContainerList);
		}
		pcmResourceContainerList.forEach[pcmResourcelandscape.resourceContainer_ResourceEnvironment.add(it)];
		log.info("ResourceLandcsape Transformation Done");
	}

	public def ResourceEnvironment getResourceEnvironmentById(String id) {
		val pcmResourceEnvironment = resourcelandscapes.get(id);
		if (pcmResourceEnvironment == null) {
			log.info("resource environment with " + id + " was not yet transformed/could not be found")
		}
		return pcmResourceEnvironment;
	}

	/**
	 * loads default Palladio ResourceTypes from files to be able to link to them in the specifications;
	 * files are from de.uka.sdq.pcm.resources; should be changed to use the plugin, if someone knows how
	 */
	ResourceRepository pcmDefaultResourceTypesRepository;

	def void loadPalladioResourceTypes() {
		log.info("Loading default Palladio ResourceTypes")
		val resourceSet = new ResourceSetImpl();
		resourceSet.getPackageRegistry
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
			Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
		val package = ResourcetypePackage.eINSTANCE;
		val fileURI = URI.createFileURI(new File("PCM_Defaults/Palladio.resourcetype").getAbsolutePath());

		
		
		val resource = resourceSet.getResource(fileURI, true);
		
		//hack but it seems to work
		var uri = URI.createURI("pathmap://PCM_MODELS/Palladio.resourcetype");
		resource.setURI(uri);

		pcmDefaultResourceTypesRepository = resource.contents.get(0) as ResourceRepository;

	}

	def ResourceType getDefaultPCMResourceTypeByName(String pcmName) {
		if(pcmDefaultResourceTypesRepository==null) loadPalladioResourceTypes();
		for (pcmDefaultResourceType : pcmDefaultResourceTypesRepository.availableResourceTypes_ResourceRepository) {
			if(pcmDefaultResourceType.entityName == pcmName) return pcmDefaultResourceType;
		}
		return null;
	}

	/**
	 * Transform DataCenter to ResourceContainer(s)
	 * adds transformed ComputingInfrastructures to List of ResourceContainers of DDC transformation
	 */
	def void transformDataCenter(DataCenter dmlDataCenter, ArrayList<ResourceContainer> pcmResourceContainerList) {
		for (abstractHardwareInfrastructure : dmlDataCenter.contains) {
			switch (abstractHardwareInfrastructure) {
				ComputingInfrastructure:
					pcmResourceContainerList.add(transformComputingInfrastructure(abstractHardwareInfrastructure))
				CompositeHardwareInfrastructure:
					extractHardwareFromCompositeHardwareInfrastructure(abstractHardwareInfrastructure,
						pcmResourceContainerList)
			}
		}

	}

	/**
	 * go through childs of DataCenter and save parent
	 */
	def void extractHardwareFromCompositeHardwareInfrastructure(CompositeHardwareInfrastructure compositeHI,
		ArrayList<ResourceContainer> pcmResourceContainerList) {
		for (abstractHardwareInfrastructure : compositeHI.contains) {
			switch (abstractHardwareInfrastructure) {
				ComputingInfrastructure:
					pcmResourceContainerList.add(transformComputingInfrastructure(abstractHardwareInfrastructure))
				CompositeHardwareInfrastructure:
					extractHardwareFromCompositeHardwareInfrastructure(abstractHardwareInfrastructure,
						pcmResourceContainerList)
			}
		}
	}

	/**
	 * returns configspec of a container
	 * template is highest priority, should probably be changed so that configSpec overrides template it,
	 * 
	 */
	def EList<ConfigurationSpecification> getConfigSpec(Container dmlContainer) {

		//DML Container may be not further specified (as long as lower levels are if components are deployed
		//(but the purpose is more just making sure that no null pointers are dereferenced)
		if (dmlContainer.template == null && (dmlContainer.configSpec == null || dmlContainer.configSpec.length == 0)) {
			if (dmlContainer.contains == null || dmlContainer.contains.length == 0) {

				//nothing to specifiy;
				return null;
			}
			if (dmlContainer.contains.length > 1) {

				//TODO: decide which vm to make into hardware, 0 for now
				return (getConfigSpec(dmlContainer.contains.get(0)));
			} else {
				return (getConfigSpec(dmlContainer.contains.get(0)));
			}
		}

		//TODO: find out if container.configSpec overrides everything or just parts				
		if (dmlContainer.template != null) {
			if(dmlContainer.configSpec != null){
				var templatelist = dmlContainer.template.templateConfig.clone();
				
			}
			return (dmlContainer.template.templateConfig);
			
			
		} else {
			return (dmlContainer.configSpec);
		}

	}
	
	def void compareConfigSpecs(EList<ConfigurationSpecification> template, EList<ConfigurationSpecification> additionals){
		//assume only unique resource types per list
		var list = new ArrayList<ProcessingResourceSpecification>();
		for(templateConfig: template){
			
		}
		
		
		 
		for(additionalConfig:additionals){
			for(templateConfig: template){			
				if(templateConfig instanceof ActiveResourceSpecification && additionalConfig instanceof ActiveResourceSpecification){
					var templateActive = templateConfig as ActiveResourceSpecification;
					var additionalActive = additionalConfig as ActiveResourceSpecification;
					for(processingresource: additionalActive.processingResourceSpecifications){
						for(processingresourceTemplate:templateActive.processingResourceSpecifications){
							if(processingresource.activeResourceType.id == processingresourceTemplate.id){
								
							}
						}
					}
				}
			}
		}
		
		
		
	}
	
	
	/**
	 * Transform ComputingInfrastructure to ResourceContainer
	 */
	def create pcmResourceContainer:ResourceenvironmentFactory.eINSTANCE.createResourceContainer transformComputingInfrastructure(
		ComputingInfrastructure dmlComputingInfrastructure) {
		log.info(
			getLogString("ComputingInfrastructure", dmlComputingInfrastructure.name, dmlComputingInfrastructure.id,
				"ResourceContainer"));
		pcmResourceContainer.id = dmlComputingInfrastructure.id;
		pcmResourceContainer.entityName = dmlComputingInfrastructure.name;
		var dmlConfigSpecs = getConfigSpec(dmlComputingInfrastructure);
		for (dmlConfigSpec : dmlConfigSpecs) {
			switch dmlConfigSpec {
				ActiveResourceSpecification: {
					transformActiveResourceSpecificationToProcessingResourceSpecifications(dmlConfigSpec).forEach[
						pcmResourceContainer.activeResourceSpecifications_ResourceContainer.add(it)]
					if (dmlComputingInfrastructure.partOf != null && dmlComputingInfrastructure.partOf.belongsTo != null) {
						val pcmResourceLandscape = resourcelandscapes.get(dmlComputingInfrastructure.partOf.belongsTo.id);
						if (pcmResourceLandscape == null) {
							log.error(
								"Could not find the ResourceEnvironment ComputingInfrastructure" +
									dmlComputingInfrastructure.name + " belongs to");
						}
						transformActiveResourceSpecificationToLinkingReourceSpecifications(dmlConfigSpec).forEach[
							pcmResourceLandscape.linkingResources__ResourceEnvironment.add(it)];
					}
				}
				//unsure how passiveresource is used in dml since aquire/release use semapohre
				PassiveResourceSpecification:
					transformPassiveResourceSpecification(dmlConfigSpec) /*todo: find out where to put passive in resource container*/
				CustomConfigurationSpecification:
					log.info(
						"Custom configuration of Resource " + dmlComputingInfrastructure.name +
							" is not transformed automatically")
			}
		}

		dmlComputingInfrastructure.contains.forEach[iterateVM(it, pcmResourceContainer)];
		mappingVMtoHardware.put(dmlComputingInfrastructure, pcmResourceContainer);
	}

	/**
	 * Store on which Hardware the VirtualMachines run
	 * (add Container and new PCM ResourceContainer to HashMap<Container,ResourceContainer>)
	 * But: would be probably enough to go up DML Container until Parentcontainer is no longer instanceof RuntimeEnvironment
	 */
	def void iterateVM(Container dmlContainer, ResourceContainer pcmHardwareTransform) {
		mappingVMtoHardware.put(dmlContainer, pcmHardwareTransform);
		for (dmlContainees : dmlContainer.contains) {
			iterateVM(dmlContainees, pcmHardwareTransform);
		}
	}

	/**
	 * Transformation: PassiveResourceSpecification to PassiveResource
	 */
	def create pcmPassiveRessource:RepositoryFactory.eINSTANCE.createPassiveResource transformPassiveResourceSpecification(
		PassiveResourceSpecification specification) {
		pcmPassiveRessource.id = specification.id;
		pcmPassiveRessource.entityName = specification.name;
		pcmPassiveRessource.capacity_PassiveResource = transformBigIntegerToRandomVariable(
			specification.capacity.capacity);
	}

	/**
	 * Transformation: LinkingResourceSpecification to LinkingResource
	 */
	
	def create pcmLinkingResource:ResourceenvironmentFactory.eINSTANCE.createLinkingResource transformLinkingResourceSpecification(
		LinkingResourceSpecification dmlLinkingResource) {
		log.info(
			getLogString("LinkingResourceSpecification", dmlLinkingResource.name, dmlLinkingResource.id,
				"LinkingResource"));
		pcmLinkingResource.id = dmlLinkingResource.id;
		pcmLinkingResource.entityName = dmlLinkingResource.name;

		//each time get Container of Spec, go up DML hierarchy until Container is ComutingInfrastructure
		for (ars : dmlLinkingResource.connectedResourceSpecifications) {
			var container = ars.eContainer;
			if(container instanceof ComputingInfrastructure){
				var pcmResContainer = transformComputingInfrastructure(container as ComputingInfrastructure);
				pcmLinkingResource.connectedResourceContainers_LinkingResource.add(pcmResContainer);
			}
			else if(container instanceof RuntimeEnvironment){
				while (container.eClass instanceof RuntimeEnvironment) {
					container = container.eContainer;
				}
				var pcmResContainer = transformComputingInfrastructure(container as ComputingInfrastructure);
				pcmLinkingResource.connectedResourceContainers_LinkingResource.add(pcmResContainer);
			}
			else{
				log.warn("Unable to resolve connected ResourceSpecification of LinkingResource to ComputingInfrastructure");
			}

		//TODO: Linking Resource: eContainer should give ComputingInfrastructure, but it needs testing, 
		//(does give eContainer always the right result, does it give back Impl?)			
		}
		/*var linksource = dmlLinkingResource.parentResourceConfiguration.eContainer;
		while (linksource.eClass instanceof RuntimeEnvironment) {
			linksource = linksource.eContainer;
			
			pcmLinkingResource.connectedResourceContainers_LinkingResource.add(
				transformComputingInfrastructure(linksource as ComputingInfrastructure));
		}*/

		var pcmLinkSpec = ResourceenvironmentFactory.eINSTANCE.createCommunicationLinkResourceSpecification;
		pcmLinkingResource.communicationLinkResourceSpecifications_LinkingResource = pcmLinkSpec;
		
		//PCM throughput: "Specifies the maximum throughput of this linking resource in byte per simulated time unit." 
		pcmLinkSpec.throughput_CommunicationLinkResourceSpecification = transformDoubleToRandomVariable(
			dmlLinkingResource.bandwidth);
		
		if (dmlLinkingResource.communicationLinkResourceType?.name == "LAN") {
			pcmLinkSpec.communicationLinkResourceType_CommunicationLinkResourceSpecification = getDefaultPCMResourceTypeByName(
				"LAN") as CommunicationLinkResourceType;
		} else {

			//non specified CommunicationLinkResourceType, shouldn't happen since DML only allows LAN atm 
			if(dmlLinkingResource.communicationLinkResourceType==null){
				log.info("LinkingResourceType of LinkingResourceSpecification not set");
			}
			else{
				log.info(
				"transformation of CommunicationLinkResourceType " +
					dmlLinkingResource.communicationLinkResourceType?.name + " not supported");
	
			}
		}

	}
	
	
	/**
	 *  Go through ActiveResourceSpecification and find LinkingResourceSpecifications and transform them into PCM counterpart
	 */
	def ArrayList<LinkingResource> transformActiveResourceSpecificationToLinkingReourceSpecifications(
		ActiveResourceSpecification dmlActiveResourceSpecification) {
		val linkingresourcespecificationList = new ArrayList<LinkingResource>;
		for (dmlLinkingResourceSpecification : dmlActiveResourceSpecification.linkingResources) {
			linkingresourcespecificationList.add(transformLinkingResourceSpecification(dmlLinkingResourceSpecification));
		}
		return linkingresourcespecificationList;
	}

	int cloneObject = 1;
	Transformator transformator;
	new(Transformator transformator) {
		this.transformator = transformator;
	}

	/**
	 *  Go through ActiveResourceSpecification and find ActiveResourceSpecifications (and transform them into PCM counterpart)
	 */
	def ArrayList<ProcessingResourceSpecification> transformActiveResourceSpecificationToProcessingResourceSpecifications(
		ActiveResourceSpecification dmlActiveResourceSpecification) {
		var processingresourcespecificationList = new ArrayList<ProcessingResourceSpecification>;
		for (dmlProcessingResourceSpecification : dmlActiveResourceSpecification.processingResourceSpecifications) {
			processingresourcespecificationList.add(
				transformProcessingResourceSpecification(dmlProcessingResourceSpecification, cloneObject++));
		}
		return processingresourcespecificationList;
	}

	/**
	 * Transform ProcessingResourceSpecification to PCM ProcessingResource
	 */
	def create pcmProcessingResourceSpecification:ResourceenvironmentFactory.eINSTANCE.createProcessingResourceSpecification transformProcessingResourceSpecification(
		edu.kit.ipd.descartes.mm.resourceconfiguration.ProcessingResourceSpecification dmlProcessingResourceSpecification,
		int toclone) {
		log.info(
			getLogString("ProcessingResourceSpecification", dmlProcessingResourceSpecification.name,
				dmlProcessingResourceSpecification.id, "ProcessingResourceSpecification"));
		pcmProcessingResourceSpecification.id = dmlProcessingResourceSpecification.id + " " + toclone.toString;

		pcmProcessingResourceSpecification.processingRate_ProcessingResourceSpecification = transformDoubleToRandomVariable(
			dmlProcessingResourceSpecification.processingRate);
		
		pcmProcessingResourceSpecification.schedulingPolicy = getSchedulingPolicy(
			dmlProcessingResourceSpecification.schedulingPolicy);
		pcmProcessingResourceSpecification.numberOfReplicas = if(dmlProcessingResourceSpecification.nrOfParProcUnits ==
			null) 1 else dmlProcessingResourceSpecification.nrOfParProcUnits.number;
		var pcmProcessingResourceType = getDefaultPCMResourceTypeByName(
			dmlProcessingResourceSpecification.activeResourceType.name);
		if (pcmProcessingResourceType == null) {
			log.info(
				"Transformation of Resource Type " + dmlProcessingResourceSpecification.activeResourceType.name +
					" not supported");
		}
		pcmProcessingResourceSpecification.activeResourceType_ActiveResourceSpecification = pcmProcessingResourceType as ProcessingResourceType;

	//pcmProcessingResourceSpecification.activeResourceType_ActiveResourceSpecification = transformProcessingResourceType(dmlProcessingResourceSpecification.activeResourceType);
	}

	def create pcmProcessingResourceType:ResourcetypeFactory.eINSTANCE.createProcessingResourceType transformProcessingResourceType(
		edu.kit.ipd.descartes.mm.resourcetype.ProcessingResourceType dmlProcessingResourceType) {

		pcmProcessingResourceType.id = dmlProcessingResourceType.id;
		pcmProcessingResourceType.entityName = dmlProcessingResourceType.name;

	//should be added to repository, repository should be saved;
	//but since DML allows only CPU and HDD which have predefined "equals" in PCM,
	//transformation of other resourcetypes not implemented
	}

	/**
	 * 
	 */
	def SchedulingPolicy getSchedulingPolicy(
		edu.kit.ipd.descartes.mm.resourceconfiguration.SchedulingPolicy dmlSchedulingPolicy) {
		var pcmSchedulingPolicy = getSchedulingPolicyByName(dmlSchedulingPolicy.literal)

		if (pcmSchedulingPolicy != null) {
			return (pcmSchedulingPolicy);
		}

		return transformSchedulingPolicy(dmlSchedulingPolicy);
	}

	def SchedulingPolicy getSchedulingPolicyByName(String dmlName) {

		/**
		 * PCM Rep 
		 *  <schedulingPolicies__ResourceRepository id="ProcessorSharing" entityName="Processor Sharing"/>
 		 *	<schedulingPolicies__ResourceRepository id="FCFS" entityName="First-Come-First-Serve"/>
 		 *	<schedulingPolicies__ResourceRepository id="Delay" entityName="Delay"/>
		 */
		for (pcmSchedulingPolicy : pcmDefaultResourceTypesRepository.schedulingPolicies__ResourceRepository) {
			if(pcmSchedulingPolicy.id == "ProcessorSharing" && dmlName == "PROCESSOR_SHARING") return pcmSchedulingPolicy;
			if(pcmSchedulingPolicy.id == "FCFS" && dmlName == "FCFS") return pcmSchedulingPolicy;
			if(pcmSchedulingPolicy.id == "Delay" && dmlName == "DELAY") return pcmSchedulingPolicy;
		}
		return null;
	}

	def create pcmSchedulingPolicy:ResourcetypeFactory.eINSTANCE.createSchedulingPolicy transformSchedulingPolicy(
		edu.kit.ipd.descartes.mm.resourceconfiguration.SchedulingPolicy dmlSchedulingPolicy) {
		pcmSchedulingPolicy.entityName = dmlSchedulingPolicy.getName();
		println("Transformation of SchedulingPolicy " + dmlSchedulingPolicy.getName() + " not supported");

	//TODO: transform non default SchedulingPolicies
	}

	def PCMRandomVariable transformBigIntegerToRandomVariable(BigInteger value) {
		var pcmrandomvar = makeRandomVar(value.toString);
		return (pcmrandomvar);
	}

	def PCMRandomVariable transformDoubleToRandomVariable(double value) {
		var pcmrandomvar = makeRandomVar(value.toString);
		return (pcmrandomvar);
	}

	def create pcmrandomvar:CoreFactory.eINSTANCE.createPCMRandomVariable makeRandomVar(String specification) {
		pcmrandomvar.specification = specification;
	}

	public def ResourceContainer getResourceContainer(Container dmlContainer) {
		return (mappingVMtoHardware.get(dmlContainer));
	}

	public def ResourceType getResourceType(edu.kit.ipd.descartes.mm.resourcetype.ResourceType dmlResourceType) {
		if(dmlResourceType == null || dmlResourceType.name == null) return null;
		var pcmResourceType = getDefaultPCMResourceTypeByName(dmlResourceType.name);
		if (pcmResourceType == null) {
			log.info("could not find ResourceType of" + dmlResourceType.name);
		}
		return (pcmResourceType);
	}

	/**
	 * add delay resource to a ResourceContainer
	 */
	public def void addDelayResourceToContainer(ResourceContainer rc) {
		for (activeresource : rc.activeResourceSpecifications_ResourceContainer) {
			if (activeresource.activeResourceType_ActiveResourceSpecification.entityName == "DELAY") {
				if (activeresource.processingRate_ProcessingResourceSpecification.specification != "1") {
					//we have a problem
				}
				return;
			}
		}
		var delay = ResourceenvironmentFactory.eINSTANCE.createProcessingResourceSpecification;
		var pcmProcessingResourceType = getDefaultPCMResourceTypeByName("DELAY");
		var speed = makeRandomVar("1");
		delay.processingRate_ProcessingResourceSpecification = speed;
		delay.activeResourceType_ActiveResourceSpecification = pcmProcessingResourceType as ProcessingResourceType;
		rc.activeResourceSpecifications_ResourceContainer.add(delay);
	}

	def String getLogString(String typeOriginal, String nameOriginal, String idOriginal, String typeTransform) {
		var logOutput = "Transforming " + typeOriginal;
		if (nameOriginal != null) {
			logOutput += " " + nameOriginal;
		} else if (idOriginal != null) {
			logOutput += " " + idOriginal;
		}
		logOutput += " to " + typeTransform;
		return logOutput;
	}

}
