package tools.descartes.dml2pcm

import de.uka.ipd.sdq.pcm.usagemodel.Workload
import edu.kit.ipd.descartes.mm.usageprofile.UsageProfile
import de.uka.ipd.sdq.pcm.usagemodel.UsagemodelFactory
import edu.kit.ipd.descartes.mm.usageprofile.OpenWorkloadType
import edu.kit.ipd.descartes.mm.usageprofile.WorkloadType
import edu.kit.ipd.descartes.mm.usageprofile.ClosedWorkloadType
import edu.kit.ipd.descartes.mm.usageprofile.ScenarioBehavior
import de.uka.ipd.sdq.pcm.usagemodel.AbstractUserAction
import edu.kit.ipd.descartes.mm.usageprofile.SystemCallUserAction
import de.uka.ipd.sdq.pcm.repository.OperationSignature
import edu.kit.ipd.descartes.mm.usageprofile.DelayUserAction
import edu.kit.ipd.descartes.mm.usageprofile.LoopUserAction
import edu.kit.ipd.descartes.mm.usageprofile.BranchUserAction
import edu.kit.ipd.descartes.mm.applicationlevel.functions.RandomVariable
import java.util.ArrayList
import de.uka.ipd.sdq.pcm.parameter.ParameterFactory
import edu.kit.ipd.descartes.mm.usageprofile.CallParameterSetting
import edu.kit.ipd.descartes.mm.applicationlevel.parameterdependencies.ParameterCharacterizationType
import de.uka.ipd.sdq.pcm.parameter.VariableCharacterisationType
import de.uka.ipd.sdq.stoex.StoexFactory
import org.apache.commons.logging.LogFactory
import org.apache.commons.logging.Log

class UsageProfileTransformator {
	private static final Log log = LogFactory.getLog(UsageProfileTransformator);
	
	Transformator transformator;
	new(Transformator transformator) {
		this.transformator = transformator;
	}
	
	def create pcmUsageModel:UsagemodelFactory.eINSTANCE.createUsageModel transformUsageProfile(UsageProfile dmlUsageProfile){
		log.info("Transforming UsageProfile "+dmlUsageProfile.name+" to UsageModel started");
		for(dmlUsageScenario:dmlUsageProfile.usageScenarios){
			var pcmUsageScenario = UsagemodelFactory.eINSTANCE.createUsageScenario;
			log.info(" Transforming UsageScenario "+dmlUsageScenario.id);
			switch(dmlUsageScenario.workloadType){
				OpenWorkloadType: pcmUsageScenario.workload_UsageScenario = transformOpenWorkloadType(dmlUsageScenario.workloadType as OpenWorkloadType)
				ClosedWorkloadType: pcmUsageScenario.workload_UsageScenario = transformClosedWorkloadType(dmlUsageScenario.workloadType as ClosedWorkloadType)
			}			
			pcmUsageScenario.scenarioBehaviour_UsageScenario= transformScenarioBehavior(dmlUsageScenario.scenarioBehavior);
			pcmUsageModel.usageScenario_UsageModel.add(pcmUsageScenario)
		}		
		log.info("Transforming UsageProfile "+dmlUsageProfile.name+" to UsageModel finished");
	}
	
	def create pcmScenarioBehavior:UsagemodelFactory.eINSTANCE.createScenarioBehaviour transformScenarioBehavior(ScenarioBehavior dmlScenarioBehavior){
		log.info("Transforming ScenarioBehavior");
		val startAction = UsagemodelFactory.eINSTANCE.createStart;
		val stopAction = UsagemodelFactory.eINSTANCE.createStop;
		var lastAction = startAction as AbstractUserAction;
		pcmScenarioBehavior.actions_ScenarioBehaviour.add(startAction);
		pcmScenarioBehavior.actions_ScenarioBehaviour.add(stopAction);
		
		
		for(dmlAction: dmlScenarioBehavior.actions){
			var pcmAction = transformUserAction(dmlAction);
			pcmAction.predecessor = lastAction;
			lastAction.successor = pcmAction;
			lastAction = pcmAction;			
			pcmScenarioBehavior.actions_ScenarioBehaviour.add(pcmAction);
		}
		
		lastAction.successor = stopAction;
		stopAction.predecessor = lastAction;
		
		
	}
	
	def AbstractUserAction transformUserAction(edu.kit.ipd.descartes.mm.usageprofile.AbstractUserAction dmlUserAction){
			
		switch(dmlUserAction){
			SystemCallUserAction: transformSystemCallUserAction(dmlUserAction)
			DelayUserAction: transformDelayAction(dmlUserAction)
			LoopUserAction: transformLoopUserAction(dmlUserAction)
		}
		
	}
	
	def create pcmBranchAction:UsagemodelFactory.eINSTANCE.createBranch transformBranchUserAction(BranchUserAction dmlBranchUserAction){
		log.info("Transforming BranchAction");
		var probabilities = new StoexTransformator().transformRandomVariableToListOfProbabilities(dmlBranchUserAction.branchingProbabilities);
		var counter = 0;
		for(dmlBranch: dmlBranchUserAction.branches){
			var pcmBranchTransition = UsagemodelFactory.eINSTANCE.createBranchTransition;
			log.info("Transforming Branch of BranchAction");
			pcmBranchTransition.branchedBehaviour_BranchTransition = transformScenarioBehavior(dmlBranch);
			if(probabilities.size<counter){
				pcmBranchTransition.branchProbability = probabilities.get(counter++);
			}
			else{
				log.info("BranchUserAction probability for branch "+counter+" not defined, setting to zero");
				pcmBranchTransition.branchProbability = 0;
			}
		
		}		
	}
	
	def create pcmLoopAction:UsagemodelFactory.eINSTANCE.createLoop transformLoopUserAction(LoopUserAction dmlLoopUserAction){
		pcmLoopAction.loopIteration_Loop = new StoexTransformator().transformRandomVariable(dmlLoopUserAction.loopIterationCount);
		pcmLoopAction.bodyBehaviour_Loop= transformScenarioBehavior(dmlLoopUserAction.loopBodyScenarioBehavior);
	}
	def create pcmDelayAction:UsagemodelFactory.eINSTANCE.createDelay transformDelayAction(DelayUserAction dmlDelayAction){
		pcmDelayAction.timeSpecification_Delay = new StoexTransformator().transformRandomVariable(dmlDelayAction.delay);
	}
	
	def create pcmSystemCall:UsagemodelFactory.eINSTANCE.createEntryLevelSystemCall transformSystemCallUserAction(SystemCallUserAction action) {
		log.info("Transforming SystemCallUserAction");
		pcmSystemCall.providedRole_EntryLevelSystemCall = transformator.repositoryTransformator.getTransformedProvidedRole(action.providingRole);
		pcmSystemCall.operationSignature__EntryLevelSystemCall = transformator.repositoryTransformator.getTransformedOperationSignature(action.signature) as OperationSignature;
		for(dmlCallParameterSetting:action.inputParameterSettings){
			pcmSystemCall.inputParameterUsages_EntryLevelSystemCall.add(transformCallParameterSetting(dmlCallParameterSetting));
		}
		
		//pcmSystemCall.inputParameterUsages_EntryLevelSystemCall
	}
	
	def create pcmVariableUsage:ParameterFactory.eINSTANCE.createVariableUsage transformCallParameterSetting(CallParameterSetting dmlCallParameterSetting){	
		//(dml)callparametersetting: 	name:string
		//								characterization:ParameterCharacterizationType (VALUE or NUMBER_OF_ELEMENTS)
		//								value: RandomVariable
		
		//TODO: needs testing
		log.info("Transforming CallParameterSetting to VariableUsage");
		var pcmVarChar = ParameterFactory.eINSTANCE.createVariableCharacterisation
		if(dmlCallParameterSetting.characterization == ParameterCharacterizationType.NUMBER_OF_ELEMENTS){
			pcmVarChar.type = VariableCharacterisationType.NUMBER_OF_ELEMENTS;
		}
		else if(dmlCallParameterSetting.characterization == ParameterCharacterizationType.VALUE){
			pcmVarChar.type = VariableCharacterisationType.VALUE;
		}
		pcmVarChar.specification_VariableCharacterisation = new StoexTransformator().transformRandomVariable(dmlCallParameterSetting.value);				
		pcmVariableUsage.variableCharacterisation_VariableUsage.add(pcmVarChar);
		
		pcmVariableUsage.namedReference__VariableUsage = StoexFactory.eINSTANCE.createVariableReference;
		pcmVariableUsage.namedReference__VariableUsage.referenceName = dmlCallParameterSetting.name;
	}
	
	def create pcmClosedWorkload:UsagemodelFactory.eINSTANCE.createClosedWorkload transformClosedWorkloadType(ClosedWorkloadType dmlClosedWorkloadType) {
		log.info("Transforming ClosedWorkload");
		pcmClosedWorkload.population = dmlClosedWorkloadType.population.intValue;
		pcmClosedWorkload.thinkTime_ClosedWorkload = new StoexTransformator().transformRandomVariable(dmlClosedWorkloadType.thinkTime);
	}
	
	def create pcmOpenWorkload:UsagemodelFactory.eINSTANCE.createOpenWorkload transformOpenWorkloadType(OpenWorkloadType dmlOpenWorkloadType) {
		log.info("Transforming OpendWorkload");
		pcmOpenWorkload.interArrivalTime_OpenWorkload = new StoexTransformator().transformRandomVariable(dmlOpenWorkloadType.interArrivalTime);
	}
	
	
}