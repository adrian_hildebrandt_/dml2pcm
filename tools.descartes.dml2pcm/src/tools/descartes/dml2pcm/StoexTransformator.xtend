package tools.descartes.dml2pcm

import de.uka.ipd.sdq.pcm.core.CoreFactory
import edu.kit.ipd.descartes.mm.applicationlevel.functions.BinaryBooleanExpression
import edu.kit.ipd.descartes.mm.applicationlevel.functions.BoolSampleList
import edu.kit.ipd.descartes.mm.applicationlevel.functions.BooleanLiteral
import edu.kit.ipd.descartes.mm.applicationlevel.functions.BooleanOperation
import edu.kit.ipd.descartes.mm.applicationlevel.functions.BoxedPDF
import edu.kit.ipd.descartes.mm.applicationlevel.functions.CompareOperation
import edu.kit.ipd.descartes.mm.applicationlevel.functions.Comparison
import edu.kit.ipd.descartes.mm.applicationlevel.functions.DoubleLiteral
import edu.kit.ipd.descartes.mm.applicationlevel.functions.DoubleSampleList
import edu.kit.ipd.descartes.mm.applicationlevel.functions.EnumSampleList
import edu.kit.ipd.descartes.mm.applicationlevel.functions.ExponentialDistribution
import edu.kit.ipd.descartes.mm.applicationlevel.functions.Expression
import edu.kit.ipd.descartes.mm.applicationlevel.functions.FormalParameter
import edu.kit.ipd.descartes.mm.applicationlevel.functions.Function
import edu.kit.ipd.descartes.mm.applicationlevel.functions.IfElseExpression
import edu.kit.ipd.descartes.mm.applicationlevel.functions.IntLiteral
import edu.kit.ipd.descartes.mm.applicationlevel.functions.IntSampleList
import edu.kit.ipd.descartes.mm.applicationlevel.functions.Literal
import edu.kit.ipd.descartes.mm.applicationlevel.functions.NormalDistribution
import edu.kit.ipd.descartes.mm.applicationlevel.functions.Power
import edu.kit.ipd.descartes.mm.applicationlevel.functions.ProbabilityDensityFunction
import edu.kit.ipd.descartes.mm.applicationlevel.functions.ProbabilityFunction
import edu.kit.ipd.descartes.mm.applicationlevel.functions.ProbabilityMassFunction
import edu.kit.ipd.descartes.mm.applicationlevel.functions.Product
import edu.kit.ipd.descartes.mm.applicationlevel.functions.ProductOperation
import edu.kit.ipd.descartes.mm.applicationlevel.functions.RandomVariable
import edu.kit.ipd.descartes.mm.applicationlevel.functions.Term
import edu.kit.ipd.descartes.mm.applicationlevel.functions.TermOperation
import edu.kit.ipd.descartes.mm.applicationlevel.functions.UnaryBooleanExpression
import java.util.ArrayList
import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory

class StoexTransformator {
	private static final Log log = LogFactory.getLog(StoexTransformator);

	def create pcmRandomVariable:CoreFactory.eINSTANCE.createPCMRandomVariable transformRandomVariable(
		RandomVariable dmlRandomVariable) {
		var rv = pcmRandomVariable as de.uka.ipd.sdq.stoex.RandomVariable;
		if (dmlRandomVariable == null || dmlRandomVariable.probFunction == null) {
			rv.specification = "0";
			return;
		}
		rv.specification = probabilityFunctionToSpec(dmlRandomVariable.probFunction);
		log.info("Transformed DML RandomVariable into PCMRandomVariable with specification: "+pcmRandomVariable.specification);
	}

	def String probabilityFunctionToSpec(ProbabilityFunction dmlProbFunc) {
		var spc = "";
		switch (dmlProbFunc) {
			ProbabilityMassFunction:
				spc = transformProbabilityMassFunctionToSpec(dmlProbFunc)
			Literal:
				spc = transformLiteralToSpec(dmlProbFunc)
			ProbabilityDensityFunction:
				spc = transformPdfToSpec(dmlProbFunc)
			default: {
				spc = "1";
				println("can not transform " + dmlProbFunc.toString + " to a specification for PCM RandomVariables")
			}
		}
	}

	def String transformPdfToSpec(ProbabilityDensityFunction dmlFunction) {
		var spec = "";
		switch (dmlFunction) {
			//SamplePDF only used internally(kozpar08,page 95)
			BoxedPDF:
				spec += boxedPdfToSpec(dmlFunction)
			ExponentialDistribution:
				spec += "Exp(" + dmlFunction.rate + ")"
			NormalDistribution:
				spec += "Norm(" + dmlFunction.mu + "," + dmlFunction.sigma + ")"
			default: {
				spec = "1";
				log.info("RandomVar " + dmlFunction.toString + " is not transformed, set to 1")
			}
		}
		return spec;
	}

	def String boxedPdfToSpec(BoxedPDF pdf) {
		var spec = "DoublePDF[";
		for (sample : pdf.sample) {
			spec += "(" + sample.value + ";" + sample.probability + ")";
		}

		spec += "]";
		return spec;
	}

	def String transformLiteralToSpec(Literal dmlLiteral) {
		var spec = "";
		switch (dmlLiteral) {
			BooleanLiteral: spec += dmlLiteral.value
			IntLiteral: spec += dmlLiteral.value
			DoubleLiteral: spec += dmlLiteral.value
			default: log.info("literal of type " + dmlLiteral.class.name + " is not transformed")
		}
		return spec;
	}

	def String transformProbabilityMassFunctionToSpec(ProbabilityMassFunction dmlProbMassFunction) {
		var spec = "";
		var samplelist = dmlProbMassFunction.samples;
		switch (samplelist) {
			IntSampleList: spec += intPMFtoSpec(samplelist)
			DoubleSampleList: spec += doublePMFtoSpec(samplelist)
			BoolSampleList: spec += boolPMFtoSpec(samplelist)
			EnumSampleList: spec += enumPMFtoSpec(samplelist)
		}
		return spec;
	}

	def String intPMFtoSpec(IntSampleList dmlIntSampleList) {
		var spec = "IntPMF[";
		for (sample : dmlIntSampleList.items) {
			spec += "(" + sample.value + ";" + sample.probability + ")";
		}
		spec += "]";
		return (spec);
	}

	def String doublePMFtoSpec(DoubleSampleList dmlSampleList) {
		var spec = "DoublePMF[";
		for (sample : dmlSampleList.items) {
			spec += "(" + sample.value + ";" + sample.probability + ")";
		}
		spec += "]";
		return (spec);
	}

	def String boolPMFtoSpec(BoolSampleList dmlSampleList) {
		var spec = "BoolPMF[";
		for (sample : dmlSampleList.items) {

			spec += "(" + sample.value + ";" + sample.probability + ")";
		}
		spec += "]";
		return (spec);
	}

	def String enumPMFtoSpec(EnumSampleList dmlSampleList) {
		var spec = "EnumPMF[";
		for (sample : dmlSampleList.items) {
			spec += "(\"" + sample.value + "\";" + sample.probability + ")";
		}
		spec += "]";
		return (spec);
	}

	def create pcmRandomVariable:CoreFactory.eINSTANCE.createPCMRandomVariable transformRandomVariable(
		Expression dmlExpression) {
			pcmRandomVariable.specification = expressionToSpecification(dmlExpression);
			log.info("Transformed DML RandomVariable into PCMRandomVariable with specification: "+pcmRandomVariable.specification);
	}

	def String expressionToSpecification(Expression dmlExpression) {
		var ret = "(";
		switch (dmlExpression) {
			ProbabilityFunction:
				ret += probabilityFunctionToSpec(dmlExpression)
			FormalParameter:
				ret += "\""+dmlExpression.value+"\""
			Function: {
				log.info("Expression of type Function is not transformed,set to 1");
				ret += "1";
			}
			IfElseExpression:
				ret += ifelseToSpec(dmlExpression)
			Term:
				ret += termToSpec(dmlExpression)
			Product:
				ret += prodToSpec(dmlExpression)
			Comparison:
				ret += compToSpec(dmlExpression)
			UnaryBooleanExpression:
				ret += unaryboolToSpec(dmlExpression)
			BinaryBooleanExpression:
				ret += binaryboolToSpec(dmlExpression)
			Power:
				ret += expressionToSpecification(dmlExpression.base) + "^" +
					expressionToSpecification(dmlExpression.exponent)
			default:
				ret += 0
		}
		ret += ")"
		return ret;
	}

	def binaryboolToSpec(BinaryBooleanExpression expression) {
		var symbol = "AND";
		if (expression.operation == BooleanOperation.OR)
			symbol = "OR"
		else if(expression.operation == BooleanOperation.XOR) symbol = "XOR"
		var ret = expressionToSpecification(expression.left) + symbol + expressionToSpecification(expression.right)
		return ret

	}

	def String unaryboolToSpec(UnaryBooleanExpression expression) {
		//DML doesnt specify anything besides inner
		return expressionToSpecification(expression.inner)
	}

	def String compToSpec(Comparison comparison) {
		var symbol = "<";
		if (comparison.operation == CompareOperation.GREATER)
			symbol = ">"
		else if (comparison.operation == CompareOperation.EQUALS)
			symbol = "=="
		else if (comparison.operation == CompareOperation.LESSEQUAL)
			symbol = "<="
		else if (comparison.operation == CompareOperation.GREATEREQUAL)
			symbol = ">="
		else if(comparison.operation == CompareOperation.NOTEQUAL) symbol = "<>"
		var ret = expressionToSpecification(comparison.left) + symbol + expressionToSpecification(comparison.right)
		return ret
	}

	def String prodToSpec(Product product) {
		var symbol = "*";
		if(product.operation == ProductOperation.DIV) symbol = "/" else if(product.operation == ProductOperation.DIV) symbol = "%"
		var ret = expressionToSpecification(product.left) + symbol + expressionToSpecification(product.right)
		return ret
	}

	def String termToSpec(Term term) {
		var symbol = "+";
		if(term.operation == TermOperation.SUB) symbol = "-"
		var ret = expressionToSpecification(term.left) + symbol + expressionToSpecification(term.right)
		return ret
	}

	def String ifelseToSpec(IfElseExpression expression) {
		var ret = ""
		ret += expressionToSpecification(expression.condition)
		ret += "?";
		ret += expressionToSpecification(expression.trueExp);
		ret += ":";
		ret += expressionToSpecification(expression.falseExp);
	}

	public def ArrayList<Double> transformRandomVariableToListOfProbabilities(RandomVariable dmlRandomVariable) {
		log.info("Transforming ProbabilityMassFunction into List of Probabilites")
		var probabilities = new ArrayList<Double>();

		var probFunction = dmlRandomVariable.probFunction;
		if (probFunction instanceof ProbabilityMassFunction) {
			var samplelist = probFunction.samples;
			switch (samplelist) {
				IntSampleList:
					for (sample : samplelist.items) {
						probabilities.add(sample.probability.doubleValue);
					}
				BoolSampleList:
					for (sample : samplelist.items) {
						probabilities.add(sample.probability.doubleValue);
					}
				DoubleSampleList:
					for (sample : samplelist.items) {
						probabilities.add(sample.probability.doubleValue);
					}
				EnumSampleList:
					for (sample : samplelist.items) {
						probabilities.add(sample.probability.doubleValue);
					}
			}
		}
		log.info("Result: "+probabilities.toString());
		return probabilities;
	}

}
