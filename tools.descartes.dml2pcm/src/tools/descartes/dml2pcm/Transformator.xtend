package tools.descartes.dml2pcm;

import edu.kit.ipd.descartes.mm.usageprofile.UsageProfile
import edu.kit.ipd.descartes.mm.deployment.Deployment
import edu.kit.ipd.descartes.mm.resourcelandscape.ResourcelandscapeFactory
import de.uka.ipd.sdq.pcm.resourceenvironment.ResourceEnvironment
import de.uka.ipd.sdq.pcm.allocation.Allocation
import de.uka.ipd.sdq.pcm.usagemodel.UsageModel
import de.uka.ipd.sdq.pcm.repository.Repository

public class Transformator {
	var BehaviorTransformator behaviorTransformator = new BehaviorTransformator;
	var DeploymentTransformator deploymentTransformator; //= new DeploymentTransformator;
	var RepositoryTransformator repositoryTransformator; //= new RepositoryTransformator;
	var ResourceLandscapeTransformator resourceLandscapeTransformator; //= new ResourceLandscapeTransformator;
	var UsageProfileTransformator usageprofileTransformator; // = new UsageProfileTransformator;	
	
	var ResourceEnvironment pcmResourceEnvironment;
	var Allocation pcmAllocation;
	var UsageModel pcmUsage;
	var Repository pcmRepository;
	var de.uka.ipd.sdq.pcm.system.System pcmSystem;
	
	new (){
		reset();
	}
	
	new(Deployment deployment, UsageProfile usage){
		reset();
		transformDML2PCM(deployment,usage);
	}
	
	
	
	public def void reset() {
		behaviorTransformator = new BehaviorTransformator(this);
		deploymentTransformator = new DeploymentTransformator(this);
		repositoryTransformator = new RepositoryTransformator(this);
		resourceLandscapeTransformator = new ResourceLandscapeTransformator(this);
		usageprofileTransformator = new UsageProfileTransformator(this);
	}
	
	public def void transformDML2PCM(Deployment deployment, UsageProfile usage){
		pcmResourceEnvironment = resourceLandscapeTransformator.transformDistributedDataCenter(deployment.targetResourceLandscape);
			val dmlRepository = deployment.system.assemblyContexts.get(0).encapsulatedComponent.repository;
		pcmRepository = repositoryTransformator.transformRepository(dmlRepository);
		pcmAllocation = deploymentTransformator.transformDeployment(deployment);
		pcmUsage = usageprofileTransformator.transformUsageProfile(usage);	
		pcmSystem = pcmAllocation.system_Allocation;	
	}
	
	public def Repository getRepository(){
		return pcmRepository;
	}
	public def Allocation getAllocation(){
		return pcmAllocation;
	}
	public def UsageModel getUsage(){
		return pcmUsage;
	}
	public def de.uka.ipd.sdq.pcm.system.System getSystem(){
		return pcmSystem;
	}
	public def ResourceEnvironment getResourceEnvironment(){
		return pcmResourceEnvironment;
	}
	public def BehaviorTransformator getBehaviorTransformator(){
		return behaviorTransformator;
	}
	public def DeploymentTransformator getDeploymentTransformator(){
		return deploymentTransformator;
	}
	public def RepositoryTransformator getRepositoryTransformator(){
		return repositoryTransformator;
	}
	public def ResourceLandscapeTransformator getResourceLandscapeTransformator(){
		return resourceLandscapeTransformator;
	}
	public def UsageProfileTransformator getUsageProfileTransformator(){
		return usageprofileTransformator;
	}
	
	
	
}
