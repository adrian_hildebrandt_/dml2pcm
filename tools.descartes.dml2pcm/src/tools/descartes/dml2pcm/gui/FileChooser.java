 
package tools.descartes.dml2pcm.gui;
 
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.filechooser.*;

import org.apache.log4j.Logger;

import tools.descartes.dml2pcm.Starter;
 
/*
 * FileChooserDemo.java uses these files:
 *   images/Open16.gif
 *   images/Save16.gif
 */
public class FileChooser extends JPanel
implements ActionListener {
	static private final String newline = "\n";
	JButton dmlButton, pcmButton,transformButton;
	JTextArea dml;
	JTextArea pcm;
	JFileChooser fc;
	
	ArrayList<JButton> moduleButtons;
	ArrayList<JTextArea> moduleTxts;
	ArrayList<File> moduleFiles = new ArrayList<File>(5);
	String[] modules = {"repository","deployment","usageprofile","resourcelandscape","system"};
	
	File[] transformationFiles = new File[3];
	
	public FileChooser() {
		super(new BorderLayout());
		
		//Create a file chooser
		fc = new JFileChooser();

		fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

		JPanel dmlPanel = new JPanel();
		dmlButton = new JButton("Set DML");
		dmlButton.addActionListener(this);
		
		dml = new JTextArea(1,50);
		dml.setMargin(new Insets(5,5,5,5));
		dml.setEditable(false);
		dml.setText("Path to Transformation Input");
		JPanel dmlloc = new JPanel();
        dmlloc.add(dml);
        
        dmlPanel.add(dmlButton);
        dmlPanel.add(dmlloc);
        dmlPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
                
        
        JPanel pcmPanel = new JPanel();
		pcmButton = new JButton("Set PCM");
		pcmButton.addActionListener(this);
		pcm = new JTextArea(1,50);
		pcm.setText("Path to Transformation Output");
		pcm.setMargin(new Insets(5,5,5,5));
		pcm.setEditable(false);
		JPanel pcmloc = new JPanel();
        pcmloc.add(pcm);
       
        pcmPanel.add(pcmButton);
        pcmPanel.add(pcmloc);
        pcmPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        
		JPanel buttonPanel = new JPanel(); //use FlowLayout
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
		buttonPanel.add(dmlPanel);
		buttonPanel.add(pcmPanel);
		
		
		
		moduleButtons = new ArrayList<JButton>();
		moduleTxts = new ArrayList<JTextArea>();
		JPanel files = new JPanel();
		JPanel names = new JPanel();
		JPanel txts = new JPanel();
		JPanel buttons = new JPanel();
		names.setLayout(new BoxLayout(names, BoxLayout.Y_AXIS));
		txts.setLayout(new BoxLayout(txts, BoxLayout.Y_AXIS));
		buttons.setLayout(new BoxLayout(buttons, BoxLayout.Y_AXIS));
		
		files.setLayout(new FlowLayout(FlowLayout.LEFT));
		//files.setLayout(new BoxLayout(files, BoxLayout.X_AXIS));
		files.add(names);		
		files.add(buttons);
		files.add(txts);
		
		for(int i = 0;i<modules.length;i++){
			JPanel panel = new JPanel();
			JPanel name = new JPanel();
			JLabel namel = new JLabel(modules[i]);
			name.add(namel);			
			
			JTextArea txt = new JTextArea(1,5);
			txt.setMargin(new Insets(5,5,5,5));
			txt.setEditable(false);
			txt.setText("File used for Transformation");
			names.add(name);
			name.setLayout(new FlowLayout(FlowLayout.LEFT));
			txts.add(txt);
			
			//panel.setLayout(new FlowLayout(FlowLayout.LEFT));
			
			
			
			moduleTxts.add(txt);
			moduleFiles.add(null);
			
		}
		buttonPanel.add(files);
		
		JPanel transformPanel = new JPanel();
		transformPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		transformButton = new JButton("Start Transformation");
		transformButton.addActionListener(this);
		transformPanel.add(transformButton);
		
		buttonPanel.add(transformPanel,BorderLayout.SOUTH);
		//buttonPanel.add(transformPanel);
		
		//Add the buttons and the log to this panel.
		add(buttonPanel, BorderLayout.PAGE_START);
		
		
		
		
		
	}
	File dmlFile, pcmFile;
	
	public File getFile(File path, String ending, String preferredname){
		File[] files= path.listFiles();
		ArrayList<File> correctendings = new ArrayList<File>();
		for(File contained:files){
			if(contained.isDirectory()) continue;
			String n = contained.getName().toLowerCase();
			if(n.endsWith("."+ending)){
				correctendings.add(contained);
			}
		}
		if(correctendings.size()==0) return null;
		if(correctendings.size()>1){
			for(File contained:correctendings){
				if(contained.getName().toLowerCase().startsWith(preferredname)){
					return contained;
				}
			}
			
			
		}
		return correctendings.get(0);
		
	}
	
	
	
	public void actionPerformed(ActionEvent e) {
		//Handle open button action.
		if (e.getSource() == dmlButton) {
			int returnVal = fc.showOpenDialog(FileChooser.this);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				if(file.isFile()){
					file = file.getParentFile();
				}
				fc.setCurrentDirectory(file);
				dml.setText(file.getAbsolutePath());
				HashMap<String,File> files = Starter.validateDMLPath(file.getAbsolutePath());
				
				transformationFiles[0] = files.get("deployment");
				transformationFiles[1] = files.get("usageprofile");
				for(int i = 0;i<modules.length;i++){
					File f = files.get(modules[i]);
					//System.out.println(f);
					if(f!=null){
						moduleTxts.get(i).setText(f.getName());
						moduleFiles.set(i, f);
					}
					
				}
				if(pcmFile == null){
					pcmFile = new File(file.getAbsolutePath()+"/Transformation");
					pcm.setText(pcmFile.getAbsolutePath());
					transformationFiles[2] = pcmFile;
				}
				/*
				HashMap<String,Integer> mostest = new HashMap<String,Integer>();
				File[] files= file.listFiles();
				for(File contained:files){
					if(contained.isDirectory()) continue;
					
					String name = contained.getName();
					String nn = name.toLowerCase();
					nn = nn.substring(0,nn.indexOf("."));
					if(mostest.get(nn)==null){
						mostest.put(nn,1);
					}
					else{
						mostest.put(nn,mostest.get(nn)+1);
					}
				}
				
				int getmax = 0;
				String foundmost = null;
				Iterator<Entry<String, Integer>> it = mostest.entrySet().iterator();
				while(it.hasNext()){
					Entry<String, Integer> entry =it.next();
					if(entry.getValue()>getmax){
						getmax = entry.getValue();
						foundmost = entry.getKey();
					}
				}
				
				for(int i =0;i<modules.length;i++){
					File f = getFile(file,modules[i],foundmost);
					moduleFiles.set(i, f);
					if(f!=null)	moduleTxts.get(i).setText(f.getName());
					else moduleTxts.get(i).setText("");
				}
				
				
				if(pcmFile == null){
					pcmFile = new File(file.getAbsolutePath()+"/Transformation");
					pcm.setText(pcmFile.getAbsolutePath());
				}
				*/
				
				
			} else {
				
			}
			

			//Handle save button action.
		} else if (e.getSource() == pcmButton) {
			int returnVal = fc.showSaveDialog(FileChooser.this);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				pcm.setText(file.getAbsolutePath());
				fc.setCurrentDirectory(file);
				transformationFiles[2] = file;
				
			} else {
				
			}
		}
		else if (e.getSource() == transformButton) {
			if(pcmFile == null) return;
			for(int i =0;i<modules.length;i++){
				//System.out.println(moduleFiles.get(i).getName());
				if(moduleFiles.get(i)==null) return;
			}
			Starter.pathFromGui(transformationFiles);			
		}
		else {			
			for(int i =0;i<modules.length;i++){
				if(e.getSource()== moduleButtons.get(i)){
					int returnVal = fc.showOpenDialog(FileChooser.this);
					File file = fc.getSelectedFile();
					if(file.isDirectory()) return;
					moduleTxts.get(i).setText(file.getName());
					moduleFiles.set(i, file);
					
				}
			}
			
			
		}
	}

	
	/**
	 * Create the GUI and show it.  For thread safety,
	 * this method should be invoked from the
	 * event dispatch thread.
	 */
	public static void createAndShowGUI() {
		//Create and set up the window.
		JFrame frame = new JFrame("Set DML Location");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//Add content to the window.
		frame.add(new FileChooser());

		//Display the window.
		frame.pack();
		frame.setVisible(true);
	}

}


