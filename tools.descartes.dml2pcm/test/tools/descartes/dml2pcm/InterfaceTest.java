package tools.descartes.dml2pcm;

import static org.junit.Assert.*;

import org.junit.Test;

import de.uka.ipd.sdq.pcm.repository.OperationInterface;
import de.uka.ipd.sdq.pcm.repository.OperationSignature;
import edu.kit.ipd.descartes.mm.applicationlevel.repository.CompositeDataType;
import edu.kit.ipd.descartes.mm.applicationlevel.repository.DataType;
import edu.kit.ipd.descartes.mm.applicationlevel.repository.InnerDeclaration;
import edu.kit.ipd.descartes.mm.applicationlevel.repository.Interface;
import edu.kit.ipd.descartes.mm.applicationlevel.repository.Parameter;
import edu.kit.ipd.descartes.mm.applicationlevel.repository.ParameterModifier;
import edu.kit.ipd.descartes.mm.applicationlevel.repository.PrimitiveDataType;
import edu.kit.ipd.descartes.mm.applicationlevel.repository.PrimitiveTypeEnum;
import edu.kit.ipd.descartes.mm.applicationlevel.repository.RepositoryFactory;
import edu.kit.ipd.descartes.mm.applicationlevel.repository.Signature;

public class InterfaceTest {

	@Test
	public void testInterface() {
		Interface interfaceUnderTest = makeInterface("to Test",4);
		Transformator transformator = new Transformator();
		OperationInterface pcmInterface = transformator.getRepositoryTransformator().transformOperationInterface(interfaceUnderTest);
		assertTrue(pcmInterface.getSignatures__OperationInterface().size()==4);
		assertTrue(pcmInterface.SignaturesHaveToBeUniqueForAnInterface(null, null));
	}
	
	@Test
	public void testParentInterface(){
		Interface parentInterface = makeInterface("to parent", 4);
		Interface interfaceUnderTest = makeInterface("to test",2);
		interfaceUnderTest.getParentInterfaces().add(parentInterface);
		Transformator transformator = new Transformator();
		OperationInterface pcmInterface = transformator.getRepositoryTransformator().transformOperationInterface(interfaceUnderTest);
		assertTrue(pcmInterface.getSignatures__OperationInterface().size()==2);
		assertTrue(pcmInterface.getParentInterfaces__Interface().size()==1);
		assertTrue(((OperationInterface)pcmInterface.getParentInterfaces__Interface().get(0)).getSignatures__OperationInterface().size() ==4);
		assertTrue(pcmInterface.SignaturesHaveToBeUniqueForAnInterface(null, null));
	}
	@Test
	public void testSignatures(){
		Signature dmlSig = makeSignature("sig to test");
		dmlSig.setReturnType(makePrimitiveData("signature to test"));	
		for(int i = 0;i<4;i++){
			Parameter p = makeParameter("sig to test"+i, ParameterModifier.IN);
			switch(i){
				case(1):p.setModifier(ParameterModifier.INOUT);
				case(2):p.setModifier(ParameterModifier.OUT);
				case(3):p.setModifier(ParameterModifier.NONE);
			}
			dmlSig.getParameters().add(p);
		}
		Transformator transformator = new Transformator();
		OperationSignature pcmSig = transformator.getRepositoryTransformator().transformOperationSignature(dmlSig);
		assertTrue(cmpSignatures(dmlSig,pcmSig));
		assertTrue(pcmSig.ParameterNamesHaveToBeUniqueForASignature(null, null));
	}
	@Test 
	public void testParameters(){
		Parameter p = makeParameter("to test", ParameterModifier.IN);
		Transformator transformator = new Transformator();
		de.uka.ipd.sdq.pcm.repository.Parameter pcmP = transformator.getRepositoryTransformator().transformParameter(p);
		assertTrue(cmpParameter(p,pcmP));		
	}
	@Test
	public void testPrimitiveDataType(){
		PrimitiveDataType dmlPD = makePrimitiveData("test");
		Transformator transformator = new Transformator();
		de.uka.ipd.sdq.pcm.repository.PrimitiveDataType pcmPD = transformator.getRepositoryTransformator().transformPrimitiveDataType(dmlPD);
		assertTrue(cmpDataTypes(dmlPD, pcmPD));
	}
	@Test 
	public void testCompositeDataType(){
		CompositeDataType dmlCD = makeCompositeData("test");
		Transformator transformator = new Transformator();
		de.uka.ipd.sdq.pcm.repository.CompositeDataType pcmCD = transformator.getRepositoryTransformator().transformCompositeDataType(dmlCD);
		assertTrue(cmpDataTypes(dmlCD,pcmCD));
	}
	
	
	private boolean cmpSignatures(Signature dmlSig,
			OperationSignature pcmSig) {
		
		if(dmlSig.getParameters().size()!=pcmSig.getParameters__OperationSignature().size()) return false;
		for(Parameter dmlPara:dmlSig.getParameters()){
			boolean found = false;
			for(de.uka.ipd.sdq.pcm.repository.Parameter pcmPara:pcmSig.getParameters__OperationSignature()){
				if(cmpParameter(dmlPara,pcmPara)) found = true;
			}
			if(!found) return false;			
		}
		if(!cmpDataTypes(dmlSig.getReturnType(), pcmSig.getReturnType__OperationSignature())) return false;		
		return true;
	}
	
	private boolean cmpParameter(Parameter dmlPara, de.uka.ipd.sdq.pcm.repository.Parameter pcmPara){
		if(!cmpDataTypes(dmlPara.getDataType(),pcmPara.getDataType__Parameter())) return false;
		if(dmlPara.getModifier() == ParameterModifier.IN && pcmPara.getModifier__Parameter()!= de.uka.ipd.sdq.pcm.repository.ParameterModifier.IN){
			return false;
		}
		if(dmlPara.getModifier() == ParameterModifier.INOUT && pcmPara.getModifier__Parameter()!= de.uka.ipd.sdq.pcm.repository.ParameterModifier.INOUT){
			return false;
		}
		if(dmlPara.getModifier() == ParameterModifier.OUT && pcmPara.getModifier__Parameter()!= de.uka.ipd.sdq.pcm.repository.ParameterModifier.OUT){
			return false;
		}
		if(dmlPara.getModifier() == ParameterModifier.NONE && pcmPara.getModifier__Parameter()!= de.uka.ipd.sdq.pcm.repository.ParameterModifier.NONE){
			return false;
		}
		if(!dmlPara.getName().equals(pcmPara.getParameterName())) return false;
		return true;
	}
	
	private boolean cmpDataTypes(DataType dmlType, de.uka.ipd.sdq.pcm.repository.DataType pcmType){
		if(dmlType instanceof CompositeDataType && pcmType instanceof de.uka.ipd.sdq.pcm.repository.CompositeDataType){
				return(cmpCompositeDataType((CompositeDataType)dmlType, (de.uka.ipd.sdq.pcm.repository.CompositeDataType)pcmType));
		}			
		
		if(dmlType instanceof PrimitiveDataType && pcmType instanceof de.uka.ipd.sdq.pcm.repository.PrimitiveDataType){
			return(cmpPrimitiveDataType((PrimitiveDataType)dmlType, (de.uka.ipd.sdq.pcm.repository.PrimitiveDataType) pcmType));
		}
		return false;		
	}
	
	private boolean cmpPrimitiveDataType(PrimitiveDataType dmlType,
			de.uka.ipd.sdq.pcm.repository.PrimitiveDataType pcmType) {
		if(dmlType.getType().getLiteral().toLowerCase().equals(pcmType.getType().getLiteral().toLowerCase())) return true;
		return false;	
		
	}

	private boolean cmpCompositeDataType(CompositeDataType dmlCd, de.uka.ipd.sdq.pcm.repository.CompositeDataType pcmCd){
		if(dmlCd.getInnerDeclarations().size()!= pcmCd.getInnerDeclaration_CompositeDataType().size()) return false;
		for(InnerDeclaration dmlId: dmlCd.getInnerDeclarations()){
			boolean found = false;
			for(de.uka.ipd.sdq.pcm.repository.InnerDeclaration pcmId: pcmCd.getInnerDeclaration_CompositeDataType()){
				if(dmlId.getName().equals(pcmId.getEntityName())){
					found = true;
					break;
				}
			}			
			if(!found) return false;
		}
		return true;
	}
	
	private Parameter makeParameter(String name,ParameterModifier pm){
		Parameter p = RepositoryFactory.eINSTANCE.createParameter();
		p.setDataType(makePrimitiveData("parameter:"+name));
		p.setModifier(pm);
		p.setName(name);
		return p;
	}
	
	private Interface makeInterface(String name, int signatures){
		Interface dmlInterface = RepositoryFactory.eINSTANCE.createInterface();
		dmlInterface.setName("Interface::"+name);
		for(int i = 0;i<signatures;i++){
			dmlInterface.getSignatures().add(makeSignature(i+"::"+name));			
		}		
		return(dmlInterface);
	}
	
	
	
	
	private CompositeDataType makeCompositeData(String name){
		CompositeDataType cd = RepositoryFactory.eINSTANCE.createCompositeDataType();
		cd.setName(name);
		
		for(int i=0;i<4;i++){
			PrimitiveDataType pd = makePrimitiveData(name);
			cd.getInnerDeclarations().add(makeInnerDeclaration(name+":"+i, pd));			
		}		
		return cd;
	}
	
	private InnerDeclaration makeInnerDeclaration(String name, DataType data){
		InnerDeclaration id = RepositoryFactory.eINSTANCE.createInnerDeclaration();
		id.setDataType(data);
		id.setName("innerdeclaration::"+name);
		return id;
	}
	private PrimitiveDataType makePrimitiveData(String name){
		PrimitiveDataType pd = RepositoryFactory.eINSTANCE.createPrimitiveDataType();
		pd.setType(PrimitiveTypeEnum.INT);
		pd.setName("primitive::"+name);
		return pd;
	}

	private Signature makeSignature(String name){
		Signature dmlSignature = RepositoryFactory.eINSTANCE.createSignature();
		dmlSignature.setName("Signature::"+name);
		return dmlSignature;
	}
}
