package tools.descartes.dml2pcm;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

import de.uka.ipd.sdq.pcm.resourceenvironment.CommunicationLinkResourceSpecification;
import de.uka.ipd.sdq.pcm.resourceenvironment.LinkingResource;
import de.uka.ipd.sdq.pcm.resourceenvironment.ResourceContainer;
import de.uka.ipd.sdq.pcm.resourceenvironment.ResourceEnvironment;
import de.uka.ipd.sdq.pcm.resourceenvironment.ResourceenvironmentFactory;
import edu.kit.ipd.descartes.mm.resourceconfiguration.ActiveResourceSpecification;
import edu.kit.ipd.descartes.mm.resourceconfiguration.ConfigurationSpecification;
import edu.kit.ipd.descartes.mm.resourceconfiguration.LinkingResourceSpecification;
import edu.kit.ipd.descartes.mm.resourceconfiguration.ProcessingResourceSpecification;
import edu.kit.ipd.descartes.mm.resourceconfiguration.ResourceconfigurationFactory;
import edu.kit.ipd.descartes.mm.resourcelandscape.ComputingInfrastructure;
import edu.kit.ipd.descartes.mm.resourcelandscape.DataCenter;
import edu.kit.ipd.descartes.mm.resourcelandscape.DistributedDataCenter;
import edu.kit.ipd.descartes.mm.resourcelandscape.ResourcelandscapeFactory;
import edu.kit.ipd.descartes.mm.resourcetype.CommunicationLinkResourceType;
import edu.kit.ipd.descartes.mm.resourcetype.ProcessingResourceType;
import edu.kit.ipd.descartes.mm.resourcetype.ResourcetypeFactory;


public class ResourceLandscapeTransformationTest {
	
	@Test
	public void testLinkingResource(){			
		CommunicationLinkResourceType clt1 = ResourcetypeFactory.eINSTANCE.createCommunicationLinkResourceType();
		clt1.setId("LAN");
		ProcessingResourceType rt1 = ResourcetypeFactory.eINSTANCE.createProcessingResourceType();
		rt1.setId("CPU");
		ArrayList<ActiveResourceSpecification> activeSpecs = new ArrayList<ActiveResourceSpecification>();
		LinkingResourceSpecification lrs1 = null;
		for(int i=0;i<2;i++){
			DataCenter dc1 = ResourcelandscapeFactory.eINSTANCE.createDataCenter();
			ComputingInfrastructure ci1 = ResourcelandscapeFactory.eINSTANCE.createComputingInfrastructure();
			dc1.getContains().add(ci1);
			ci1.setName("DataCenter"+i);
			ci1.setId("DataCenter"+i);
			ActiveResourceSpecification cs1 = ResourceconfigurationFactory.eINSTANCE.createActiveResourceSpecification();
			activeSpecs.add(cs1);
			ci1.getConfigSpec().add(cs1);			
			
			if(i==1){
				lrs1 = ResourceconfigurationFactory.eINSTANCE.createLinkingResourceSpecification();
				lrs1.setCommunicationLinkResourceType(clt1);
				lrs1.setName("linking 1");
				lrs1.setBandwidth(i*1000.0f);
				lrs1.getConnectedResourceSpecifications().add(activeSpecs.get(0));
				lrs1.getConnectedResourceSpecifications().add(activeSpecs.get(1));
				lrs1.setParentResourceConfiguration(activeSpecs.get(1));
				cs1.getLinkingResources().add(lrs1);
			}
		}
		Transformator transformator = new Transformator();
		LinkingResource ls = transformator.getResourceLandscapeTransformator().transformLinkingResourceSpecification(lrs1);		
		System.out.println("Linking Resource:"+ls.getCommunicationLinkResourceSpecifications_LinkingResource().getThroughput_CommunicationLinkResourceSpecification().getSpecification());
		
		assertTrue(ls.getCommunicationLinkResourceSpecifications_LinkingResource().getThroughput_CommunicationLinkResourceSpecification().getSpecification().equals(Double.toString(lrs1.getBandwidth())));
		
		assertTrue(ls.getConnectedResourceContainers_LinkingResource().size()==2);
		
		for(ResourceContainer pcmRc : ls.getConnectedResourceContainers_LinkingResource()){
			System.out.println("2::"+pcmRc.getEntityName()+"::"+pcmRc.getId());
			assertTrue(pcmRc.getEntityName().equals("DataCenter1") || pcmRc.getEntityName().equals("DataCenter0"));
		}	
		
	}
	
	@Test
	public void distributedDataCentertest() {
		DistributedDataCenter ddc = ResourcelandscapeFactory.eINSTANCE.createDistributedDataCenter();
		
		ArrayList<DataCenter> datacenters = new ArrayList<DataCenter>();		
		ProcessingResourceType rt1 = ResourcetypeFactory.eINSTANCE.createProcessingResourceType();
		rt1.setId("CPU");
		
		CommunicationLinkResourceType clt1 = ResourcetypeFactory.eINSTANCE.createCommunicationLinkResourceType();
		clt1.setId("LAN");
		
		
		ArrayList<ActiveResourceSpecification> activeSpecs = new ArrayList<ActiveResourceSpecification>();
		for(int i =0;i<3;i++){
			DataCenter dc1 = ResourcelandscapeFactory.eINSTANCE.createDataCenter();
			ComputingInfrastructure ci1 = ResourcelandscapeFactory.eINSTANCE.createComputingInfrastructure();
			dc1.getContains().add(ci1);
			dc1.setName("DataCenter"+i);
			ActiveResourceSpecification cs1 = ResourceconfigurationFactory.eINSTANCE.createActiveResourceSpecification();
			activeSpecs.add(cs1);
			ci1.getConfigSpec().add(cs1);
			
			ProcessingResourceSpecification pr1 = ResourceconfigurationFactory.eINSTANCE.createProcessingResourceSpecification();
			pr1.setProcessingRate(i*100.0f);
			pr1.setActiveResourceType(rt1);
			cs1.getProcessingResourceSpecifications().add(pr1);
			
			if(i==1){
				LinkingResourceSpecification lrs1 = ResourceconfigurationFactory.eINSTANCE.createLinkingResourceSpecification();
				lrs1.setCommunicationLinkResourceType(clt1);
				lrs1.setName("linking 1");
				lrs1.setBandwidth(i*1000.0f);
				lrs1.getConnectedResourceSpecifications().add(activeSpecs.get(0));
				lrs1.getConnectedResourceSpecifications().add(activeSpecs.get(1));
				lrs1.setParentResourceConfiguration(activeSpecs.get(1));
				cs1.getLinkingResources().add(lrs1);
			}
			if(i==2){
				LinkingResourceSpecification lrs1 = ResourceconfigurationFactory.eINSTANCE.createLinkingResourceSpecification();
				lrs1.setCommunicationLinkResourceType(clt1);
				lrs1.setName("linking 2");
				lrs1.getConnectedResourceSpecifications().add(activeSpecs.get(1));
				lrs1.getConnectedResourceSpecifications().add(activeSpecs.get(2));
				lrs1.setParentResourceConfiguration(activeSpecs.get(2));
				cs1.getLinkingResources().add(lrs1);				
			}
			datacenters.add(dc1);
		}
		for(DataCenter dc:datacenters){
			ddc.getConsistsOf().add(dc);
		}
		
		Transformator transformator = new Transformator();
		ResourceEnvironment re = transformator.getResourceLandscapeTransformator().transformDistributedDataCenter(ddc);
		
			
		for(LinkingResource ls : re.getLinkingResources__ResourceEnvironment()){
			System.out.println(ls);
			for(ResourceContainer rc: ls.getConnectedResourceContainers_LinkingResource()){
				System.out.println("	"+ls);
			}
		}
		
		
		
		
	}


}
