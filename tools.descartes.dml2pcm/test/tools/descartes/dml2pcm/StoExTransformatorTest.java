package tools.descartes.dml2pcm;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.Token;
import org.antlr.runtime.TokenSource;
import org.antlr.runtime.TokenStream;
import org.junit.Test;

import de.uka.ipd.sdq.pcm.core.PCMRandomVariable;
import de.uka.ipd.sdq.stoex.Parenthesis;
import de.uka.ipd.sdq.stoex.StoexFactory;
import de.uka.ipd.sdq.stoex.parser.StochasticExpressionsLexer;
import de.uka.ipd.sdq.stoex.parser.StochasticExpressionsParser;
import de.uka.ipd.sdq.stoex.provider.StoexEditPlugin;
import edu.kit.ipd.descartes.mm.applicationlevel.functions.BoolSample;
import edu.kit.ipd.descartes.mm.applicationlevel.functions.BoolSampleList;
import edu.kit.ipd.descartes.mm.applicationlevel.functions.ContinuousSample;
import edu.kit.ipd.descartes.mm.applicationlevel.functions.DoubleSample;
import edu.kit.ipd.descartes.mm.applicationlevel.functions.DoubleSampleList;
import edu.kit.ipd.descartes.mm.applicationlevel.functions.EnumSample;
import edu.kit.ipd.descartes.mm.applicationlevel.functions.EnumSampleList;
import edu.kit.ipd.descartes.mm.applicationlevel.functions.Expression;
import edu.kit.ipd.descartes.mm.applicationlevel.functions.FormalParameter;
import edu.kit.ipd.descartes.mm.applicationlevel.functions.FunctionsFactory;
import edu.kit.ipd.descartes.mm.applicationlevel.functions.IntLiteral;
import edu.kit.ipd.descartes.mm.applicationlevel.functions.IntSample;
import edu.kit.ipd.descartes.mm.applicationlevel.functions.IntSampleList;
import edu.kit.ipd.descartes.mm.applicationlevel.functions.ProbabilityMassFunction;
import edu.kit.ipd.descartes.mm.applicationlevel.functions.Product;
import edu.kit.ipd.descartes.mm.applicationlevel.functions.ProductOperation;
import edu.kit.ipd.descartes.mm.applicationlevel.functions.Term;
import edu.kit.ipd.descartes.mm.applicationlevel.functions.TermOperation;
import de.uka.ipd.sdq.pcm.seff.ProbabilisticBranchTransition;
import de.uka.ipd.sdq.pcm.stochasticexpressions.parser.PCMStoExLexer;
import de.uka.ipd.sdq.pcm.stochasticexpressions.parser.PCMStoExParser;
import de.uka.ipd.sdq.probfunction.BoxedPDF;

import org.antlr.runtime.CommonTokenStream;
public class StoExTransformatorTest {

	@Test
	public void TermTest(){
		Term term = FunctionsFactory.eINSTANCE.createTerm();
		
		IntLiteral left = FunctionsFactory.eINSTANCE.createIntLiteral();
		BigInteger leftVal = BigInteger.valueOf(12);
		left.setValue(leftVal);
		
		IntLiteral right = FunctionsFactory.eINSTANCE.createIntLiteral();
		BigInteger rightVal = BigInteger.valueOf(33);		
		right.setValue(rightVal);
		
		term.setLeft(left);
		term.setRight(right);
		term.setOperation(TermOperation.ADD);
		
		PCMRandomVariable pcmRandomVar = new StoexTransformator().transformRandomVariable(term);
		System.out.println("StoEX:"+pcmRandomVar.getSpecification());
		Parenthesis p = (Parenthesis) pcmRandomVar.getExpression();
		de.uka.ipd.sdq.stoex.TermExpression t = isOfTerm(p);
		de.uka.ipd.sdq.stoex.Expression exp = parser(pcmRandomVar.getSpecification());
		assertTrue(exp!=null);
		
		//t.getLeft()
		//de.uka.ipd.sdq.stoex.Term = StoexFactory.eINSTANCE.createTermExpression();
	
		
	}
	
	@Test 
	public void EnumPMFTest(){
		ProbabilityMassFunction pmf = FunctionsFactory.eINSTANCE.createProbabilityMassFunction();
		EnumSampleList list = FunctionsFactory.eINSTANCE.createEnumSampleList();
		EnumSample sample1 = FunctionsFactory.eINSTANCE.createEnumSample();
		EnumSample sample2 = FunctionsFactory.eINSTANCE.createEnumSample();
		sample1.setProbability(BigDecimal.valueOf(0.5));
		sample1.setValue("01");
		sample2.setProbability(BigDecimal.valueOf(0.5));
		sample2.setValue("02");
		
		list.getItems().add(sample1);
		list.getItems().add(sample2);
		pmf.setSamples(list);
		
		String spec = new StoexTransformator().transformProbabilityMassFunctionToSpec(pmf);
		System.out.println("stoex pmf: "+spec);
		de.uka.ipd.sdq.stoex.Expression exp = parser(spec);
		assertTrue(exp!=null);
		
	}
	
	@Test 
	public void BoolPMFTest(){
		ProbabilityMassFunction pmf = FunctionsFactory.eINSTANCE.createProbabilityMassFunction();
		BoolSampleList list = FunctionsFactory.eINSTANCE.createBoolSampleList();
		BoolSample sample1 = FunctionsFactory.eINSTANCE.createBoolSample();
		BoolSample sample2 = FunctionsFactory.eINSTANCE.createBoolSample();
		sample1.setProbability(BigDecimal.valueOf(0.5));
		sample1.setValue(true);
		sample2.setProbability(BigDecimal.valueOf(0.5));
		sample2.setValue(false);
		
		list.getItems().add(sample1);
		list.getItems().add(sample2);
		pmf.setSamples(list);
		
		String spec = new StoexTransformator().transformProbabilityMassFunctionToSpec(pmf);
		System.out.println("stoex pmf: "+spec);
		de.uka.ipd.sdq.stoex.Expression exp = parser(spec);
		assertTrue(exp!=null);	
	}
	@Test 
	public void IntPMFTest(){
		ProbabilityMassFunction pmf = FunctionsFactory.eINSTANCE.createProbabilityMassFunction();
		IntSampleList list = FunctionsFactory.eINSTANCE.createIntSampleList();
		IntSample sample1 = FunctionsFactory.eINSTANCE.createIntSample();
		IntSample sample2 = FunctionsFactory.eINSTANCE.createIntSample();
		sample1.setProbability(BigDecimal.valueOf(0.5));
		sample1.setValue(BigInteger.valueOf(1));
		sample2.setProbability(BigDecimal.valueOf(0.5));
		sample2.setValue(BigInteger.valueOf(2));
		
		list.getItems().add(sample1);
		list.getItems().add(sample2);
		pmf.setSamples(list);
		
		String spec = new StoexTransformator().transformProbabilityMassFunctionToSpec(pmf);
		System.out.println("stoex pmf: "+spec);
		de.uka.ipd.sdq.stoex.Expression exp = parser(spec);
		assertTrue(exp!=null);	
	}
	@Test 
	public void DoublePMFTest(){
		ProbabilityMassFunction pmf = FunctionsFactory.eINSTANCE.createProbabilityMassFunction();
		DoubleSampleList list = FunctionsFactory.eINSTANCE.createDoubleSampleList();
		DoubleSample sample1 = FunctionsFactory.eINSTANCE.createDoubleSample();
		DoubleSample sample2 = FunctionsFactory.eINSTANCE.createDoubleSample();
		sample1.setProbability(BigDecimal.valueOf(0.5));
		sample1.setValue(BigDecimal.valueOf(1.5));
		sample2.setProbability(BigDecimal.valueOf(0.5));
		sample2.setValue(BigDecimal.valueOf(2.5));
		
		list.getItems().add(sample1);
		list.getItems().add(sample2);
		pmf.setSamples(list);
		
		String spec = new StoexTransformator().transformProbabilityMassFunctionToSpec(pmf);
		System.out.println("stoex pmf: "+spec);
		de.uka.ipd.sdq.stoex.Expression exp = parser(spec);
		assertTrue(exp!=null);	
	}
	@Test 
	public void PDFTest(){
		edu.kit.ipd.descartes.mm.applicationlevel.functions.BoxedPDF pdf = FunctionsFactory.eINSTANCE.createBoxedPDF();
		ContinuousSample sample1 = FunctionsFactory.eINSTANCE.createContinuousSample();
		ContinuousSample sample2 = FunctionsFactory.eINSTANCE.createContinuousSample();
		
		sample1.setProbability(BigDecimal.valueOf(0.5));
		sample1.setValue(BigDecimal.valueOf(1.5));
		sample2.setProbability(BigDecimal.valueOf(0.5));
		sample2.setValue(BigDecimal.valueOf(2.5));
		
		
		pdf.getSample().add(sample1);
		pdf.getSample().add(sample2);
		
		String spec = new StoexTransformator().transformPdfToSpec(pdf);
		System.out.println("stoex pmf: "+spec);
		de.uka.ipd.sdq.stoex.Expression exp = parser(spec);
		
		assertTrue(exp!=null);		
	}
	
	@Test 
	public void variableExpressionTest(){
		Product product = FunctionsFactory.eINSTANCE.createProduct();
		product.setOperation(ProductOperation.MULT);
		FormalParameter fp = FunctionsFactory.eINSTANCE.createFormalParameter();
		fp.setValue("variable");
		IntLiteral left = FunctionsFactory.eINSTANCE.createIntLiteral();
		left.setValue(BigInteger.valueOf(12));
		product.setLeft(left);
		product.setRight(fp);
		PCMRandomVariable pcmRandomVar = new StoexTransformator().transformRandomVariable(product);
		System.out.println("StoEX:"+pcmRandomVar.getSpecification());
		de.uka.ipd.sdq.stoex.Expression exp = parser(pcmRandomVar.getSpecification());
		assertTrue(exp!=null);	
	}
	
	
	private de.uka.ipd.sdq.stoex.Expression parser(String expression) {
		PCMStoExLexer lexer = new PCMStoExLexer(
				new ANTLRStringStream(expression));
		de.uka.ipd.sdq.stoex.Expression formula = null;
		try {
			formula = new PCMStoExParser(new CommonTokenStream(lexer)).expression();
		} catch (RecognitionException e) {
			return null;
		}
		return formula;
	}
	
	public de.uka.ipd.sdq.stoex.TermExpression isOfTerm(de.uka.ipd.sdq.stoex.Expression ex){
		if(ex==null){return null;}
		if(ex instanceof Parenthesis){
			return isOfTerm(((Parenthesis) ex).getInnerExpression());
		}
		else{
			if(ex instanceof de.uka.ipd.sdq.stoex.TermExpression){
				return ( de.uka.ipd.sdq.stoex.TermExpression) ex;
			}
			return null;
		}
		
	}
	
}
