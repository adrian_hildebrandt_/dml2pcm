package tools.descartes.dml2pcm;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.junit.Test;

import de.uka.ipd.sdq.pcm.seff.AbstractAction;
import de.uka.ipd.sdq.pcm.seff.BranchAction;
import de.uka.ipd.sdq.pcm.seff.ProbabilisticBranchTransition;
import de.uka.ipd.sdq.pcm.seff.ResourceDemandingSEFF;
import tools.descartes.dml2pcm.*;
import edu.kit.ipd.descartes.core.CoreFactory;
import edu.kit.ipd.descartes.mm.applicationlevel.functions.BoolSample;
import edu.kit.ipd.descartes.mm.applicationlevel.functions.BoolSampleList;
import edu.kit.ipd.descartes.mm.applicationlevel.functions.FunctionsFactory;
import edu.kit.ipd.descartes.mm.applicationlevel.functions.IntLiteral;
import edu.kit.ipd.descartes.mm.applicationlevel.functions.ProbabilityMassFunction;
import edu.kit.ipd.descartes.mm.applicationlevel.functions.RandomVariable;
import edu.kit.ipd.descartes.mm.applicationlevel.parameterdependencies.ModelVariableCharacterizationType;
import edu.kit.ipd.descartes.mm.applicationlevel.repository.InterfaceRequiringRole;
import edu.kit.ipd.descartes.mm.applicationlevel.repository.RepositoryFactory;
import edu.kit.ipd.descartes.mm.applicationlevel.repository.Semaphore;
import edu.kit.ipd.descartes.mm.applicationlevel.repository.Signature;
import edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.AcquireAction;
import edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.BlackBoxBehavior;
import edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.CallFrequency;
import edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.CoarseGrainedBehavior;
import edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.ComponentInternalBehavior;
import edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.ExternalCall;
import edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.ExternalCallAction;
import edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.ExternalCallFrequency;
import edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.FineGrainedBehavior;
import edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.InternalAction;
import edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.ReleaseAction;
import edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.ResourceDemand;
import edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.ResponseTime;
import edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.ServicebehaviorFactory;
import edu.kit.ipd.descartes.mm.resourcetype.ResourceType;
import edu.kit.ipd.descartes.mm.resourcetype.ResourcetypeFactory;

public class BehaviorTransformatorTest {

	@Test
	public void transformAcquireActionTest() {
		AcquireAction dmlAcquireAction = ServicebehaviorFactory.eINSTANCE.createAcquireAction();
		
		Semaphore dmlSemaphore = RepositoryFactory.eINSTANCE.createSemaphore();
		dmlSemaphore.setName("junitest_semaphore");
		dmlSemaphore.setCapacity(BigInteger.valueOf(1234));
		dmlAcquireAction.setSemaphore(dmlSemaphore);		
		Transformator transformator = new Transformator();
		BehaviorTransformator behaviorTransformator = transformator.getBehaviorTransformator();
		de.uka.ipd.sdq.pcm.seff.AcquireAction pcmAcquireAction = behaviorTransformator.transformAcquireAction(dmlAcquireAction);
		System.out.println("Semaphore action:" +pcmAcquireAction.getPassiveresource_AcquireAction());
		assertTrue(pcmAcquireAction.getPassiveresource_AcquireAction().getCapacity_PassiveResource().getSpecification().equals(dmlAcquireAction.getSemaphore().getCapacity().toString()));
		
	}
	
	@Test
	public void transformReleaseActionTest() {
		ReleaseAction dmlReleaseAction = ServicebehaviorFactory.eINSTANCE.createReleaseAction();
		Semaphore dmlSemaphore = RepositoryFactory.eINSTANCE.createSemaphore();
		dmlSemaphore.setName("junitest_semaphore");
		dmlSemaphore.setCapacity(BigInteger.valueOf(1234));
		dmlReleaseAction.setSemaphore(dmlSemaphore);		
		Transformator transformator = new Transformator();
		BehaviorTransformator behaviorTransformator = transformator.getBehaviorTransformator();
		de.uka.ipd.sdq.pcm.seff.ReleaseAction pcmAcquireAction = behaviorTransformator.transformReleaseAction(dmlReleaseAction);
		//System.out.println("Semaphore action:" +pcmAcquireAction.getPassiveResource_ReleaseAction().getCapacity_PassiveResource().getSpecification());
		assertTrue(pcmAcquireAction.getPassiveResource_ReleaseAction().getCapacity_PassiveResource().getSpecification().equals(dmlReleaseAction.getSemaphore().getCapacity().toString()));
	}
	@Test
	public void transfromExternalCallAction(){
		ExternalCall ec = makeExternalCall("externalcallaction");
		ExternalCallAction ea = ServicebehaviorFactory.eINSTANCE.createExternalCallAction();
		ea.setExternalCall(ec);
		Transformator transformator = new Transformator();		
		de.uka.ipd.sdq.pcm.seff.ExternalCallAction pcmEA = transformator.getBehaviorTransformator().transformExternalCallAction(ea);
		
		assertTrue(pcmEA.getRole_ExternalService().getId().equals(ec.getInterfaceRequiringRole().getId()));
		assertTrue(pcmEA.getCalledService_ExternalService().getId().equals(pcmEA.getCalledService_ExternalService().getId()));
		
	}
	
	
	
	@Test
	public void transformFineGrainedBehavior(){
		FineGrainedBehavior fgb = ServicebehaviorFactory.eINSTANCE.createFineGrainedBehavior();
		ComponentInternalBehavior cib = ServicebehaviorFactory.eINSTANCE.createComponentInternalBehavior();
		InternalAction dmlInternalAction = makeInternalAction();
		fgb.setBehavior(cib);
		cib.getActions().add(dmlInternalAction);
		Transformator transformator = new Transformator();
		ResourceDemandingSEFF pcmSEFF = transformator.getBehaviorTransformator().transformFineGrainedBehavior(fgb);					
		assertTrue(pcmSEFF.EachActionExceptStartActionandStopActionMustHhaveAPredecessorAndSuccessor(null, null));
		
	}
	@Test
	public void transformBlackBoxBehaviorTest(){
		BlackBoxBehavior bbb = ServicebehaviorFactory.eINSTANCE.createBlackBoxBehavior();
		ResponseTime rt = ServicebehaviorFactory.eINSTANCE.createResponseTime();
		rt.setCharacterization(ModelVariableCharacterizationType.EXPLICIT);
		
		RandomVariable dmlRV = FunctionsFactory.eINSTANCE.createRandomVariable();
		IntLiteral dmlIntLiteral = FunctionsFactory.eINSTANCE.createIntLiteral();	
		dmlRV.setProbFunction(dmlIntLiteral);
		
		dmlIntLiteral.setValue(BigInteger.valueOf(100));
		rt.setExplicitDescription((RandomVariable) dmlRV);
		bbb.setResponseTime(rt);
		Transformator transformator = new Transformator();
		BehaviorTransformator behaviorTransformator = transformator.getBehaviorTransformator();
		ResourceDemandingSEFF pcmSEFF = behaviorTransformator.transformBlackBoxBehaviour(bbb);
		
		assertTrue(pcmSEFF.EachActionExceptStartActionandStopActionMustHhaveAPredecessorAndSuccessor(null, null));		
		for(AbstractAction aa: pcmSEFF.getSteps_Behaviour()){
			if(aa instanceof de.uka.ipd.sdq.pcm.seff.InternalAction){
				assertTrue(((de.uka.ipd.sdq.pcm.seff.InternalAction) aa).getResourceDemand_Action().get(0).getRequiredResource_ParametricResourceDemand().getEntityName().toLowerCase().equals("delay"));
				System.out.println("BBB:"+((de.uka.ipd.sdq.pcm.seff.InternalAction) aa).getResourceDemand_Action().get(0).getSpecification_ParametericResourceDemand().getSpecification());
				assertTrue(((de.uka.ipd.sdq.pcm.seff.InternalAction) aa).getResourceDemand_Action().get(0).getSpecification_ParametericResourceDemand().getSpecification().equals(dmlIntLiteral.getValue().toString()));
			}
		}
	}
	@Test
	public void transformCoarseGrainedBehaviorTest(){
		CoarseGrainedBehavior cgb = ServicebehaviorFactory.eINSTANCE.createCoarseGrainedBehavior();
		ExternalCallFrequency ecf = ServicebehaviorFactory.eINSTANCE.createExternalCallFrequency();
		CallFrequency cf = ServicebehaviorFactory.eINSTANCE.createCallFrequency();
		cf.setCharacterization(ModelVariableCharacterizationType.EXPLICIT);		
		RandomVariable rv = makePMF();
		cf.setExplicitDescription(rv);
		ecf.setFrequency(cf);
		ExternalCall ec = makeExternalCall("cgb to test");
		ecf.setExternalCall(ec);
		cgb.getExternalCallFrequency().add(ecf);
		
		cgb.getResourceDemand().add(makeResourceDemand());
		Transformator transformator = new Transformator();
		BehaviorTransformator behaviorTransformator = transformator.getBehaviorTransformator();
		ResourceDemandingSEFF pcmSEFF = behaviorTransformator.transformCoarseGrainedBehavior(cgb);
		
		
		for(AbstractAction aa:pcmSEFF.getSteps_Behaviour()){
			if(aa instanceof BranchAction){
				assertTrue(((BranchAction) aa).getBranches_Branch().size()==2);
				ProbabilisticBranchTransition branch1 = (ProbabilisticBranchTransition) ((BranchAction) aa).getBranches_Branch().get(0);
				ProbabilisticBranchTransition branch2 = (ProbabilisticBranchTransition) ((BranchAction) aa).getBranches_Branch().get(1);
				assertTrue(Float.toString((float) branch1.getBranchProbability()).equals("0.8") || Float.toString((float) branch1.getBranchProbability()).equals("0.2")); 
				assertTrue(Float.toString((float) branch2.getBranchProbability()).equals("0.8") || Float.toString((float) branch2.getBranchProbability()).equals("0.2")); 
			}
			else if(aa instanceof de.uka.ipd.sdq.pcm.seff.InternalAction){
				assertTrue(((de.uka.ipd.sdq.pcm.seff.InternalAction) aa).getResourceDemand_Action().get(0).getSpecification_ParametericResourceDemand().getSpecification().equals("100"));
			}
		}
		
		
	}
	
	@Test
	public void transformCoarseGrainedBehaviorTestMultipleResourceDemands(){
		CoarseGrainedBehavior cgb = ServicebehaviorFactory.eINSTANCE.createCoarseGrainedBehavior();
		ResourceType rt = ResourcetypeFactory.eINSTANCE.createProcessingResourceType();
		rt.setId("CPU");
		rt.setName("CPU");
		for(int i = 0;i<9;i++){
			cgb.getResourceDemand().add(makeResourceDemand());
			cgb.getResourceDemand().get(i).setResourceType(rt);
		}
		
		Transformator transformator = new Transformator();
		BehaviorTransformator behaviorTransformator = transformator.getBehaviorTransformator();
		ResourceDemandingSEFF pcmSEFF = behaviorTransformator.transformCoarseGrainedBehavior(cgb);
		
		int branchActions = 0;
		for(AbstractAction aa:pcmSEFF.getSteps_Behaviour()){
			if(aa instanceof de.uka.ipd.sdq.pcm.seff.InternalAction){
				branchActions++;
			}			
		}
		assertTrue(branchActions == 9);
		assertTrue(pcmSEFF.EachActionExceptStartActionandStopActionMustHhaveAPredecessorAndSuccessor(null, null));
	}
	
	
	private RandomVariable makePMF(){
		ProbabilityMassFunction pmf = FunctionsFactory.eINSTANCE.createProbabilityMassFunction();
		BoolSampleList list = FunctionsFactory.eINSTANCE.createBoolSampleList();
		BoolSample sample1 = FunctionsFactory.eINSTANCE.createBoolSample();
		BoolSample sample2 = FunctionsFactory.eINSTANCE.createBoolSample();
		sample1.setProbability(BigDecimal.valueOf(0.8));
		sample1.setValue(true);
		sample2.setProbability(BigDecimal.valueOf(0.2));
		sample2.setValue(false);
		
		list.getItems().add(sample1);
		list.getItems().add(sample2);
		pmf.setSamples(list);
		
		RandomVariable rv = FunctionsFactory.eINSTANCE.createRandomVariable();
		rv.setProbFunction(pmf);
		return rv;
		
	}
	
	private InternalAction makeInternalAction(){
		InternalAction dmlInternalAction = ServicebehaviorFactory.eINSTANCE.createInternalAction();
		ResourceDemand dmlResourceDemand = makeResourceDemand();
				
		dmlResourceDemand.setCharacterization(ModelVariableCharacterizationType.EXPLICIT);
		dmlInternalAction.getResourceDemand().add(dmlResourceDemand);
		return dmlInternalAction;
	}
	
	private ResourceDemand makeResourceDemand(){
		ResourceDemand dmlResourceDemand = ServicebehaviorFactory.eINSTANCE.createResourceDemand();				
		dmlResourceDemand.setCharacterization(ModelVariableCharacterizationType.EXPLICIT);
		RandomVariable dmlRV = FunctionsFactory.eINSTANCE.createRandomVariable();
		IntLiteral dmlIntLiteral = FunctionsFactory.eINSTANCE.createIntLiteral();	
		dmlRV.setProbFunction(dmlIntLiteral);
		
		dmlIntLiteral.setValue(BigInteger.valueOf(100));
		dmlResourceDemand.setExplicitDescription((RandomVariable) dmlRV);
		
		return dmlResourceDemand;
	}
	
	
	private ExternalCall makeExternalCall(String name){
		ExternalCall ec = ServicebehaviorFactory.eINSTANCE.createExternalCall();
		ec.setId("external call::"+name);
		InterfaceRequiringRole reqRole = RepositoryFactory.eINSTANCE.createInterfaceRequiringRole();
		reqRole.setName("external call::reqrole::"+name);
		ec.setInterfaceRequiringRole(reqRole);
		Signature sig = RepositoryFactory.eINSTANCE.createSignature();
		sig.setName("external call::signature::"+name);
		ec.setSignature(sig);
		return ec;
		//ec.setInterfaceRequiringRole();
	}
	
	
	
}
