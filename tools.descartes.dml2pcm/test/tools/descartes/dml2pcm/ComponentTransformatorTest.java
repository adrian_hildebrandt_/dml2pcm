package tools.descartes.dml2pcm;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

import com.sun.org.apache.xerces.internal.impl.dtd.models.CMLeaf;

import de.uka.ipd.sdq.pcm.core.composition.ProvidedDelegationConnector;
import de.uka.ipd.sdq.pcm.core.composition.RequiredDelegationConnector;
import de.uka.ipd.sdq.pcm.resourceenvironment.ResourceContainer;
import de.uka.ipd.sdq.stoex.RandomVariable;
import de.uka.ipd.sdq.stoex.parser.StochasticExpressionsLexer;
import de.uka.ipd.sdq.stoex.parser.StochasticExpressionsParser;
import edu.kit.ipd.descartes.mm.applicationlevel.repository.AssemblyConnector;
import edu.kit.ipd.descartes.mm.applicationlevel.repository.AssemblyContext;
import edu.kit.ipd.descartes.mm.applicationlevel.repository.BasicComponent;
import edu.kit.ipd.descartes.mm.applicationlevel.repository.ComposedStructure;
import edu.kit.ipd.descartes.mm.applicationlevel.repository.CompositeComponent;
import edu.kit.ipd.descartes.mm.applicationlevel.repository.Interface;
import edu.kit.ipd.descartes.mm.applicationlevel.repository.InterfaceProvidingRole;
import edu.kit.ipd.descartes.mm.applicationlevel.repository.InterfaceRequiringRole;
import edu.kit.ipd.descartes.mm.applicationlevel.repository.ProvidingDelegationConnector;
import edu.kit.ipd.descartes.mm.applicationlevel.repository.RepositoryComponent;
import edu.kit.ipd.descartes.mm.applicationlevel.repository.RepositoryFactory;
import edu.kit.ipd.descartes.mm.applicationlevel.repository.RequiringDelegationConnector;
import edu.kit.ipd.descartes.mm.applicationlevel.repository.Signature;
import edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.FineGrainedBehavior;
import edu.kit.ipd.descartes.mm.applicationlevel.servicebehavior.ServicebehaviorFactory;

public class ComponentTransformatorTest {

	@Test
	public void transformBasicComponentTest() {
		
		BasicComponent dmlBasicComponent = RepositoryFactory.eINSTANCE.createBasicComponent();
		InterfaceProvidingRole dmlProvidingRole = RepositoryFactory.eINSTANCE.createInterfaceProvidingRole();
		InterfaceRequiringRole dmlRequiringRole = RepositoryFactory.eINSTANCE.createInterfaceRequiringRole();
		Interface dmlInterface = RepositoryFactory.eINSTANCE.createInterface();
		Signature dmlSignature1 = RepositoryFactory.eINSTANCE.createSignature();
		Signature dmlSignature2 = RepositoryFactory.eINSTANCE.createSignature();		
		dmlInterface.getSignatures().add(dmlSignature2);
		dmlInterface.getSignatures().add(dmlSignature1);
		
		Interface dmlRequiringInterface = RepositoryFactory.eINSTANCE.createInterface();
		Signature dmlRequiringSignature1 = RepositoryFactory.eINSTANCE.createSignature();
		Signature dmlRequiringSignature2 = RepositoryFactory.eINSTANCE.createSignature();		
		dmlRequiringInterface.getSignatures().add(dmlRequiringSignature2);
		dmlRequiringInterface.getSignatures().add(dmlRequiringSignature1);
		
		dmlProvidingRole.setInterface(dmlInterface);
		dmlRequiringRole.setInterface(dmlRequiringInterface);
		
		dmlBasicComponent.getInterfaceProvidingRoles().add(dmlProvidingRole);
		dmlBasicComponent.getInterfaceRequiringRoles().add(dmlRequiringRole);
		Transformator transformator = new Transformator();
		de.uka.ipd.sdq.pcm.repository.BasicComponent pcmBasicComponent = transformator.getRepositoryTransformator().transformBasicComponent(dmlBasicComponent);
		
		assertTrue(pcmBasicComponent.getProvidedRoles_InterfaceProvidingEntity().size() == dmlBasicComponent.getInterfaceProvidingRoles().size());
		assertTrue(pcmBasicComponent.getRequiredRoles_InterfaceRequiringEntity().size() == dmlBasicComponent.getInterfaceRequiringRoles().size());
		assertTrue(pcmBasicComponent.getProvidedRoles_InterfaceProvidingEntity().get(0).getId()== dmlProvidingRole.getId());
		assertTrue(pcmBasicComponent.getRequiredRoles_InterfaceRequiringEntity().get(0).getId()== dmlRequiringRole.getId());		
		pcmBasicComponent.NoSeffTypeUsedTwice(null, null);			
	}
	@Test
	public void transformCompositeComponentTest(){
		CompositeComponent dmlCompositeUnderTest = makeCompositeComponent("under test");		
		BasicComponent bc1 = makeBasicComponent("bc1");
		BasicComponent bc2 = makeBasicComponent("bc2");
		CompositeComponent dmlCompositeAssembly = makeCompositeComponent("integrated");		
		addProvReqRolesToComponent(dmlCompositeAssembly);
		dmlCompositeUnderTest.getAssemblyContexts().add(makeAssemblyContext(bc1));
		dmlCompositeUnderTest.getAssemblyContexts().add(makeAssemblyContext(bc2));
		dmlCompositeUnderTest.getAssemblyContexts().add(makeAssemblyContext(dmlCompositeAssembly));
		Transformator transformator = new Transformator();
		de.uka.ipd.sdq.pcm.repository.CompositeComponent pcmComposite = transformator.getRepositoryTransformator().transformCompositeComponent(dmlCompositeUnderTest);
		assertTrue(pcmComposite.getAssemblyContexts__ComposedStructure().size()==3);
		
		de.uka.ipd.sdq.pcm.repository.RepositoryComponent[] assemblycomponents = new de.uka.ipd.sdq.pcm.repository.RepositoryComponent[3];
		
		String[] names = new String[3];
		names[0] = bc1.getName();
		names[1] = bc2.getName();
		names[2] = dmlCompositeAssembly.getName();
		int count = 3;
		for(de.uka.ipd.sdq.pcm.core.composition.AssemblyContext assembly:pcmComposite.getAssemblyContexts__ComposedStructure()){
			for(int i = 0;i<3;i++){
				if(assembly.getEncapsulatedComponent__AssemblyContext().getEntityName().equals(names[0])) count--;
			}
			
		}	
		
		assertTrue(count==0);	
	}
	
	@Test
	public void transformAssemblyConnectorTest(){
		BasicComponent bc1 = makeBasicComponent("bc1");
		BasicComponent bc2 = makeBasicComponent("bc2");
		AssemblyContext acxt1 = makeAssemblyContext(bc1);
		AssemblyContext acxt2 = makeAssemblyContext(bc2);
		AssemblyConnector acon = makeAssemblyConnector(acxt1,acxt2);
		
		Transformator transformator = new Transformator();
		de.uka.ipd.sdq.pcm.core.composition.AssemblyConnector pcmACon = transformator.getRepositoryTransformator().transformAssemblyConnector(acon);
		
		assertTrue(pcmACon.AssemblyConnectorsReferencedInterfacesMustMatch(null, null));
		assertTrue(pcmACon.AssemblyConnectorsReferencedProvidedRolesAndChildContextMustMatch(null, null));
		assertTrue(pcmACon.AssemblyConnectorsReferencedRequiredRoleAndChildContextMustMatch(null, null));
		
		assertTrue(pcmACon.getProvidingAssemblyContext_AssemblyConnector().getEncapsulatedComponent__AssemblyContext().getEntityName() == bc1.getName());
		assertTrue(pcmACon.getRequiringAssemblyContext_AssemblyConnector().getEncapsulatedComponent__AssemblyContext().getEntityName() == bc2.getName());
	}
	
	@Test
	public void transformProvidingDelegationConnectorTest(){
		CompositeComponent dmlComposite = makeCompositeComponent("compC");		
		BasicComponent bc1 = makeBasicComponent("bc1");
		AssemblyContext ac1 = makeAssemblyContext(bc1);
		dmlComposite.getAssemblyContexts().add(ac1);
		ProvidingDelegationConnector provDel = makeProvidingDelegationConnector(dmlComposite, ac1);
		dmlComposite.getProvidingDelegationConnectors().add(provDel);
		
		Transformator transformator = new Transformator();
		 ProvidedDelegationConnector pcmProvDel = transformator.getRepositoryTransformator().transformProvidingDelegationConnector(provDel);
		
		 assertTrue(pcmProvDel.ComponentOfAssemblyContextAndInnerRoleProvidingComponentNeedToBeTheSame(null, null));
		 assertTrue(pcmProvDel.getParentStructure__Connector().getEntityName().equals(dmlComposite.getName()));
		 assertTrue(pcmProvDel.getAssemblyContext_ProvidedDelegationConnector().getEntityName().equals(ac1.getName()));
		 
	}
	@Test 
	public void transformRequiringDelegationConnectorTest(){
		CompositeComponent dmlComposite = makeCompositeComponent("compC");		
		BasicComponent bc1 = makeBasicComponent("bc1");
		AssemblyContext ac1 = makeAssemblyContext(bc1);
		dmlComposite.getAssemblyContexts().add(ac1);
		
		RequiringDelegationConnector reqDel = makeRequiringDelegationConnector(dmlComposite, ac1);
		Transformator transformator = new Transformator();
		RequiredDelegationConnector pcmReqDel = transformator.getRepositoryTransformator().transformRequiringDelegationConnector(reqDel);
		
		 assertTrue(pcmReqDel.ComponentOfAssemblyContextAndInnerRoleRequiringComponentNeedToBeTheSame(null, null));
		 assertTrue(pcmReqDel.getParentStructure__Connector().getEntityName().equals(dmlComposite.getName()));
		 assertTrue(pcmReqDel.getAssemblyContext_RequiredDelegationConnector().getEntityName().equals(ac1.getName()));
		
		
	}
	
	private RequiringDelegationConnector makeRequiringDelegationConnector(RepositoryComponent boundary, AssemblyContext delegate){
		RequiringDelegationConnector delCon = RepositoryFactory.eINSTANCE.createRequiringDelegationConnector();
		InterfaceRequiringRole reqRole = delegate.getEncapsulatedComponent().getInterfaceRequiringRoles().get(0);
		delCon.setInnerInterfaceRequiringRole(reqRole);
		InterfaceRequiringRole reqRoleOuter = makeRequiringRole(boundary.getName(), reqRole.getInterface());
		boundary.getInterfaceRequiringRoles().add(reqRoleOuter);
		delCon.setOuterInterfaceRequiringRole(reqRoleOuter);
		delCon.setAssemblyContext(delegate);
		delCon.setParentStructure((ComposedStructure) boundary);
		return delCon;
	}
	
	private ProvidingDelegationConnector makeProvidingDelegationConnector(RepositoryComponent boundary, AssemblyContext delegate){
		ProvidingDelegationConnector delCon = RepositoryFactory.eINSTANCE.createProvidingDelegationConnector();
		InterfaceProvidingRole provRole = delegate.getEncapsulatedComponent().getInterfaceProvidingRoles().get(0);
		delCon.setInnerInterfaceProvidingRole(provRole);
		InterfaceProvidingRole provRoleOuter = makeProvidingRole(boundary.getName(), provRole.getInterface());
		boundary.getInterfaceProvidingRoles().add(provRoleOuter);
		delCon.setOuterInterfaceProvidingRole(provRoleOuter);
		delCon.setAssemblyContext(delegate);
		delCon.setParentStructure((ComposedStructure) boundary);
		return delCon;
	}
	
	private AssemblyConnector makeAssemblyConnector(AssemblyContext prov, AssemblyContext req){
		AssemblyConnector ac = RepositoryFactory.eINSTANCE.createAssemblyConnector();
		InterfaceProvidingRole provRole = prov.getEncapsulatedComponent().getInterfaceProvidingRoles().get(0);
		InterfaceRequiringRole reqRole = makeRequiringRole(req.getEncapsulatedComponent().getName(), provRole.getInterface());
		req.getEncapsulatedComponent().getInterfaceRequiringRoles().add(reqRole);
		ac.setInterfaceProvidingRole(provRole);
		ac.setInterfaceRequiringRole(reqRole);
		ac.setProvidingAssemblyContext(prov);
		ac.setRequiringAssemblyContext(req);		
		return ac;
	}
	
	private AssemblyContext makeAssemblyContext(RepositoryComponent rc){
		AssemblyContext ac = RepositoryFactory.eINSTANCE.createAssemblyContext();
		ac.setName("assemblycontext::"+rc.getName());
		ac.setEncapsulatedComponent(rc);
		return ac;
	}
	
	private BasicComponent makeBasicComponent(String name){
		BasicComponent dmlBasicComponent = RepositoryFactory.eINSTANCE.createBasicComponent();
		dmlBasicComponent.setName("BasicComponent::"+name);
		addProvReqRolesToComponent(dmlBasicComponent);
		return(dmlBasicComponent);
	}
	private InterfaceProvidingRole makeProvidingRole(String name,Interface iface){
		InterfaceProvidingRole dmlProvidingRole = RepositoryFactory.eINSTANCE.createInterfaceProvidingRole();
		dmlProvidingRole.setName("ProvidingRole::"+name);
		dmlProvidingRole.setInterface(iface);
		return dmlProvidingRole;
	}
	private InterfaceRequiringRole makeRequiringRole(String name,Interface iface){
		InterfaceRequiringRole dmlRequiringRole = RepositoryFactory.eINSTANCE.createInterfaceRequiringRole();
		dmlRequiringRole.setName("RequiringRole::"+name);
		dmlRequiringRole.setInterface(iface);
		return dmlRequiringRole;
	}
	
	private void addProvReqRolesToComponent(RepositoryComponent rc){
		String name = rc.getName();
		InterfaceProvidingRole dmlProvidingRole = RepositoryFactory.eINSTANCE.createInterfaceProvidingRole();
		dmlProvidingRole.setName("ProvidingRole::"+name);
		InterfaceRequiringRole dmlRequiringRole = RepositoryFactory.eINSTANCE.createInterfaceRequiringRole();
		dmlRequiringRole.setName("RequiringRole::"+name);
		Interface dmlRequiringInterface = makeInterface(name+"requiring",1);
		Interface dmlProvidingInterface = makeInterface(name+"providing",1);		
		dmlProvidingRole.setInterface(dmlProvidingInterface);
		dmlRequiringRole.setInterface(dmlRequiringInterface);		
		rc.getInterfaceProvidingRoles().add(dmlProvidingRole);
		rc.getInterfaceRequiringRoles().add(dmlRequiringRole);
		
	}
	
	private Interface makeInterface(String name, int signatures){
		Interface dmlInterface = RepositoryFactory.eINSTANCE.createInterface();
		dmlInterface.setName("Interface::"+name);
		for(int i = 0;i<signatures;i++){
			dmlInterface.getSignatures().add(makeSignature(i+"::"+name));			
		}		
		return(dmlInterface);
	}
	
	private Signature makeSignature(String name){
		Signature dmlSignature = RepositoryFactory.eINSTANCE.createSignature();
		dmlSignature.setName("Signature::"+name);
		return dmlSignature;
	}
	
	
	private CompositeComponent makeCompositeComponent(String name){
		CompositeComponent dmlComposite = RepositoryFactory.eINSTANCE.createCompositeComponent();
		dmlComposite.setName(name);
		return dmlComposite;
	}
	
	
	

}
